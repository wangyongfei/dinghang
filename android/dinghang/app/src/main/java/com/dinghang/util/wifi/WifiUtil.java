package com.dinghang.util.wifi;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;

import static android.content.Context.TELEPHONY_SERVICE;

/**
 * wifi、移动网络校验
 * Created by zhuchuanyang on 2017/4/28.
 */

public class WifiUtil {

    public static Boolean netIsExist(Activity activity){
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(null == networkInfo || !connectivityManager.getBackgroundDataSetting()){
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public static int validNetType(Activity activity){

       ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
       NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
       TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(TELEPHONY_SERVICE);

       if(null == networkInfo){
           return -1;
       }

       int netType = networkInfo.getType();
       int netSubType = networkInfo.getSubtype();

       boolean flag1  = false, flag2 = false;
       if(netType == ConnectivityManager.TYPE_WIFI){
            flag1 = true;

       }
       if(netType == ConnectivityManager.TYPE_MOBILE
                && netSubType == TelephonyManager.NETWORK_TYPE_UMTS
                && !telephonyManager.isNetworkRoaming()){

           flag2 = true;

       }

       if(flag1 && !flag2){
           return 1;
       }
       else if(!flag1 && flag2){
            return 2;
       }
       else if(flag1 && flag2){
            return 3;
       }

       return -1;
    }


}
