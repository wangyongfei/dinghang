package com.dinghang.fragment.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dinghang.R;
import com.dinghang.bean.MessageBean;
import com.dinghang.roundimages.common.DownImage;

import java.util.List;

public class MessageAdapter extends BaseAdapter {
	private List<MessageBean> mListMsgBean = null;
	private Context mContext;
	private LayoutInflater mInflater;
	public MessageAdapter(List<MessageBean> listMsgBean, Context context){
		mListMsgBean = listMsgBean;
		mContext = context;
		mInflater = LayoutInflater.from(mContext);
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mListMsgBean.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mListMsgBean.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v = mInflater.inflate(R.layout.message_item_layout, null);
		
		ImageView imageView = (ImageView) v.findViewById(R.id.img_msg_item);
//		imageView.setImageResource(mListMsgBean.get(position).getPhotoDrawableId());
//		imageView.setImageBitmap(returnBitMap(mListMsgBean.get(position).getImgUrl()));
		DownImage downImage = new DownImage(imageView,null);
		downImage.execute(mListMsgBean.get(position).getImgUrl());

		imageView.setBackgroundColor(Color.LTGRAY);
		imageView.setPadding(5,5,5,5);
		TextView nameMsg = (TextView)v.findViewById(R.id.name_msg_item);
		nameMsg.setText(mListMsgBean.get(position).getMessageName());

		TextView contentMsg = (TextView)v.findViewById(R.id.content_msg_item);
		contentMsg.setText(mListMsgBean.get(position).getMessageContent());
		
		TextView timeMsg = (TextView)v.findViewById(R.id.time_msg_item);
		timeMsg.setText(mListMsgBean.get(position).getMessageTime());

		return v;
	}

//v
//	public Bitmap returnBitMap(String url){
//		URL myFileUrl = null;
//		Bitmap bitmap = null;
//		try {
//			myFileUrl = new URL(url);
//		} catch (MalformedURLException e) {
//			e.printStackTrace();
//		}
//		try {
//			HttpURLConnection conn = (HttpURLConnection) myFileUrl
//					.openConnection();
//			conn.setDoInput(true);
//			conn.connect();
//			InputStream is = conn.getInputStream();
//			bitmap = BitmapFactory.decodeStream(is);
//			is.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return bitmap;
//	}

//	class DownImage extends AsyncTask<String,String,Object> {
//
//		private ImageView imageView;
//
//		public DownImage(ImageView imageView) {
//			this.imageView = imageView;
//		}
//
//		@Override
//		protected void onPostExecute(Bitmap result) {
//			imageView.setImageBitmap(result);
//		}
//
//		@Override
//		protected Object doInBackground(String... params) {
//			String url = params[0];
//			Bitmap bitmap = null;
//			try {
//				//加载一个网络图片
//				InputStream is = new URL(url).openStream();
//				bitmap = BitmapFactory.decodeStream(is);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			return bitmap;
//		}
//	}
}

