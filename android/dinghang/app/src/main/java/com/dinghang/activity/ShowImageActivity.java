package com.dinghang.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.dinghang.R;
import com.dinghang.roundimages.GestureImageView;
import com.dinghang.roundimages.common.DownImage;

/**
 * Created by zhuchuanyang on 2017/4/30.
 */

public class ShowImageActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.showimages);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Bitmap bitmap = null;
        /*获取Intent中的Bundle对象*/
        Bundle bundle = this.getIntent().getExtras();
        String url = bundle.getString("imgUrl");
        GestureImageView view = (GestureImageView) findViewById(R.id.dmImageView);
        DownImage downImage = new DownImage(view,null);
        downImage.execute(url);
//        view.setImageResource(R.drawable.ic_app_logo);
//        view.setImageBitmap(bitmap);
//        view.setLayoutParams(params);
//        /*获取Bundle中的数据，注意类型和key*/
//        String url = bundle.getString("imgUrl");
//        Log.i("imgUrl=================",url);
//        AsyncImage downImage = new AsyncImage(view,null);
//        downImage.execute(url);



    }

}
