package com.dinghang.util.http;

/**
 * Created by zhuchuanyang on 2017/4/29.
 */

//import com.lidroid.xutils.HttpUtils;
//import com.lidroid.xutils.exception.HttpException;
//import com.lidroid.xutils.http.RequestParams;
//import com.lidroid.xutils.http.ResponseInfo;
//import com.lidroid.xutils.http.ResponseStream;
//import com.lidroid.xutils.http.callback.RequestCallBack;
//import com.lidroid.xutils.http.client.HttpRequest;
//import com.lidroid.xutils.util.LogUtils;

import java.io.File;

public class HttpUtil {

    String result = "" ;

//    /**
//     * Get请求  异步的
//     * @param url  服务器地址
//     * @param userkey
//     * @param str
//     * @param sign 签名
//     * @return
//     */
//    public String xutilsGet( String url , String userkey , String str , String sign ){
//        RequestParams params = new RequestParams();
//        params.addQueryStringParameter("userkey", userkey );
//        params.addQueryStringParameter("str", str );
//        params.addQueryStringParameter("sign", sign );
//        HttpUtils http = new HttpUtils();
//        http.configCurrentHttpCacheExpiry(1000 * 10); //设置超时时间   10s
//        http.send(HttpRequest.HttpMethod.GET,
//                url ,
//                new RequestCallBack<String>(){
//                    @Override
//                    public void onLoading(long total, long current, boolean isUploading) {
//
//                    }
//
//                    @Override
//                    public void onSuccess(ResponseInfo<String> responseInfo) {
//                        result = responseInfo.result.toString() ;
//                    }
//
//                    @Override
//                    public void onStart() {
//                    }
//
//                    @Override
//                    public void onFailure(HttpException error, String msg) {
//                    }
//                });
//
//        return result ;
//    }
//
//    /**
//     * Post请求 异步的
//     * @param url
//     * @param userkey
//     * @param str
//     * @param sign
//     * @return
//     */
//    public String xutilsPost( String url , String userkey , String str , String sign ){
//        RequestParams params = new RequestParams();
//        params.addQueryStringParameter("userkey", userkey );
//        params.addQueryStringParameter("str", str );
//        params.addQueryStringParameter("sign", sign );
//
//        // 只包含字符串参数时默认使用BodyParamsEntity，
//        // 类似于UrlEncodedFormEntity（"application/x-www-form-urlencoded"）。
//        //params.addBodyParameter("name", "value");
//
//        // 加入文件参数后默认使用MultipartEntity（"multipart/form-data"），
//        // 如需"multipart/related"，xUtils中提供的MultipartEntity支持设置subType为"related"。
//        // 使用params.setBodyEntity(httpEntity)可设置更多类型的HttpEntity（如：
//        // MultipartEntity,BodyParamsEntity,FileUploadEntity,InputStreamUploadEntity,StringEntity）。
//        // 例如发送json参数：params.setBodyEntity(new StringEntity(jsonStr,charset));
//
//        HttpUtils http = new HttpUtils();
//        http.configCurrentHttpCacheExpiry(1000 * 10); //设置超时时间   10s
//        http.send(HttpRequest.HttpMethod.POST ,
//                url ,
//                params,
//                new RequestCallBack<String>() {
//
//                    @Override
//                    public void onStart() {
//                    }
//
//                    @Override
//                    public void onLoading(long total, long current, boolean isUploading) {
//                    }
//
//                    @Override
//                    public void onSuccess(ResponseInfo<String> responseInfo) {
//                        result = responseInfo.result.toString() ;
//                    }
//
//                    @Override
//                    public void onFailure(HttpException error, String msg) {
//
//                    }
//                });
//
//        return result ;
//    }
//
//    /**
//     * 带上传文件的 Post请求   异步的
//     * @param url
//     * @param userkey
//     * @param str
//     * @param sign
//     * @param picString  文件的地址
//     * @return
//     */
//    public String xutilsFilePost( String url , String userkey , String str , String sign , String picString ){
//        RequestParams params = new RequestParams();
//        params.addQueryStringParameter("userkey", userkey );
//        params.addQueryStringParameter("str", str );
//        params.addQueryStringParameter("sign", sign );
//
//        // 只包含字符串参数时默认使用BodyParamsEntity，
//        // 类似于UrlEncodedFormEntity（"application/x-www-form-urlencoded"）。
//        //params.addBodyParameter("name", "value");
//
//        // 加入文件参数后默认使用MultipartEntity（"multipart/form-data"），
//        // 如需"multipart/related"，xUtils中提供的MultipartEntity支持设置subType为"related"。
//        // 使用params.setBodyEntity(httpEntity)可设置更多类型的HttpEntity（如：
//        // MultipartEntity,BodyParamsEntity,FileUploadEntity,InputStreamUploadEntity,StringEntity）。
//        // 例如发送json参数：params.setBodyEntity(new StringEntity(jsonStr,charset));
//        params.addBodyParameter("picture", new File( picString )) ;
//
//        com.lidroid.xutils.HttpUtils http = new com.lidroid.xutils.HttpUtils();
//        http.send(HttpRequest.HttpMethod.POST ,
//                url ,
//                params,
//                new RequestCallBack<String>() {
//
//                    @Override
//                    public void onStart() {
//                    }
//
//                    @Override
//                    public void onLoading(long total, long current, boolean isUploading) {
//                    }
//
//                    @Override
//                    public void onSuccess(ResponseInfo<String> responseInfo) {
//                        result = responseInfo.result.toString() ;
//                    }
//
//                    @Override
//                    public void onFailure(HttpException error, String msg) {
//                    }
//                });
//
//        return result ;
//    }
//
//    //-------------------以上的代码 是 异步请求的， 以下的代码是同步请求的-------------------------//<br>
//    /**
//     * Get同步请求 必须在异步块儿中执行
//     * @param url
//     * @param userkey
//     * @param str
//     * @param sign
//     * @return
//     */
//    private String xutilsGetSync(String url , String userkey , String str , String sign ) {
//        RequestParams params = new RequestParams();
//        params.addQueryStringParameter("userkey", userkey );
//        params.addQueryStringParameter("str", str );
//        params.addQueryStringParameter("sign", sign );
//        HttpUtils http = new HttpUtils() ;
//        http.configCurrentHttpCacheExpiry(1000 * 10); //设置超时时间
//        try {
//            ResponseStream responseStream = http.sendSync(HttpRequest.HttpMethod.GET,
//                    url ,
//                    params ) ;
//            //int statusCode = responseStream.getStatusCode();
//            //Header[] headers = responseStream.getBaseResponse().getAllHeaders();
//            return responseStream.readString();
//        } catch (Exception e) {
//            LogUtils.e(e.getMessage(), e);
//        }
//        return null;
//    }
//
//    /**
//     * Post同步请求 必须在异步块儿中执行
//     * @param url
//     * @param userkey
//     * @param str
//     * @param sign
//     * @return
//     */
//    private String xutilsPostSync(String url , String userkey , String str , String sign ) {
//        RequestParams params = new RequestParams();
//        params.addQueryStringParameter("userkey", userkey );
//        params.addQueryStringParameter("str", str );
//        params.addQueryStringParameter("sign", sign );
//        HttpUtils http = new HttpUtils() ;
//        http.configCurrentHttpCacheExpiry(1000 * 10); //设置超时时间
//        try {
//            ResponseStream responseStream = http.sendSync(HttpRequest.HttpMethod.POST ,
//                    url ,
//                    params ) ;
//            //int statusCode = responseStream.getStatusCode();
//            //Header[] headers = responseStream.getBaseResponse().getAllHeaders();
//            return responseStream.readString();
//        } catch (Exception e) {
//            LogUtils.e(e.getMessage(), e);
//        }
//        return null;
//    }
}