package com.dinghang.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.dinghang.R;
import com.dinghang.activity.MainActivity;
import com.dinghang.activity.PlayerActivity;
import com.dinghang.constant.Constant;

public class NewsFragment extends BaseFragment {

	private Button button;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View newsLayout = inflater.inflate(R.layout.news_layout, container,
				false);
		button = (Button) newsLayout.findViewById(R.id.playerBtn);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(getActivity(), PlayerActivity.class);
				startActivity(intent);
			}
		});
		return newsLayout;
	}
	
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	
		MainActivity.currFragTag = Constant.FRAGMENT_FLAG_NEWS;
	}
	

}
