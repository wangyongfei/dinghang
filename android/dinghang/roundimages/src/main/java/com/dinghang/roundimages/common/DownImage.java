package com.dinghang.roundimages.common;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;

public class DownImage extends AsyncTask<String, Integer, Bitmap> {

	private ImageView imageView;
	private String url;


	public DownImage(ImageView textView, String url) {
		super();
		this.imageView = textView;
		this.url = url;
	}


	/**
	 * 这里的Integer参数对应AsyncTask中的第一个参数
	 * 这里的String返回值对应AsyncTask的第三个参数
	 * 该方法并不运行在UI线程当中，主要用于异步操作，所有在该方法中不能对UI当中的空间进行设置和修改
	 * 但是可以调用publishProgress方法触发onProgressUpdate对UI进行操作
	 */
//	@Override
//	protected Bitmap doInBackground(String... params) {
//		String url = params[0];
//		Bitmap bitmap = null;
//		try {
//			//加载一个网络图片
//			InputStream is = new URL(url).openStream();
//			bitmap = BitmapFactory.decodeStream(is);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return bitmap;
//	}


	/**
	 * 这里的String参数对应AsyncTask中的第三个参数（也就是接收doInBackground的返回值）
	 * 在doInBackground方法执行结束之后在运行，并且运行在UI线程当中 可以对UI空间进行设置
	 */
	@Override
	protected void onPostExecute(Bitmap result) {
		imageView.setImageBitmap(result);
	}


	@Override
	protected Bitmap doInBackground(String... params) {
		String url = params[0];
		Bitmap bitmap = null;
		try {
			//加载一个网络图片
			InputStream is = new URL(url).openStream();
			bitmap = BitmapFactory.decodeStream(is);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		return bitmap;
		return getRoundCornerImage(bitmap,60, HalfType.ALL);
	}

	//该方法运行在UI线程当中,并且运行在UI线程当中 可以对UI空间进行设置
	@Override
	protected void onPreExecute() {
//		textView.setText("开始执行异步线程");
	}


	/**
	 * 这里的Intege参数对应AsyncTask中的第二个参数
	 * 在doInBackground方法当中，，每次调用publishProgress方法都会触发onProgressUpdate执行
	 * onProgressUpdate是在UI线程中执行，所有可以对UI空间进行操作
	 */
	@Override
	protected void onProgressUpdate(Integer... values) {
		int vlaue = values[0];

	}

	/**
	 * 将图片的四角圆化
	 * @param bitmap 原图
	 * @param roundPixels 圆滑率
	 * @param half 是否截取半截
	 * @return
	 */
	public  Bitmap getRoundCornerImage(Bitmap bitmap, int roundPixels,HalfType half)
	{
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();

		Bitmap roundConcerImage = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);//创建一个和原始图片一样大小的位图
		Canvas canvas = new Canvas(roundConcerImage);//创建位图画布
		Paint paint = new Paint();//创建画笔

		Rect rect = new Rect(0, 0, width, height);//创建一个和原始图片一样大小的矩形
		RectF rectF = new RectF(rect);
		paint.setAntiAlias(true);// 抗锯齿

		canvas.drawRoundRect(rectF, roundPixels, roundPixels, paint);//画一个基于前面创建的矩形大小的圆角矩形
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));//设置相交模式
		canvas.drawBitmap(bitmap, null, rect, paint);//把图片画到矩形去

		switch (half) {
			case LEFT:
				return Bitmap.createBitmap(roundConcerImage, 0, 0, width - roundPixels, height);
			case RIGHT:
				return Bitmap.createBitmap(roundConcerImage, width - roundPixels, 0, width - roundPixels, height);
			case TOP: // 上半部分圆角化 “- roundPixels”实际上为了保证底部没有圆角，采用截掉一部分的方式，就是截掉和弧度一样大小的长度
				return Bitmap.createBitmap(roundConcerImage, 0, 0, width, height - roundPixels);
			case BOTTOM:
				return Bitmap.createBitmap(roundConcerImage, 0, height - roundPixels, width, height - roundPixels);
			case ALL:
				return roundConcerImage;
			default:
				return roundConcerImage;
		}
	}

	/**
	 * 转换图片成圆形
	 * @param bitmap 传入Bitmap对象
	 * @return
	 */
	public Bitmap toRoundBitmap(Bitmap bitmap) {
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		float roundPx;
		float left,top,right,bottom,dst_left,dst_top,dst_right,dst_bottom;
		if (width <= height) {
			roundPx = width / 2;
			top = 0;
			bottom = width;
			left = 0;
			right = width;
			height = width;
			dst_left = 0;
			dst_top = 0;
			dst_right = width;
			dst_bottom = width;
		} else {
			roundPx = height / 2;
			float clip = (width - height) / 2;
			left = clip;
			right = width - clip;
			top = 0;
			bottom = height;
			width = height;
			dst_left = 0;
			dst_top = 0;
			dst_right = height;
			dst_bottom = height;
		}

		Bitmap output = Bitmap.createBitmap(width,
				height, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect src = new Rect((int)left, (int)top, (int)right, (int)bottom);
		final Rect dst = new Rect((int)dst_left, (int)dst_top, (int)dst_right, (int)dst_bottom);
		final RectF rectF = new RectF(dst);

		paint.setAntiAlias(true);

		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(bitmap, src, dst, paint);
		return output;
	}


}