<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%-- <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.edasoft.com/tags/efa" prefix="efa"%>  --%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
 <c:set var="cssBasePath" value="${contextPath}/styles" />
<c:set var="scriptBasePath" value="${contextPath}/scripts" />
<c:set var="imagesBasePath" value="${contextPath}/images" />
<c:set var="imagesHeadPath" value="${contextPath}/images/headImg/" />
<c:set var="mongoImgPath" value="http://www.fcworld.net/dhpics/"/>
<c:set var="basePath" value="http://127.0.0.1:8558${contextPath}"/>
<c:set var="videoPath" value="${contextPath}/video/" /> 
<%
	response.setHeader("Cache-Control","no-cache,no-strore");
	response.setHeader("Pragma","no-cache");
	response.setDateHeader("Expires",-1); 
%>