<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="loading">
<head>
    <%@ include file="/WEB-INF/views/common/layout/meta.jsp"%>
    <%@ include file="/WEB-INF/views/common/layout/links.jsp"%>
    <%--<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />--%>
    <%--<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />--%>
    <%--<meta name="viewport" content="width=device-width, initial-scale=0.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />--%>
    <%--<meta name="keywords" content="飞  阳  园 fcworld Javascript css" />--%>
    <%--<meta name="description" content="fcworld">--%>
    <%--<meta name="author" content="fcworld" />--%>
    <%--<link rel="stylesheet" type="text/css" href="css/style.css" />--%>
    <%--<link rel="stylesheet" type="text/css" href="css/css3/style.css" />--%>
    <%--<meta content="yes" name="apple-mobile-web-app-capable">--%>
    <%--<meta content="black" name="apple-mobile-web-app-status-bar-style">--%>
    <%--<meta content="telephone=no" name="format-detection">--%>
    <%--<link rel="apple-touch-icon-precomposed" href="http://www.17sucai.com/static/images/favicon.ico">--%>
    <script>
        var logined = 0
    </script>
    <title>飞阳园</title>
</head>

<body>
<script>
    var now_page = 1, search_value = '';
    function search_detial(id){
       window.location.href ="${contextPath}/news_detial?id="+id;
    }
    function submitUser(){
        $.ajax({
            url:"register_cust",
            type:"post",
            data:{"userName":$("#userName").val(),"password":$("#password").val()},
            success:function(data){
                window.location.href ="${contextPath}/index";
            },
            error:function(e){
                alert(e);
            }
        });
    }
</script>

<div id="menu" style="padding-top: 5px;">
    <ul>
        <li class="nav_index menu_cur"><a href="${contextPath}/index"><i></i><span>首页</span><b></b><div class="clear"></div></a></li>
        <li class="nav_about"><a href="${contextPath}/about_us"><i></i><span>关于我们</span><b></b><div class="clear"></div></a></li>
    </ul>
</div>
<div id="user">
    <div class="account">
        <div class="login_b_t">
            <div class="pd10">
                <div class="fl">还没有账号<a id="reg_now" href="" onclick="return false;">立即注册</a></div>
                <div class="fr"><a href="#">忘记密码?</a></div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div class="pd10">
        <form method="post" action="">
            <div class="login_b_i">
                <div class="login_input">
                    <div class="login_user">
                        <input type="text" id="userName" placeholder="帐号" /><i></i></div>
                    <div class="login_password">
                        <input type="password" id="password" name="password" placeholder="密码" /><i></i></div>
                </div>
            </div>
            <a class="login_submit" onclick="submitUser();">登录</a>
        </form>
    </div>
</div>
<div id="header">
    <div class="wrap">
        <i class="menu_open"></i>
        <div class="logo" style="text-align: center;"><span style="line-height: 42px; color: white; font-size: 16px;">飞阳园</span></div>
        <i class="search_open"></i>
    </div>
    <div class="logo_msk"></div>
</div>
<div id="container">
    <div id="search">
        <form method="post" action="${contextPath}/index">
        <table width="100%" border="0" cellspacing="0">
            <tr>
                <td>
                    <input type="text" name="searchContent" style="width: 70%;" placeholder="标题/文章"/>
                    <input type="submit" value="搜索" style="width: 20%; margin-left: 10px;" />
                </td>
            </tr>
        </table>
        </form>
    </div>
    <div id="content">
        <div id="list">
            <c:forEach var="item" items="${list}" varStatus="status">
            <ul>
                <li>
                    <div class="wrap">
                        <a class="alist" href="#" onclick="search_detial('${item.id}')">
                            <div class="list_litpic fl"><img src="${mongoImgPath}${item.titleImg}" /></div>
                            <div class="list_info">
                                <h4>${item.title}</h4>
                                <h5>by<span>经验分享</span><em>(其他)</em></h5>
                                <div class="list_info_i">
                                    <dl class="list_info_views">
                                        <dt></dt>
                                        <dd>2309</dd>
                                        <div class="clear"></div>
                                    </dl>
                                    <dl class="list_info_comment">
                                        <dt></dt>
                                        <dd>5</dd>
                                        <div class="clear"></div>
                                    </dl>
                                    <dl class="list_info_like">
                                        <dt></dt>
                                        <dd>8</dd>
                                        <div class="clear"></div>
                                    </dl>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </a>
                    </div>
                </li>
            </ul>
            </c:forEach>
            <div class="list_loading"><i></i><span>努力加载中...</span></div>
        </div>
    </div>
    <div class="push_msk"></div>
</div>
<script language="javascript" src="${scriptBasePath}/zepto.min.js"></script>
<script language="javascript" src="${scriptBasePath}/fx.js"></script>
<script language="javascript" src="${scriptBasePath}/script.js"></script>

</body>

</html>