package com.dh.client.controller;

import com.dh.client.util.StringUtil;
import com.dh.sys.mongo.FilePicsEntity;
import com.dh.sys.mongo.MongoService;
import com.dh.sys.news.model.DhNews;
import com.dh.sys.news.service.DhNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Administrator on 2016/9/11.
 */
@Controller
public class IndexController {

    @Autowired
    private DhNewsService service;

    @Autowired
    private MongoService mongoService;

    @RequestMapping("/index")
    @ResponseBody
    public ModelAndView search(
            @RequestParam(value="searchContent", defaultValue = "") String searchContent,
            @RequestParam(value="page", defaultValue="1") Integer page,
            @RequestParam(value="rows", defaultValue="10")  Integer rows){

        ModelAndView modelAndView = new ModelAndView("index");

        DhNews record = new DhNews();
        if(page>=1){
            page = (page-1)*rows;
        }
        List<DhNews> list = service.findAllListPage(record,page,rows); //获取所有用户数据
        int total = service.count(record);
        for(int i = 0; i<list.size(); i++){
            FilePicsEntity filePicsEntity =mongoService.findById(list.get(i).getId());
            list.get(i).setMark(new String(filePicsEntity.getContent()));
        }
        modelAndView.addObject("pageNo", page);
        modelAndView.addObject("total", total);
        modelAndView.addObject("list", list);

        return modelAndView;

    }

    @RequestMapping("/news_detial")
    @ResponseBody
    public ModelAndView search(
            @RequestParam(value="id", defaultValue = "") String id){

        ModelAndView modelAndView = new ModelAndView("news/detial");
        if(!StringUtil.isNullOrEmpty(id)){
            DhNews record = service.selectById(id);
            FilePicsEntity filePicsEntity =mongoService.findById(id);
            record.setUpdateDate(new String(filePicsEntity.getContent()));
            modelAndView.addObject("vo", record);
        }
        return modelAndView;
    }

    @RequestMapping("/about_us")
    @ResponseBody
    public ModelAndView about_us(){
        ModelAndView modelAndView = new ModelAndView("us/us");
        return modelAndView;
    }

}
