package com.dh.client.controller;

import com.dh.client.util.DhUtil;
import com.dh.client.util.StringUtil;
import com.dh.sys.custinfo.model.CustInfo;
import com.dh.sys.custinfo.service.CustInfoService;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Administrator on 2016/9/16.
 */
@Controller
public class CustInfoController {

    @Autowired
    private CustInfoService custInfoService;

    @RequestMapping("/register_cust")
      @ResponseBody
      public void search(
            HttpServletResponse response,
            @RequestParam(value="userName") String  userName,
            @RequestParam(value="password")  String password){

//        ModelAndView modelAndView = new ModelAndView("/index");
        CustInfo custInfo = new CustInfo();
        custInfo.setUserName(userName);
        //custInfo.setId(DhUtil.getUUID());
        custInfo.setPassword(password);
//        custInfo.setUserCode();
      //  custInfo.setCreateTime(StringUtil.getCurrLongDate());
        custInfoService.select(custInfo);
       // custInfoService.save(custInfo);

        try {
            PrintWriter out = response.getWriter();
            out.write(returnMsg("删除成功",0));
        } catch (IOException e) {
            e.printStackTrace();
        }
//        modelAndView.addObject("pageNo", page);
//        modelAndView.addObject("total", total);
//        modelAndView.addObject("list", list);

//        return modelAndView;

    }



    private String returnMsg(String msg, Integer flag){
        JsonObject object=new JsonObject();
        object.addProperty("msg", msg);
        object.addProperty("flag", flag);
        return object.toString();
    }
}
