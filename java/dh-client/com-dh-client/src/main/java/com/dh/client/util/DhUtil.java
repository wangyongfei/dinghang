package com.dh.client.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;




/**
 * 
 * @项目名称：TYBCustClient
 * @类名称：SrxUtil.java
 * @类描述：公共校验方法
 * @创建人：Administrator
 * @创建时间：2014-12-8下午2:56:09
 * @修改人：Administrator
 * @修改时间：2014-12-8下午2:56:09
 * @修改备注：
 * @版本：V1.0
 */
public class DhUtil {
//	private static Provider provider = new BouncyCastleProvider();
//	static {
//		Security.addProvider(provider);
//	}

	/**
	 * 潜在客户编号
	 */
	public static String Procedure_Parameter_CUSTNO="CUSTINFO";
	/**
	 * 客户业务信息编号
	 */
	public static String Procedure_Parameter_CUSTBUSSINESS="CUSTBUSSINESS";
	/**
	 * 客户上下游信息编号
	 */
//	public static String Procedure_Parameter_CUSTUPDOWN="CUSTUPDOWN";

	/**
	 * 联系人编号
	 */
	public static String Procedure_Parameter_LINKMAN="LINKMAN";
	/**
	 * 联系人关系编号
	 */
	public static String Procedure_Parameter_LINKMANREL="LINKMANREL";
	
	
	
	/**
	 * 推送处理流水号
	 */
	public static String Procedure_Parameter_DATA_PUSH="DATA_PUSH";
	/**
	 * 线索id
	 */
	public static String Procedure_Parameter_LEADOPP="LEADOPP";
	/**
	 * 线索计划流水号
	 */
	public static String Procedure_Parameter_LEADOPPS_ACTIVITY="LEADOPPS_ACTIVITY";
	public static String System_SpecialChar_pwd="' -- ;";
	public static String System_SpecialChar = "% < > & ? | & ; $ %% @ ' \" \\\' \\\" <> () + CR LF \\";
	private static Pattern floatNumericPattern = Pattern
			.compile("^[0-9\\-\\.]+$");

	/**
	 * @Title: getUID
	 * @Description:得uid
	 * @return String
	 * @throws
	 */
	public static String getUUID() {
		return UUID.randomUUID().toString().replace("-", "");
	}

	/**
	 * 
	 * @Title: getAge
	 * @Description:根据生日得年龄
	 * @param birdth
	 * @return int
	 * @throws
	 */
	@SuppressWarnings("deprecation")
	public static int getAge(Date birdth) {
		int iAge = 0;
		if (birdth != null) {
			Date nDate = new Date();
			int iNowYear = nDate.getYear() + 1900;
			int iNowMonth = nDate.getMonth() + 1;
			int iNowDay = nDate.getDate();
			// System.out.println(iNowYear+" "+iNowMonth+" "+iNowDay);

			int iYear = birdth.getYear() + 1900;
			int iMonth = birdth.getMonth() + 1;
			int iDay = birdth.getDate();

			iAge = iNowYear - iYear - 1;
			if (iNowMonth >= iMonth) {
				if (iNowMonth > iMonth)
					iAge = iAge + 1;
				else {
					if (iNowDay >= iDay) {

						iAge = iAge + 1;
					}
				}

			}

		}
		// System.out.println("age="+iAge);
		return iAge;
	}

	/**
	 * 判断是否浮点数字表示
	 * 
	 * @param src
	 *            源字符串
	 * @return 是否数字的标志
	 */
	public static boolean isFloatNumeric(String src) {
		boolean return_value = false;
		if (src != null && src.length() > 0) {
			Matcher m = floatNumericPattern.matcher(src);
			if (m.find()) {
				return_value = true;
			}
		}
		return return_value;
	}

	/**
	 * 
	 * @param mobilePhone 手机号
	 * @return
	 */
	public static boolean checkMobilePhoneValid(String mobilePhone) {
		boolean b = false;
		String regex = "^1\\d{10}$";// 移动电话正则
		b = mobilePhone.matches(regex);
		return b;
	}
	/**
	 * 判断身份证
	 * @param cardid 身份证号码
	 * @return boolean
	 */
	public static boolean checkcardid(String cardid){
		boolean result=false;
		if(cardid.length()==15){
			String regex15="^[1-9]{1,15}$　";
			result= cardid.matches(regex15);
		}
		if(cardid.length()==18){
			String regex18="^[1-9]{1,18}$";
			result= cardid.matches(regex18);
			if(result){
				String[] arrCh ={"1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"};
				String endstr=regex18.substring(regex18.length()-1);
				for(int i=0;i<arrCh.length;i++){
					if(arrCh[i]!=endstr){
						result=false;
					}
				}
			}
		}
		return result;
	}
	/**
	 * 判断是否是正整数
	 * @param str 验证字符串
	 * @param len 限制位数
	 * @return
	 */
	public static boolean checknumber(String str,String len){
		boolean result=false;
		String varstr="";
		if(len.length()==0 && len==""){
			 varstr="^[0-9]$";
		}else{
			varstr="^[0-9]{1,"+len+"}$";
		}
		result=str.matches(varstr);
		return result;
	}
	/**
	 * 
	 * @Title: checkMobilePhone
	 * @Description: 检查固定电话格式
	 * @param mobilePhone
	 * @return boolean
	 * @throws
	 */
	public static boolean checkWorkPhone(String workPhone) {
		boolean b = false;
		String regex = "^(\\d{3,4}\\-)(\\d{6,8})(\\-\\d{1,6})?$";
		b = workPhone.matches(regex);
		return b;
	}

	/**
	 * enail正则表达式
	 * 
	 * @param email
	 * @return
	 */
	public static boolean checkEmail(String email) {
		boolean falg = false;
		String regex = "^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)+$";
		falg = email.matches(regex);
		return falg;
	}

	/**
	 * MSN格式
	 * 
	 * @param msn
	 * @return
	 */
	public static boolean checkMSN(String msn) {
		//boolean falg = false;
		// String regex = "=/(\\S)+[@]{1}(\\S)+[.]{1}(\\w)+/";
		// falg = msn.matches(regex);
		// return falg;
		 return Pattern.matches("(\\S)+[@]{1}(\\S)+[.]{1}(\\w)+", msn);
	}

	// /**
	// * 打开文件
	// *
	// * @param FilePath
	// */
	// public static void openFile(String FilePath) {
	// try {
	// Runtime.getRuntime().exec(
	// "rundll32 url.dll FileProtocolHandler " + FilePath);
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }

	/**
	 * 复制对象
	 * 
	 * @param src
	 * @param dest
	 */
	public static void copyObject(Object src, Object dest) {
		Class<? extends Object> classSrc = src.getClass();
		Class<? extends Object> classDest = dest.getClass();
		Field[] arySrcFields = classSrc.getFields();
		Field[] aryDestFields = classDest.getFields();
		for (int i = 0; i < arySrcFields.length; i++) {
			Field fieldSrc = arySrcFields[i];
			for (int j = 0; j < aryDestFields.length; j++) {
				Field fieldDest = aryDestFields[j];

				if (fieldSrc.getName().equalsIgnoreCase(fieldDest.getName())) {
					try {
						fieldDest.set(dest, fieldSrc.get(src));
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
					break;
				}
			}
		}
	}

	/**
	 * 删除指定目录下的所有文件并 根绝条件来决定是否删除根文件夹
	 * 
	 * @param dir
	 *            文件对象
	 * @param falg
	 *            是否删除根文件夹
	 * @return
	 */
	@SuppressWarnings("unused")
	private static boolean deleteFolder(File dir, boolean falg) {
		File filelist[] = dir.listFiles();
		int listlen = filelist.length;
		for (int i = 0; i < listlen; i++) {
			if (filelist[i].isDirectory()) {
				deleteFolder(filelist[i], true);
			} else {
				if (!filelist[i].delete())
					return false;
			}
		}
		if (falg) {
			if (!dir.delete())
				return false;// 删除当前目录
			else
				return true;
		} else {
			return true;
		}
	}

	/***
	 * 限制数值小数位数
	 * 
	 * @param dvar要格式化小数位数的数值
	 * @param sca保留小数位数
	 * @return 保留dvar位小数的数值
	 */
	public static double FormatDouble(double dvar, int sca) {
		return BigDecimal.valueOf(dvar).setScale(sca, BigDecimal.ROUND_HALF_UP)
				.doubleValue();
	}

	/***
	 * 返回非科学计数法格式数值字符串
	 * 
	 * @param d
	 *            科学计数法格式数值
	 * @return 非科学计数法格式数值字符串
	 */
	public static String NumberFormat(double d) {
		java.text.NumberFormat nf = java.text.NumberFormat.getInstance();
		nf.setGroupingUsed(false);
		return nf.format(d);
	}

	/**
	 * 判断路径是否存在如果不存在则创建
	 * 
	 * @param sPath
	 *            需要检索路径
	 * @param isCreate
	 *            如果不存在是否创建
	 * @return
	 */
	public static boolean isPathExist(String sPath, boolean isCreate) {
		File file = new File(sPath);
		if (file.exists()) {
			return true;
		} else {
			if (isCreate)
				file.mkdirs();
			return false;
		}
	}
	
	/**
	 * 判断文件是否存在
	 * @param sPath
	 * @return
	 */
	public static boolean  IsExistFile(String sPath){
		File file = new File(sPath);
		if (file.exists()) {
			return true;
		} 
		return false;
	}

	// /**
	// *
	// * @Title: getCode
	// * @Description: 取枚举数据项列表 返回vector数据集
	// * @param codeType
	// * @param isSelectAll
	// * 选项中是否有请选择选项 VALUE是""
	// * @return
	// */
	// public static Vector getCodeByCodeType(String codeType,
	// boolean isPleaseSelect) {
	// if (codeType == null || "".equals(codeType)) {
	// return null;
	// }
	// List<SysCode> lstCode = SysCode.getCodes(codeType);
	// Vector v = null;
	// if (isPleaseSelect) {
	// v = new Vector();
	// v.add(new CheckBoxPo("", "请选择"));
	// }
	// if (lstCode != null) {
	// SysCode sysCode = null;
	// for (int i = 0; i < lstCode.size(); i++) {
	// sysCode = lstCode.get(i);
	// if (v == null)
	// v = new Vector();
	// v.add(new CheckBoxPo(sysCode.getCodeId(), sysCode.getCodeDesc()));
	// }
	// }
	// return v;
	//
	// }

	/**
	 * MD5加密
	 */
	public static String md5Digest(String src) throws Exception {
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] b = md.digest(src.getBytes("gb2312"));
		return byte2HexStr(b);
	}

	public static String md5Digest(String src, String encoding)
			throws Exception {
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] b = md.digest(src.getBytes(encoding));
		return byte2HexStr(b);
	}

	private static String byte2HexStr(byte[] b) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < b.length; i++) {
			String s = Integer.toHexString(b[i] & 0xFF);
			if (s.length() == 1) {
				sb.append("0");
			}
			sb.append(s.toUpperCase());
		}
		return sb.toString();
	}
	
	/**
	 * 验证字符串长度是否符合要求，一个汉字等于3个字符
	 * 
	 * @param strParameter
	 *            要验证的字符串
	 * @param iMaxLength
	 *            验证的长度
	 * @return 符合长度true 超出范围false
	 */
	public static boolean validateStrLength(String inputStr, int iMaxLength) {
		if (StringUtil.isNullOrEmpty(inputStr))
			return true;
		int valueLength = 0;
		String chinese = "[\u0391-\uFFE5]";
		for (int i = 0; i < inputStr.length(); i++) {
			String temp = inputStr.substring(i, i + 1);
			if (temp.matches(chinese)) {
				valueLength += 3;
			} else {
				valueLength += 1;
			}
		}
		if (valueLength > iMaxLength)
			return false;
		return true;
	}

	/**
	 * 验证数值是否符合要求 NULL 或 "" 返回false
	 * 
	 * @param sInputNumStr
	 *            待验证的数值字符串
	 * @param integerLength
	 *            数值的整数位长度
	 * @param iSmallLength
	 *            数值的小数位长度
	 * @return true:符合指定大小 false: 溢出
	 */
	public static boolean validateNumericalSize(String sInputNumStr,
			int integerLength, int iSmallLength) {
		if (StringUtil.isNullOrEmpty(sInputNumStr)) {
			return false;
		}
		try {
			new BigDecimal(sInputNumStr);
		} catch (Exception e) {
			return false;
		}
		String[] arrStr = sInputNumStr.split("[.]");
		if (arrStr[0].length() > integerLength) {
			return false;
		}
		if (arrStr.length > 1 && arrStr[1].length() > iSmallLength) {
			return false;
		}
		return true;
	}

	/**
	 * 判断输入的数值是否大于Integer
	 * 
	 * @param sIntegerValue
	 *            Integer类型的字符串
	 * @return true：是Integer类型 false：非Integer类型或数值过大
	 */
	public static boolean validateIntegerSize(String sIntegerValue) {
		try {
			Integer.parseInt(sIntegerValue);
		} catch (Exception e) {
			return false;
		}

		return true;
	}

	/**
	 * 判断此数据类型是否为数值类型
	 * 
	 * @param
	 * @return 是 返回true；否返回 false
	 */
	public static boolean isDouble(Object b) {
		try {
			Double.valueOf(String.valueOf(b));
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static String StringCheck(String str) throws Exception {
		if (StringUtil.isNullOrEmpty(str)) {
			throw new Exception("参数不能为null");
		}

		String temp = str;
		int strLength = temp.trim().length();
		int result = -1;
		int DivResult = 0;
		StringBuilder sb = null;
		if (strLength > 0) {
			do {
				DivResult = strLength / 64;
				result = strLength % 64;

				sb = new StringBuilder();
				for (int i = 0; i < DivResult; i++) {
					sb.append(md5Digest(temp.substring(i * 64, 64 * (i + 1)),
							"utf-8").toLowerCase());
				}
				if (result > 0) {
					sb.append(md5Digest(
							temp.substring((DivResult * 64), strLength),
							"utf-8").toLowerCase());
				}
				temp = sb.toString();
				strLength = temp.length();
			} while (strLength > 32);
		} else {
			throw new Exception("参数不能为null");
		}
		return temp;
	}

	@SuppressWarnings("unused")
	private static String getPath(String fileName) {
		int p1 = fileName.lastIndexOf("/");
		int p2 = fileName.lastIndexOf("\\");

		if (p1 < 0 && p2 < 0)
			return null;
		else if (p1 > p2)
			return fileName.substring(0, p1);
		else if (p2 > p1)
			return fileName.substring(0, p2);

		return null;
	}

	public static void appendMethodA(String fileName, String content) {
		try {
			// 打开一个随机访问文件流，按读写方式
			RandomAccessFile randomFile = new RandomAccessFile(fileName, "rw");
			// 文件长度，字节数
			long fileLength = randomFile.length();
			// 将写文件指针移到文件尾。
			randomFile.seek(fileLength);
			randomFile.writeBytes(content);
			randomFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	

	/***
	 * 复制文件
	 * @param sor 源文件
	 * @param des 目标文件
	 * @return 成功返回true 失败false
	 */
	public static boolean copyFile(String sor, String des)
	{
		int length=2097152;
		FileInputStream in = null;
		FileOutputStream out = null;
		try {
			in = new FileInputStream(sor);
			out=new FileOutputStream(des);
			byte[] buffer=new byte[length];
			int ins=in.read(buffer);
			while(ins!=-1){
					out.write(buffer,0,ins);
					ins=in.read(buffer);
			}			
			out.flush();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			try {
				in.close();
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}						
		}
		return false;
	}
	/**
	 * 检查输入的字符串是否含有特殊字符
	 * @param inputStr 待检验的字符串
	 * @return true:含有特殊字符 false：未包含特殊字符串
	 */
	public static boolean checkSpecialchar_pwd(String inputStr){
		if(StringUtil.isNullOrEmpty(inputStr))
			return false;
		String strField = System_SpecialChar_pwd;//"<>&";// 非法字符集
		String[] sSpecialChar_str = strField.split(" ");
		for(int i=0;i<sSpecialChar_str.length;i++){
			if(inputStr.indexOf(sSpecialChar_str[i])>=0){
				return true;
			}
		}
		return false;
	}
	
	public static boolean checkMobilePhone(String value){
		if(StringUtil.isNullOrEmpty(value)) 
			return false;
		//String regExp = "^((13\\d{1})|(18\\d{1})|(15[0-25-9])|(145)|(147))\\d{8}$";
		String regExp = "^((13\\d{1})|(145)|(147)|(15[0-35-9])|(170)|(17[6-8])|(18\\d{1}))\\d{8}$";
		return 	value.matches(regExp);
	}
	/**
	 * 检查输入的字符串是否含有特殊字符
	 * @param inputStr 待检验的字符串
	 * @return true:含有特殊字符 false：未包含特殊字符串
	 */
	public static boolean checkSpecialchar(String inputStr){
		if(StringUtil.isNullOrEmpty(inputStr))
			return false;
//		String strField = Messages.getStringById(R.string.System_SpecialChar);//"<>&";// 非法字符集  2012-12-08 wyb
		String strField = System_SpecialChar;//"<>&";// 非法字符集
//		for (int i = 0; i < inputStr.length(); i++) {
//			char c = inputStr.charAt(i);
//			if (strField.indexOf(c) != -1) {
//				return true;
//			}
//		}
		if(StringUtil.isNullOrEmpty(strField)){
			return false;
		}
		String[] sSpecialChar_str = strField.split(" ");
		for(int i=0;i<sSpecialChar_str.length;i++){
			if(inputStr.indexOf(sSpecialChar_str[i])>=0){
				return true;
			}
		}
		return false;
	}
	/**
	 * 检查字符的合法性  是否含有特殊字符 及 字符长度是否符合要求 ，一个汉字等于3个字符
	 * @param inputStr 要验证的字符串
	 * @param iMaxLength 验证的长度
	 * @return 0:合法 1:含有特殊字符 2：长度超出预设长度
	 */
	public static String checkStrLegitimacy(String inputStr,int iMaxLength){
		if(checkSpecialchar(inputStr)){
			return "1";
		}
		if(!validateStrLength(inputStr,iMaxLength)){
			return "2";
		}
		return "0";
	}
	/**
	 * 生成流水号
	 * @param i_seq_name 传入参数
	 * @return
	 */
//	public static String getSerialNumber(String i_seq_name){
//		Connection con = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//		CallableStatement proc = null;
//		String rntStr = "";
//		try {
//			con = ConnectionManager.getConnection();
//			proc = con.prepareCall("{ call GRP.pro_sequence(?,?) }");
//			proc.setString(1, i_seq_name);
//			proc.registerOutParameter(2, Types.VARCHAR);
//			proc.execute();
//			String testPrint = proc.getString(2);
//			rntStr = testPrint;
////			System.out.println("=testPrint=is="+testPrint);
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} finally {
//			if (rs != null) {
//				try {
//					rs.close();
//					if (pstmt != null) {
//						pstmt.close();
//					}
//					if (con != null) {
//						con.close();
//					}
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
//			}
//
//		}
//		return rntStr;
//	}
	
	/**
	 * @描述 将List<Integer> 转换为int[]
	 * @param 
	 * @return 
	 * @author zzy
	 * @创建时间 2014-3-25 
	 */
	public static int[] IntegerList2IntArray(List<Integer> array)
    {
        if(array == null)
            return null;
        if(array.size() == 0)
            return  new int[0];
        int result[] = new int[array.size()];
        for(int i = 0; i < array.size(); i++)
            result[i] = array.get(i);
        return result;
    }
	
	public static boolean IsNumberType(String str){
		Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(str);
		return isNum.matches();
	}
	
	public static boolean IsStringType(String str){
		Pattern pattern = Pattern.compile("[a-zA-Z]*");
        Matcher isNum = pattern.matcher(str);
		return isNum.matches();
	}
	
	/**
	 * 加密
	 * @param str
	 * @return
	 */
	public static String getTwoEncryptionPStr(String str){
		if(StringUtil.isNullOrEmpty(str)) return null;
		String[] strAry = new String[str.length()];
		int count  = 0;
		StringBuffer sb = new StringBuffer(1024);
		//倒序
		for(int i = str.length()-1; i >=0; i--){
			char sChar = str.charAt(i);
			String strCha = String.valueOf(sChar);
			String cha = strCha;
			if(IsNumberType(strCha)){
				cha = String.valueOf(sChar-'0');
			}
			if(IsStringType(strCha)){
				cha = String.valueOf((char)(sChar));
			}
			strAry[count++] = cha;
		}
		
		//前半部分与后半部分交换位置
		//后半部分
		for(int i = strAry.length/2+1; i < strAry.length; i++){
			sb.append(strAry[i]);
		}
		//前半部分
		for(int i = 0; i<=strAry.length/2; i++){
			sb.append(strAry[i]);
		}
		return sb.toString();
	}
	/**
	 * 解密
	 * @param str
	 * @return
	 */
	public static String getTwoDecipheringStr(String str){
		if(StringUtil.isNullOrEmpty(str)) return null;
		
		StringBuffer sb = new StringBuffer(1024);
		for(int i = str.length()-1; i >= 0; i--){
			char sChar = str.charAt(i);
			String strCha = String.valueOf(sChar);
			String cha = strCha;
			if(IsNumberType(strCha)){
				cha = String.valueOf((sChar)-'0');
			}
			if(IsStringType(strCha)){
				cha = String.valueOf((char)(sChar));
			}
			sb.append(cha);
		}
		
		String[] strAry = new String[str.length()];
		StringBuffer sb1 = new StringBuffer(1024);
		for(int i = 0; i < sb.toString().length(); i++){
			char sChar = sb.toString().charAt(i);
			strAry[i]= String.valueOf(sChar);
		}
		//前半部分与后半部分交换位置
		//后半部分
		for(int i = strAry.length/2+1; i < strAry.length; i++){
			sb1.append(strAry[i]);
		}
		//前半部分
		for(int i = 0; i<=strAry.length/2; i++){
			sb1.append(strAry[i]);
		}		
		return sb1.toString();
	}
	
	
	public static String getStringNoBlank(String str) {      
        if(str!=null && !"".equals(str)) {
        	 Pattern p = Pattern.compile("\\s*|\t|\r|\n");      
             Matcher m = p.matcher(str);      
             String strNoBlank = m.replaceAll("");      
             return strNoBlank; 
                
        }else {      
            return str;      
        }           
    }

	public static void main(String[] args) {
//		String str = "01239abc1236767678878qqqq";
//		System.out.println(str);
//		for(int i = 0; i< str.length(); i++){
//			String strCha = String.valueOf(str.charAt(i));
////			String.valueOf((pwd.charAt(i)-1)-48);
//			
//			String cha = String.valueOf(str.charAt(i));
//			if(IsNumberType(strCha)){
//				cha = String.valueOf((str.charAt(i)+1)-'0');
//			}
//			if(IsStringType(strCha)){
//				cha = String.valueOf((char)(str.charAt(i)+1));
//			}
//			System.out.println(cha);
//		}
//		System.out.println(getTwoEncryptionPStr(str));
//		System.out.println(getTwoDecipheringStr(getTwoEncryptionPStr(str)));
		
		System.out.println(checkMobilePhone("18301306265"));
		System.out.println(checkMobilePhone("17701306265"));
		System.out.println(checkMobilePhone("15301306265"));
		System.out.println(checkMobilePhone("17101306265"));
		System.out.println("DhUtil.main");
	}

}
