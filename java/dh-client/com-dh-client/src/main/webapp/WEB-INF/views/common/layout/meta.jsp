<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="zh-cn" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="Keywords" content="北京鼎航信科技有限公司" />
<meta name="Description" content="北京鼎航信科技有限公司" />
<meta name="Robots" content="all" />
<meta name="Author" content="北京鼎航信科技有限公司" />
<meta name="Copyright" content="北京鼎航信科技有限公司版权所有 2016" />
<META HTTP-EQUIV="pragma" CONTENT="no-cache"/> 
<META HTTP-EQUIV="Cache-Control" CONTENT="no-store,no-cache,must-revalidate"/>
<META HTTP-EQUIV="Expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0,target-densitydpi=medium-dpi, minimal-ui" name="viewport" /> 
<meta content="yes" name="apple-mobile-web-app-capable" /> 
<meta content="black" name="apple-mobile-web-app-status-bar-style" /> 
<meta content="telephone=no" name="format-detection" />

<meta http-equiv="expires" content="0">
<meta name="format-detection" content="telephone=no" />
<!-- <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, target-densitydpi=medium-dpi, minimal-ui"> -->
    