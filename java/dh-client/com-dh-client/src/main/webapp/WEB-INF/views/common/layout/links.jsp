<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/base.jsp"%>
<%-- <efa:resourceGroup type="css">
	<efa:resource path="/styles/wx/css/website.css"/> --%>
<%-- <efa:resourceGroup type="js">
	<efa:resource path="/resources/base/sea.js"/>	
	<efa:resource path="/resources/base/config.js"/>
	<efa:resource path="/scripts/wx/js/jquery.js"/>
	<efa:resource path="/scripts/wx/js/website.js"/>
</efa:resourceGroup> --%>
<script type="text/javascript" src="${scriptBasePath}/common/jquery-1.9.1.js"></script>
<link rel="stylesheet" type="text/css" href="${cssBasePath}/css/style.css" />
<link rel="stylesheet" type="text/css" href="${cssBasePath}/css/css3/style.css" />
<script type="text/javascript">
var jscontextPath="${contextPath}";
var jsPath="${path}";
var jsimagesBasePath="${imagesBasePath}";
var jsheadPath="${imagesHeadPath}";
var jsscriptBasePath="${scriptBasePath}";
var jsimagesServerPath="${imagesServerPath}";
var jsvideoPath = "${videoPath}";
</script>