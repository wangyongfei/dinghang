package com.dhsys.system.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 数据库条件操作类
*  create by  wyf at 2016-05-31
*/
public class SysMenuCriteria {

    /**
    * 排序字段
    */
    protected String orderByClause;

    /**
    * 数据库字段拼接集合
    */
    protected List<Criteria> oredCriteria;

    /**
    * 实例化对象的同时，为oredCriteria实例化
    */
    public SysMenuCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
    * 1.实例化对象的同时，为oredCriteria实例化
    * 2.实例化对象的同时，为排序字段orderByClause赋值
    * @param example
    */
    protected SysMenuCriteria(SysMenuCriteria example) {
        this.orderByClause = example.orderByClause;
        this.oredCriteria = example.oredCriteria;
    }


    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }


    public String getOrderByClause() {
        return orderByClause;
    }


    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }


    public void or(Criteria criteria) {
     oredCriteria.add(criteria);
    }


    public Criteria createCriteria() {
        /**
        * 获取criteria实例化对象
        */
        Criteria criteria = createCriteriaInternal();

        /**
        * 首次调用时如果数据库字段拼接集合为空，
        * 则获取一个新的实例对象加到该结合中
        */
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
    * 获取一个Criteria实例对象
    * @return
    */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }


    /**
    * 清空数据库字段拼接集合
    */
    public void clear() {
        oredCriteria.clear();
    }


    /**
    * 数据库字段拼接实体类
    */
    public static class Criteria {
    protected List<String> criteriaWithoutValue;

    protected List<Map<String, Object>> criteriaWithSingleValue;

    protected List<Map<String, Object>> criteriaWithListValue;

    protected List<Map<String, Object>> criteriaWithBetweenValue;

    protected Criteria() {
        super();
        criteriaWithoutValue = new ArrayList<String>();
        criteriaWithSingleValue = new ArrayList<Map<String, Object>>();
        criteriaWithListValue = new ArrayList<Map<String, Object>>();
        criteriaWithBetweenValue = new ArrayList<Map<String, Object>>();
    }

    public boolean isValid() {
        return criteriaWithoutValue.size() > 0
        || criteriaWithSingleValue.size() > 0
        || criteriaWithListValue.size() > 0
        || criteriaWithBetweenValue.size() > 0;
    }

    public List<String> getCriteriaWithoutValue() {
        return criteriaWithoutValue;
    }

    public List<Map<String, Object>> getCriteriaWithSingleValue() {
        return criteriaWithSingleValue;
    }

    public List<Map<String, Object>> getCriteriaWithListValue() {
        return criteriaWithListValue;
    }

    public List<Map<String, Object>> getCriteriaWithBetweenValue() {
        return criteriaWithBetweenValue;
    }

    protected void addCriterion(String condition) {
        if (condition == null) {
            throw new RuntimeException("Value for condition cannot be null");
        }
        criteriaWithoutValue.add(condition);
    }

    protected void addCriterion(String condition, Object value, String property) {
        if (value == null) {
            throw new RuntimeException("Value for " + property + " cannot be null");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("value", value);
        criteriaWithSingleValue.add(map);
    }

    protected void addCriterion(String condition, List<? extends Object> values, String property) {
        if (values == null || values.size() == 0) {
            throw new RuntimeException("Value list for " + property + " cannot be null or empty");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", values);
        criteriaWithListValue.add(map);
    }

    protected void addCriterion(String condition, Object value1, Object value2, String property) {
        if (value1 == null || value2 == null) {
            throw new RuntimeException("Between values for " + property + " cannot be null");
        }
        List<Object> list = new ArrayList<Object>();
        list.add(value1);
        list.add(value2);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", list);
        criteriaWithBetweenValue.add(map);
    }


    
    public Criteria andIdIsNull() {
        addCriterion("ID is null");
        return this;
    }

    public Criteria andIdIsNotNull() {
        addCriterion("ID is not null");
        return this;
    }

    public Criteria andIdEqualTo(String value) {
        addCriterion("ID =", value, "id");
        return this;
    }

    public Criteria andIdNotEqualTo(String value) {
        addCriterion("ID <>", value, "id");
        return this;
    }

    public Criteria andIdGreaterThan(String value) {
        addCriterion("ID >", value, "id");
        return this;
    }

    public Criteria andIdGreaterThanOrEqualTo(String value) {
        addCriterion("ID >=", value, "id");
        return this;
    }

    public Criteria andIdLessThan(String value) {
        addCriterion("ID <", value, "id");
        return this;
    }

    public Criteria andIdLessThanOrEqualTo(String value) {
        addCriterion("ID <=", value, "id");
        return this;
    }

    public Criteria andIdLike(String value) {
        addCriterion("ID like", value, "id");
        return this;
    }

    public Criteria andIdNotLike(String value) {
        addCriterion("ID not like", value, "id");
        return this;
    }

    public Criteria andIdIn(List<String> values) {
        addCriterion("ID in", values, "id");
        return this;
    }

    public Criteria andIdNotIn(List<String> values) {
        addCriterion("ID not in", values, "id");
        return this;
    }

    public Criteria andIdBetween(String value1, String value2) {
        addCriterion("ID between", value1, value2, "id");
        return this;
    }

    public Criteria andIdNotBetween(String value1, String value2) {
        addCriterion("ID not between", value1, value2, "id");
        return this;
    }

    
    public Criteria andNameIsNull() {
        addCriterion("NAME is null");
        return this;
    }

    public Criteria andNameIsNotNull() {
        addCriterion("NAME is not null");
        return this;
    }

    public Criteria andNameEqualTo(String value) {
        addCriterion("NAME =", value, "name");
        return this;
    }

    public Criteria andNameNotEqualTo(String value) {
        addCriterion("NAME <>", value, "name");
        return this;
    }

    public Criteria andNameGreaterThan(String value) {
        addCriterion("NAME >", value, "name");
        return this;
    }

    public Criteria andNameGreaterThanOrEqualTo(String value) {
        addCriterion("NAME >=", value, "name");
        return this;
    }

    public Criteria andNameLessThan(String value) {
        addCriterion("NAME <", value, "name");
        return this;
    }

    public Criteria andNameLessThanOrEqualTo(String value) {
        addCriterion("NAME <=", value, "name");
        return this;
    }

    public Criteria andNameLike(String value) {
        addCriterion("NAME like", value, "name");
        return this;
    }

    public Criteria andNameNotLike(String value) {
        addCriterion("NAME not like", value, "name");
        return this;
    }

    public Criteria andNameIn(List<String> values) {
        addCriterion("NAME in", values, "name");
        return this;
    }

    public Criteria andNameNotIn(List<String> values) {
        addCriterion("NAME not in", values, "name");
        return this;
    }

    public Criteria andNameBetween(String value1, String value2) {
        addCriterion("NAME between", value1, value2, "name");
        return this;
    }

    public Criteria andNameNotBetween(String value1, String value2) {
        addCriterion("NAME not between", value1, value2, "name");
        return this;
    }

    
    public Criteria andImageUrlIsNull() {
        addCriterion("IMAGE_URL is null");
        return this;
    }

    public Criteria andImageUrlIsNotNull() {
        addCriterion("IMAGE_URL is not null");
        return this;
    }

    public Criteria andImageUrlEqualTo(String value) {
        addCriterion("IMAGE_URL =", value, "imageUrl");
        return this;
    }

    public Criteria andImageUrlNotEqualTo(String value) {
        addCriterion("IMAGE_URL <>", value, "imageUrl");
        return this;
    }

    public Criteria andImageUrlGreaterThan(String value) {
        addCriterion("IMAGE_URL >", value, "imageUrl");
        return this;
    }

    public Criteria andImageUrlGreaterThanOrEqualTo(String value) {
        addCriterion("IMAGE_URL >=", value, "imageUrl");
        return this;
    }

    public Criteria andImageUrlLessThan(String value) {
        addCriterion("IMAGE_URL <", value, "imageUrl");
        return this;
    }

    public Criteria andImageUrlLessThanOrEqualTo(String value) {
        addCriterion("IMAGE_URL <=", value, "imageUrl");
        return this;
    }

    public Criteria andImageUrlLike(String value) {
        addCriterion("IMAGE_URL like", value, "imageUrl");
        return this;
    }

    public Criteria andImageUrlNotLike(String value) {
        addCriterion("IMAGE_URL not like", value, "imageUrl");
        return this;
    }

    public Criteria andImageUrlIn(List<String> values) {
        addCriterion("IMAGE_URL in", values, "imageUrl");
        return this;
    }

    public Criteria andImageUrlNotIn(List<String> values) {
        addCriterion("IMAGE_URL not in", values, "imageUrl");
        return this;
    }

    public Criteria andImageUrlBetween(String value1, String value2) {
        addCriterion("IMAGE_URL between", value1, value2, "imageUrl");
        return this;
    }

    public Criteria andImageUrlNotBetween(String value1, String value2) {
        addCriterion("IMAGE_URL not between", value1, value2, "imageUrl");
        return this;
    }

    
    public Criteria andUrlIsNull() {
        addCriterion("URL is null");
        return this;
    }

    public Criteria andUrlIsNotNull() {
        addCriterion("URL is not null");
        return this;
    }

    public Criteria andUrlEqualTo(String value) {
        addCriterion("URL =", value, "url");
        return this;
    }

    public Criteria andUrlNotEqualTo(String value) {
        addCriterion("URL <>", value, "url");
        return this;
    }

    public Criteria andUrlGreaterThan(String value) {
        addCriterion("URL >", value, "url");
        return this;
    }

    public Criteria andUrlGreaterThanOrEqualTo(String value) {
        addCriterion("URL >=", value, "url");
        return this;
    }

    public Criteria andUrlLessThan(String value) {
        addCriterion("URL <", value, "url");
        return this;
    }

    public Criteria andUrlLessThanOrEqualTo(String value) {
        addCriterion("URL <=", value, "url");
        return this;
    }

    public Criteria andUrlLike(String value) {
        addCriterion("URL like", value, "url");
        return this;
    }

    public Criteria andUrlNotLike(String value) {
        addCriterion("URL not like", value, "url");
        return this;
    }

    public Criteria andUrlIn(List<String> values) {
        addCriterion("URL in", values, "url");
        return this;
    }

    public Criteria andUrlNotIn(List<String> values) {
        addCriterion("URL not in", values, "url");
        return this;
    }

    public Criteria andUrlBetween(String value1, String value2) {
        addCriterion("URL between", value1, value2, "url");
        return this;
    }

    public Criteria andUrlNotBetween(String value1, String value2) {
        addCriterion("URL not between", value1, value2, "url");
        return this;
    }

    
    public Criteria andQtipIsNull() {
        addCriterion("QTIP is null");
        return this;
    }

    public Criteria andQtipIsNotNull() {
        addCriterion("QTIP is not null");
        return this;
    }

    public Criteria andQtipEqualTo(String value) {
        addCriterion("QTIP =", value, "qtip");
        return this;
    }

    public Criteria andQtipNotEqualTo(String value) {
        addCriterion("QTIP <>", value, "qtip");
        return this;
    }

    public Criteria andQtipGreaterThan(String value) {
        addCriterion("QTIP >", value, "qtip");
        return this;
    }

    public Criteria andQtipGreaterThanOrEqualTo(String value) {
        addCriterion("QTIP >=", value, "qtip");
        return this;
    }

    public Criteria andQtipLessThan(String value) {
        addCriterion("QTIP <", value, "qtip");
        return this;
    }

    public Criteria andQtipLessThanOrEqualTo(String value) {
        addCriterion("QTIP <=", value, "qtip");
        return this;
    }

    public Criteria andQtipLike(String value) {
        addCriterion("QTIP like", value, "qtip");
        return this;
    }

    public Criteria andQtipNotLike(String value) {
        addCriterion("QTIP not like", value, "qtip");
        return this;
    }

    public Criteria andQtipIn(List<String> values) {
        addCriterion("QTIP in", values, "qtip");
        return this;
    }

    public Criteria andQtipNotIn(List<String> values) {
        addCriterion("QTIP not in", values, "qtip");
        return this;
    }

    public Criteria andQtipBetween(String value1, String value2) {
        addCriterion("QTIP between", value1, value2, "qtip");
        return this;
    }

    public Criteria andQtipNotBetween(String value1, String value2) {
        addCriterion("QTIP not between", value1, value2, "qtip");
        return this;
    }

    
    public Criteria andLeafIsNull() {
        addCriterion("LEAF is null");
        return this;
    }

    public Criteria andLeafIsNotNull() {
        addCriterion("LEAF is not null");
        return this;
    }

    public Criteria andLeafEqualTo(Integer value) {
        addCriterion("LEAF =", value, "leaf");
        return this;
    }

    public Criteria andLeafNotEqualTo(Integer value) {
        addCriterion("LEAF <>", value, "leaf");
        return this;
    }

    public Criteria andLeafGreaterThan(Integer value) {
        addCriterion("LEAF >", value, "leaf");
        return this;
    }

    public Criteria andLeafGreaterThanOrEqualTo(Integer value) {
        addCriterion("LEAF >=", value, "leaf");
        return this;
    }

    public Criteria andLeafLessThan(Integer value) {
        addCriterion("LEAF <", value, "leaf");
        return this;
    }

    public Criteria andLeafLessThanOrEqualTo(Integer value) {
        addCriterion("LEAF <=", value, "leaf");
        return this;
    }

    public Criteria andLeafLike(Integer value) {
        addCriterion("LEAF like", value, "leaf");
        return this;
    }

    public Criteria andLeafNotLike(Integer value) {
        addCriterion("LEAF not like", value, "leaf");
        return this;
    }

    public Criteria andLeafIn(List<Integer> values) {
        addCriterion("LEAF in", values, "leaf");
        return this;
    }

    public Criteria andLeafNotIn(List<Integer> values) {
        addCriterion("LEAF not in", values, "leaf");
        return this;
    }

    public Criteria andLeafBetween(Integer value1, Integer value2) {
        addCriterion("LEAF between", value1, value2, "leaf");
        return this;
    }

    public Criteria andLeafNotBetween(Integer value1, String value2) {
        addCriterion("LEAF not between", value1, value2, "leaf");
        return this;
    }

    
    public Criteria andParentNodeIsNull() {
        addCriterion("PARENT_NODE is null");
        return this;
    }

    public Criteria andParentNodeIsNotNull() {
        addCriterion("PARENT_NODE is not null");
        return this;
    }

    public Criteria andParentNodeEqualTo(Integer value) {
        addCriterion("PARENT_NODE =", value, "parentNode");
        return this;
    }

    public Criteria andParentNodeNotEqualTo(Integer value) {
        addCriterion("PARENT_NODE <>", value, "parentNode");
        return this;
    }

    public Criteria andParentNodeGreaterThan(Integer value) {
        addCriterion("PARENT_NODE >", value, "parentNode");
        return this;
    }

    public Criteria andParentNodeGreaterThanOrEqualTo(Integer value) {
        addCriterion("PARENT_NODE >=", value, "parentNode");
        return this;
    }

    public Criteria andParentNodeLessThan(Integer value) {
        addCriterion("PARENT_NODE <", value, "parentNode");
        return this;
    }

    public Criteria andParentNodeLessThanOrEqualTo(Integer value) {
        addCriterion("PARENT_NODE <=", value, "parentNode");
        return this;
    }

    public Criteria andParentNodeLike(Integer value) {
        addCriterion("PARENT_NODE like", value, "parentNode");
        return this;
    }

    public Criteria andParentNodeNotLike(Integer value) {
        addCriterion("PARENT_NODE not like", value, "parentNode");
        return this;
    }

    public Criteria andParentNodeIn(List<Integer> values) {
        addCriterion("PARENT_NODE in", values, "parentNode");
        return this;
    }

    public Criteria andParentNodeNotIn(List<Integer> values) {
        addCriterion("PARENT_NODE not in", values, "parentNode");
        return this;
    }

    public Criteria andParentNodeBetween(Integer value1, Integer value2) {
        addCriterion("PARENT_NODE between", value1, value2, "parentNode");
        return this;
    }

    public Criteria andParentNodeNotBetween(Integer value1, String value2) {
        addCriterion("PARENT_NODE not between", value1, value2, "parentNode");
        return this;
    }

    
    public Criteria andFlagIsNull() {
        addCriterion("FLAG is null");
        return this;
    }

    public Criteria andFlagIsNotNull() {
        addCriterion("FLAG is not null");
        return this;
    }

    public Criteria andFlagEqualTo(Integer value) {
        addCriterion("FLAG =", value, "flag");
        return this;
    }

    public Criteria andFlagNotEqualTo(Integer value) {
        addCriterion("FLAG <>", value, "flag");
        return this;
    }

    public Criteria andFlagGreaterThan(Integer value) {
        addCriterion("FLAG >", value, "flag");
        return this;
    }

    public Criteria andFlagGreaterThanOrEqualTo(Integer value) {
        addCriterion("FLAG >=", value, "flag");
        return this;
    }

    public Criteria andFlagLessThan(Integer value) {
        addCriterion("FLAG <", value, "flag");
        return this;
    }

    public Criteria andFlagLessThanOrEqualTo(Integer value) {
        addCriterion("FLAG <=", value, "flag");
        return this;
    }

    public Criteria andFlagLike(Integer value) {
        addCriterion("FLAG like", value, "flag");
        return this;
    }

    public Criteria andFlagNotLike(Integer value) {
        addCriterion("FLAG not like", value, "flag");
        return this;
    }

    public Criteria andFlagIn(List<Integer> values) {
        addCriterion("FLAG in", values, "flag");
        return this;
    }

    public Criteria andFlagNotIn(List<Integer> values) {
        addCriterion("FLAG not in", values, "flag");
        return this;
    }

    public Criteria andFlagBetween(Integer value1, Integer value2) {
        addCriterion("FLAG between", value1, value2, "flag");
        return this;
    }

    public Criteria andFlagNotBetween(Integer value1, String value2) {
        addCriterion("FLAG not between", value1, value2, "flag");
        return this;
    }

        }
}