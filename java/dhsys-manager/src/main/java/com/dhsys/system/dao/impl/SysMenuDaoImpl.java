package com.dhsys.system.dao.impl;

import java.util.List;
import com.dhsys.common.BaseDao;
import org.springframework.stereotype.Repository;

import com.dhsys.system.dao.SysMenuDao;
import com.dhsys.system.model.SysMenu;
import com.dhsys.system.model.SysMenuCriteria;

@Repository("SysMenuDao")
public class SysMenuDaoImpl extends BaseDao implements SysMenuDao {


        public SysMenuDaoImpl() {
            super();
        }


        public int countByExample(SysMenuCriteria example) {
            Integer count = (Integer)  getSqlMapClientTemplate().queryForObject("sys_menu.ibatorgenerated_countByExample", example);
            return count;
        }


        public int deleteByExample(SysMenuCriteria example) {
            int rows = getSqlMapClientTemplate().delete("sys_menu.ibatorgenerated_deleteByExample", example);
            return rows;
        }


        public int deleteByPrimaryKey(String id) {
        SysMenu key = new SysMenu();
            key.setId(id);
            int rows = getSqlMapClientTemplate().delete("sys_menu.ibatorgenerated_deleteByPrimaryKey", key);
            return rows;
        }


        public void insert(SysMenu record) {
            getSqlMapClientTemplate().insert("sys_menu.ibatorgenerated_insert", record);
        }


        public void insertSelective(SysMenu record) {
            getSqlMapClientTemplate().insert("sys_menu.ibatorgenerated_insertSelective", record);
        }


        @SuppressWarnings("unchecked")
        public List<SysMenu> selectByExample(SysMenuCriteria example) {
            List<SysMenu> list = getSqlMapClientTemplate().queryForList("sys_menu.ibatorgenerated_selectByExample", example);
            return list;
        }


        public SysMenu selectByPrimaryKey(String id) {
            SysMenu key = new SysMenu();
            key.setId(id);
            SysMenu record = (SysMenu) getSqlMapClientTemplate().queryForObject("sys_menu.ibatorgenerated_selectByPrimaryKey", key);
            return record;
        }


        public int updateByExampleSelective(SysMenu record, SysMenuCriteria example) {
            UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
            int rows = getSqlMapClientTemplate().update("sys_menu.ibatorgenerated_updateByExampleSelective", parms);
            return rows;
        }


        public int updateByExample(SysMenu record, SysMenuCriteria example) {
            UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
            int rows = getSqlMapClientTemplate().update("sys_menu.ibatorgenerated_updateByExample", parms);
            return rows;
        }


        public int updateByPrimaryKeySelective(SysMenu record) {
            int rows = getSqlMapClientTemplate().update("sys_menu.ibatorgenerated_updateByPrimaryKeySelective", record);
            return rows;
        }


        public int updateByPrimaryKey(SysMenu record) {
            int rows = getSqlMapClientTemplate().update("sys_menu.ibatorgenerated_updateByPrimaryKey", record);
            return rows;
        }


        private static class UpdateByExampleParms extends SysMenuCriteria {
        private Object record;

        public UpdateByExampleParms(Object record, SysMenuCriteria example) {
            super(example);
            this.record = record;
        }

        public Object getRecord() {
            return record;
        }
  }
}