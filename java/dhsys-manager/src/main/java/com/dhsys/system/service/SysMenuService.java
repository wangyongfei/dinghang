package com.dhsys.system.service;

import com.dhsys.system.dao.impl.ext.SysMenuDaoExtImpl;
import com.dhsys.system.model.SysMenu;
import com.dhsys.system.model.SysMenuCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by wyf on 2016/6/3 0003.
 */
@Service
public class SysMenuService {

    @Autowired
    private SysMenuDaoExtImpl sysMenuDaoExtImpl;

    public int countByExample(SysMenuCriteria example) {
        return sysMenuDaoExtImpl.countByExample(example);
    }

    public int deleteByExample(SysMenuCriteria example) {
        return sysMenuDaoExtImpl.deleteByExample(example);
    }


    public int deleteByPrimaryKey(String id) {
        return sysMenuDaoExtImpl.deleteByPrimaryKey(id);
    }


    public void insert(SysMenu record) {
        sysMenuDaoExtImpl.insert(record);
    }

    public void insertSelective(SysMenu record) {
        sysMenuDaoExtImpl.insertSelective(record);
    }

    @SuppressWarnings("unchecked")
    public List<SysMenu> selectByExample(SysMenu record) {
        SysMenuCriteria example = new SysMenuCriteria();
        if(record != null){
            if(record.getFlag() != null && !"".equals(record.getFlag())){
                example.createCriteria().andFlagEqualTo(record.getFlag());
            }
        }
        return sysMenuDaoExtImpl.selectByExample(example);
    }


    public SysMenu selectByPrimaryKey(String id) {
        return sysMenuDaoExtImpl.selectByPrimaryKey(id);
    }


    public int updateByExampleSelective(SysMenu record, SysMenuCriteria example) {
        return sysMenuDaoExtImpl.updateByExampleSelective(record,example);
    }


    public int updateByExample(SysMenu record, SysMenuCriteria example) {
        return sysMenuDaoExtImpl.updateByExample(record,example);
    }


    public int updateByPrimaryKeySelective(SysMenu record) {
       return sysMenuDaoExtImpl.updateByPrimaryKeySelective(record);
    }


    public int updateByPrimaryKey(SysMenu record) {
       return sysMenuDaoExtImpl.updateByPrimaryKey(record);
    }


    private static class UpdateByExampleParms extends SysMenuCriteria {
        private Object record;

        public UpdateByExampleParms(Object record, SysMenuCriteria example) {
            super(example);
            this.record = record;
        }

        public Object getRecord() {
            return record;
        }
    }
}
