package com.dh.sys.controller.syssync;

import com.dh.sys.service.thread.Consumer;
import com.dh.sys.syssync.model.SysSync;
import com.dh.sys.syssync.service.SysSyncService;
import com.dh.sys.userquota.model.UserQuota;
import com.dh.sys.userquota.service.UserQuotaService;
import com.dh.sys.util.StringUtil;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.*;


@Controller
public class SysSyncController {

    @Autowired
    private SysSyncService service;

    @Autowired
    private UserQuotaService userQuotaService;

    @RequestMapping("/syssync/searchpage")
    public ModelAndView searchPage(){

//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    for (int i = 0; i < 100000; i++) {
//                        SysSync record = new SysSync();
//                        record.setId(StringUtil.getUUID());
//                        record.setLoanId(StringUtil.getUUID());
//                        record.setProcessinstanceid(StringUtil.getUUID());
//                        record.setCallCount(new Random().nextInt(4));
//                        record.setCreteDate(StringUtil.getDateByFormat(new Date(), StringUtil.LONG_DATE_FMT));
//                        record.setOptDate(StringUtil.getDateByFormat(new Date(), StringUtil.LONG_DATE_FMT));
//                        record.setStatus(1);
//                        record.setUid("test" + new Random().nextInt(20));
//                        service.save(record);
//                    }
//                }
//            }).start();
//        List<SysSync>list = service.select(null);
//        HashMap<String,List<SysSync>>map = initData(list);
//        for(String key: map.keySet()){
//            UserQuota sm = new UserQuota();
//            sm.setCreditQuota(10000L);
//            sm.setUsedQuota(0L);
//            sm.setUid(key);
//            userQuotaService.save(sm);
//        }
        //事务测试
//        userQuotaService.insert();
        return  new ModelAndView("syssync/list");
    }

    /**
     * 执行任务
     * 0:执行成功
     * 1:未执行
     * 2:执行中
     * 3:执行异常
     * 4:执行超时
     */
    @RequestMapping("/syssync/jobSync")
    @ResponseBody
    public void jobSync(HttpServletResponse response){
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        List<Integer>unStatus = new ArrayList<>();
        unStatus.add(1);
        unStatus.add(3);
        unStatus.add(4);

        //查询未执行数据
        List<SysSync>list = service.findSysSyncData(unStatus, 2);
        try {
            PrintWriter out = response.getWriter();
            if(null != list && !list.isEmpty()){
                HashMap<String,List<SysSync>>map = initData(list);
                final List<BlockingQueue<List<SysSync>>>queue = initDeque(map);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for(BlockingQueue<List<SysSync>>link:queue){
                            Consumer consumer = new Consumer(link,service);
                            ExecutorService executorService = Executors.newCachedThreadPool();
                            executorService.execute(consumer);
                        }
                    }
                }).start();

                StringBuffer sb = new StringBuffer();
                sb.append("<table border='1'>");
                String mark = "</td><td>";
                sb.append("<tr><td>");
                sb.append("任务执行条数"+mark+list.size());
                sb.append("</td></tr>");
                sb.append("<tr><td>");
                sb.append("任务对列数"+mark+queue.size());
                sb.append("</td></tr>");
                for(String key: map.keySet()){
                    sb.append("<tr><td>");
                    sb.append(key+mark+map.get(key).size());
                    sb.append("</td></tr>");
                }
                sb.append("</table>");
                out.write(returnMsg(sb.toString(),0));
            }else{
                out.write(returnMsg("没有需要执行的数据",1));
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



    }

    @RequestMapping("/syssync/search")
    @ResponseBody
    public void search(
        HttpServletRequest request,
        HttpServletResponse response,
        @RequestParam(value="page", defaultValue="1") Integer page,
        @RequestParam(value="rows", defaultValue="10")  Integer rows){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        SysSync record = new SysSync();
        if(page>=1){
            page = (page-1)*rows;
        }
        List<SysSync> list = service.findAllListPage(record,page,rows);
        int total = service.count(record);

        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(pageListToJson(list, total));

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }

    }

    @RequestMapping(value="/syssync/edit")
    public ModelAndView edit(
                            HttpServletRequest request,
                            HttpServletResponse response,
                            @RequestParam(value="id", defaultValue="") String id){

        ModelAndView modelAndView = new ModelAndView("syssync/edit");

        if(!StringUtil.isNullOrEmpty(id)){
            SysSync record = service.selectById(id);
            modelAndView.addObject("vo", record);
        }
        return modelAndView;

    }

    @RequestMapping(value="/syssync/delete")
    @ResponseBody
    public void delete(
                        SysSync vo,
                        HttpServletResponse response){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        try {

            boolean flag =  service.delete(vo);
            PrintWriter out = response.getWriter();
            out.write(returnMsg("删除成功",0));

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @RequestMapping(value="/syssync/save")
    @ResponseBody
    public void save(
                        SysSync vo,
                        HttpServletResponse response){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        try {
            if(StringUtil.isNullOrEmpty(vo.getId())){
                service.save(vo);
            }else{
                service.update(vo);
            }
            PrintWriter out = response.getWriter();
            out.write(returnMsg("操作成功",0));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private String pageListToJson(List list, int total){

        JsonObject object=new JsonObject();
        Gson gson=new Gson();
        int count=list.size();
        JsonElement jsonElement=gson.toJsonTree(list);
        object.add("rows", jsonElement);
        object.addProperty("total", total);
        return object.toString();
    }

    private String pageListToJson2(List list, int total){

        JsonObject object=new JsonObject();
        Gson gson=new Gson();
        int count=list.size();
        JsonElement jsonElement=gson.toJsonTree(list);
        object.add("list", jsonElement);
        object.addProperty("total", total);
        return object.toString();
    }

    private String returnMsg(String msg, Integer flag){
        JsonObject object=new JsonObject();
        object.addProperty("msg", msg);
        object.addProperty("flag", flag);
        return object.toString();
    }

    /**
     * 整合同一用户为同一组
     * @param list
     * @return
     */
    private HashMap<String,List<SysSync>> initData(List<SysSync>list){
        //整合同一用户为同一组
        HashMap<String,List<SysSync>> map = new HashMap<>();
        for(SysSync sysSync: list){
            List<SysSync>list2 = new ArrayList<>();
            if(null != map.get(sysSync.getUid())){
                list2.addAll(map.get(sysSync.getUid()));
            }
            list2.add(sysSync);
            map.put(sysSync.getUid(),list2);
        }
        return map;
    }

    /**
     * 创建执行队列
     * @param map
     * @return
     */
    private List<BlockingQueue<List<SysSync>>> initDeque(HashMap<String,List<SysSync>>map){
        List<BlockingQueue<List<SysSync>>> list = new ArrayList<>();
        for(String key: map.keySet()){
            BlockingQueue<List<SysSync>> queue = new LinkedBlockingQueue<>();
            queue.add(map.get(key));
            list.add(queue);
        }
        return list;
    }
}
