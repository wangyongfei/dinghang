package com.dh.sys.tag;

import com.dh.sys.syscode.model.SysCode;
import com.dh.sys.syscode.service.SysCodeService;
import org.springframework.web.servlet.tags.RequestContextAwareTag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import java.util.List;

/**
 * 字典查询
 * Created by Administrator on 2016/6/26.
 */
public class CodeDicSelect extends RequestContextAwareTag {

    private  String codeType;
    private String value;
    private String name;


    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    @Override
    protected int doStartTagInternal() throws Exception {
        try {
            SysCodeService sysCodeService = (SysCodeService) this.getRequestContext().getWebApplicationContext().getBean(SysCodeService.class);
            JspWriter out = this.pageContext.getOut();
            StringBuffer sb = new StringBuffer();
            SysCode record = new SysCode();
            record.setCodeType(this.getCodeType());
            List<SysCode> list = sysCodeService.select(record);
            sb.append("<select id='"+getId()+"' name='"+getName()+"'>");
            for(SysCode sysCode: list){
                sb.append("<option value='"+sysCode.getCode()+"'>");
                sb.append(sysCode.getCodeName());
                sb.append("</option>");
            }
            sb.append("</select>");
            out.write(sb.toString());
        } catch(Exception e) {
            throw new JspException(e.getMessage());
        }
        return EVAL_PAGE;
    }
}
