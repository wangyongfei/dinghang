package com.dh.sys.controller;

import com.dh.sys.controller.sysmenu.SysMenuCollection;
import com.dh.sys.syslog.model.SysLog;
import com.dh.sys.syslog.service.SysLogService;
import com.dh.sys.sysmenu.model.SysMenu;
import com.dh.sys.sysmenu.service.SysMenuService;
import com.dh.sys.sysuser.model.SysUser;
import com.dh.sys.sysuser.service.SysUserService;
import com.dh.sys.util.StringUtil;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Controller
public class UserLogin {

	@Autowired
	private SysMenuService sysMenuService;
	@Autowired
	private SysUserService sysUserService;

	@Autowired
	private SysLogService sysLogService;

	private static final Logger log = LoggerFactory.getLogger(UserLogin.class);
	//private final String INDEX = "index";
	private final String LOGIN = "login";
	private final String MAINPAGE = "main";
//	private final String HEADERPAGE = "header";
	@RequestMapping("/index")
	public String index(HttpSession httpSession){
//
//		for(int i = 0; i<100000; i++){
//
//////			final int finalI = i;
//////			new Thread(new Runnable() {
////				@Override
////				public void run() {
//					SysLog sysLog = new SysLog();
//					sysLog.setId(StringUtil.getUUID());
//					sysLog.setLogName("登陆"+ i);
//					sysLog.setCreateDate(StringUtil.getDateByFormat(new Date(),StringUtil.LONG_DATE_FMT));
//					sysLogService.save(sysLog);
//				}
////			}).start();
//
////		}

		System.out.println("跳转登陆页面");
		httpSession.setAttribute("su","");
		log.info("LOG]跳转登陆页面");
		return LOGIN;
	}
	
	@RequestMapping(value="/userLogin", method= RequestMethod.POST)
	public void userLogin(String username, String password, HttpServletResponse response, HttpSession httpSession){
		System.out.println("登陆请求1...");
		log.info("[LOG]登陆请求1...");
//		String json = ServletUtils.parseRequest2String(request);
		SysUser su = new SysUser();
		su.setPassword(password);
		su.setUserName(username);
		su.setFlag(1);
		log.info("[LOG]登陆请求参数..."+username+","+password);
		List<SysUser> list = sysUserService.select(su);
		String flag = "1";
		if(list != null && list.size() > 0){
			httpSession.setAttribute("system_user", list.get(0));
			flag = "0";
		}
		try {
			 PrintWriter out = response.getWriter();
			 out.write("{\"success\":\""+flag+"\"}");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/mainPage")
	public ModelAndView mainPage(){
		ModelAndView modelAndView = new ModelAndView("main");

		try {

			SysMenu record = new SysMenu();
			record.setFlag(1);//查询以启用功能
			List<SysMenu> list = sysMenuService.selectSysMenu(record); //获取所有用户数据
			if(list != null && list.size() > 0){
				List<SysMenuCollection>listColl = new ArrayList<SysMenuCollection>();
				for (SysMenu sm : list) {
					if("0".equalsIgnoreCase(sm.getParentNode())){
						SysMenuCollection smc = new SysMenuCollection();
						smc.setSysMenu(sm);
						List<SysMenu>smcList = new ArrayList<SysMenu>();
						for (SysMenu smChild : list) {
							if(!"0".equalsIgnoreCase(smChild.getParentNode())&& sm.getId().equalsIgnoreCase(smChild.getParentNode())){//子节点
								smcList.add(smChild);
							}
						}
						smc.setList(smcList);
						listColl.add(smc);
					}
				}
				modelAndView.addObject("list",listColl);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return modelAndView;
	}
	
	@RequestMapping("/headerPage")
	public ModelAndView headerPage(){
//		System.out.println("跳转至header...");
		log.info("[LOG]跳转至header...");
		return  new ModelAndView("common/header");
	}
	
	@RequestMapping("/footerPage")
	public ModelAndView footerPage(){
//		System.out.println("跳转至footer...");
		log.info("[LOG]跳转至footer...");
		return  new ModelAndView("common/footer");
	}
	
	@RequestMapping("/firstPage")
	public ModelAndView firstPage(){
//		System.out.println("跳转至footer...");
		
		log.info("[LOG]跳转至firstPage...");
		return  new ModelAndView("first");
	}

	private String pageListToJson(List<?> list, int total){

		JsonObject object=new JsonObject();
		Gson gson=new Gson();
		int count=list.size();
		//list.get(0).setCreateDate(null);
		JsonElement jsonElement=gson.toJsonTree(list);
		//items为extjs store proxy reader中设置的root，即为数据源；totalCount为数据总数。
		object.add("rows", jsonElement);
		object.addProperty("total", total);
		return object.toString();
	}

	private String pageListToJson2(List list, int total){

		JsonObject object=new JsonObject();
		Gson gson=new Gson();
		int count=list.size();
		//list.get(0).setCreateDate(null);
		JsonElement jsonElement=gson.toJsonTree(list);
		//items为extjs store proxy reader中设置的root，即为数据源；totalCount为数据总数。
		object.add("list", jsonElement);
		object.addProperty("total", total);
		return object.toString();
	}

	private String returnMsg(String msg, Integer flag){
		JsonObject object=new JsonObject();
		object.addProperty("msg", msg);
		object.addProperty("flag", flag);
		return object.toString();
	}
	
	
}
