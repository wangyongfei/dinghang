package com.dh.sys.util;

import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 自动生成code编码工具
 * Created by Administrator on 2016/9/16.
 */
public class AutoGenerationCode {
    private static  final  Integer CODE_LENGTH = 8;

    /**
     * 自动生成code编码
     * 最大生产7位数字 - X9999999
     * @param mark 第一个字符标识
     * @param maxCode 返回的最大码值
     * @return
     */
    public static String autoGeneration8Code(String maxCode,String mark) {

        if(StringUtils.isEmpty(mark) || mark.length() >= 8) {
            return null;
        }
        Integer length = CODE_LENGTH - mark.length();
        StringBuffer str = new StringBuffer();
        str.append("%s%0");
        str.append(length);
        str.append("d");
        if (StringUtils.isEmpty(maxCode)) {
            return String.format(str.toString(), mark, 1);
        }
        String subMaxCode = maxCode.substring(mark.length(), maxCode.length());
        Pattern pattern = Pattern.compile("^\\d+$");
        Matcher matcher = pattern.matcher(subMaxCode);
        if (matcher.matches()) {
            Integer num = Integer.parseInt(subMaxCode);
            if (String.valueOf(num + 1).length() >= 8) {
                return null;
            }
            return String.format(str.toString(), mark, num + 1);
        }
        return String.format(str.toString(), mark, 1);
    }
}
