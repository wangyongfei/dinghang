package com.dh.sys.util;


import org.apache.commons.lang.StringUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/**
* Created by Administrator on 2016/12/19.
*/
public class DhHttpUtils {
    private static final Logger LOG = LoggerFactory.getLogger(DhHttpUtils.class);

    //上传文件时 每个post请求参数之间的分隔
    private static final String BOUNDARY = "------WebKitFormBoundary";

    private static final String PROTOCOL_SSLV3 = "SSLv3";//任何版本的TLS都比SSLv3安全
    private static final String PROTOCOL_TLS = "TLSv1.2";//支持所有协议

    private static final int MAX_TOTAL = 100;
    private static final int DEFAULT_MAX_PER_ROUTE = 200;
    private static final int CONNECT_TIME_OUT = 60000;
    private static final int SOCKET_TIME_OUT = 60000;

    private static CloseableHttpClient httpClient;

//    static {
//        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
//                .register("http", PlainConnectionSocketFactory.INSTANCE)
//                .register("https", new SSLConnectionSocketFactory(createSSLContextTLS()))
//                .build();
//        PoolingHttpClientConnectionManager http = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
//        http.setMaxTotal(MAX_TOTAL);
//        http.setDefaultMaxPerRoute(DEFAULT_MAX_PER_ROUTE);
//        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(CONNECT_TIME_OUT)
//                .setSocketTimeout(SOCKET_TIME_OUT).build();
//        httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig)
//                .setConnectionManager(http).build();
//    }

    private DhHttpUtils(){

    }

//    public static String post(String url, Map<String,String> paramMap, Map<String,byte[]> fileMap) throws IOException {
//        return post(url, paramMap, fileMap, null);
//    }

    /**
     * 带文件的post请求
     * @param url url
     * @param paramMap 参数
     * @param fileMap 文件参数
     * @return
     * @throws IOException
     */
//    public static String post(String url, Map<String,String> paramMap, Map<String,byte[]> fileMap,
//                              Map<String, List<byte[]>> fileArrayMap)
//            throws IOException {
//        HttpPost post = new HttpPost(url);
//        post.addHeader("Content-Type", "multipart/form-data;charset=utf-8;boundary="+BOUNDARY);
//        MultipartEntityBuilder builder = setParam(paramMap, fileMap, fileArrayMap);
//        post.setEntity(builder.build());
//        HttpResponse response = httpClient.execute(post);
//        return EntityUtils.toString(response.getEntity(), "UTF-8");
//    }

    /**
     * 带文件的post 塞参数
     * @param paramMap
     * @param fileMap
     */
//    private static MultipartEntityBuilder setParam(Map<String,String> paramMap, Map<String,byte[]> fileMap,
//                                                   Map<String, List<byte[]>> fileArrayMap) {
//        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
//        builder.setBoundary(BOUNDARY);
//        if (null != paramMap && !paramMap.isEmpty()) {
//            for (Map.Entry<String,String> entry:paramMap.entrySet()) {
//                builder.addTextBody(entry.getKey(), entry.getValue(),
//                        ContentType.create("application/x-www-form-urlencoded", Consts.UTF_8));
//            }
//        }
//        if (null != fileMap && !fileMap.isEmpty()) {
//            for (Map.Entry<String,byte[]> entry:fileMap.entrySet()) {
//                //在上传文件的时候必须得加上ContentType
//                builder.addBinaryBody(entry.getKey(),entry.getValue(), ContentType.MULTIPART_FORM_DATA, "");
//            }
//        }
//        if (null != fileArrayMap && !fileArrayMap.isEmpty()) {
//            for (Map.Entry<String,List<byte[]>> entry:fileArrayMap.entrySet()) {
//                List<byte[]> list = entry.getValue();
//                if (null != list) {
//                    for (byte[] bytes : entry.getValue()) {
//                        builder.addBinaryBody(entry.getKey(), bytes, ContentType.MULTIPART_FORM_DATA, "");
//                    }
//                }
//            }
//        }
//        return builder;
//    }

    /**
     * post json 请求
     * @param url
     * @param jsonStr
     * @return
     * @throws IOException
     */
    public static String postJson(String url, String jsonStr) throws IOException {
        if (StringUtils.isBlank(url)) {
            return "url不能为空";
        }
        HttpPost post = new HttpPost(url);
        post.addHeader("Content-type", "application/json");
        HttpEntity httpEntity = new StringEntity(jsonStr, "UTF-8");
        post.setEntity(httpEntity);
        HttpResponse response = httpClient.execute(post);
        return EntityUtils.toString(response.getEntity());
    }

    /**
     * post 请求
     * @param url
     * @param paramMap
     * @return
     * @throws IOException
     */
    public static String post(String url, Map<String,String> paramMap) throws IOException {
        if (StringUtils.isBlank(url)) {
            return "url不能为空";
        }
        HttpPost post = new HttpPost(url);
        post.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
        if (null != paramMap && !paramMap.isEmpty()) {
            List<NameValuePair> params = getNameValuePair(paramMap);
            post.setEntity(new UrlEncodedFormEntity(params, Consts.UTF_8));
        }
        HttpResponse response = httpClient.execute(post);
        return EntityUtils.toString(response.getEntity());
    }

    public static String get(String url) throws IOException {
        if (StringUtils.isBlank(url)) {
            return "url不能为空";
        }
        HttpGet get = new HttpGet(url);
        HttpResponse response = httpClient.execute(get);
        return EntityUtils.toString(response.getEntity());
    }

    /**
     * 参数转换 Map<String,String> ==》List<NameValuePair>
     * @param paramMap
     * @return
     */
    private static List<NameValuePair> getNameValuePair(Map<String,String> paramMap) {
        List<NameValuePair> params = new ArrayList<>();
        for (Map.Entry<String,String> entry:paramMap.entrySet()) {
            params.add(new BasicNameValuePair(entry.getKey(),entry.getValue()));
        }
        return params;
    }

    /**
     * 创建SSLContext SSLv3
     * @return SSLContext
     */
    private static SSLContext createSSLContextSSLv3() {
        return createSSLContext(PROTOCOL_SSLV3);
    }

    /**
     * 创建SSLContext TLS
     * @return
     */
    private static SSLContext createSSLContextTLS() {
        return createSSLContext(PROTOCOL_TLS);
    }

    /**
     * 创建SSLContext
     * @param protocol 协议
     * @return SSLContext
     */
    private static SSLContext createSSLContext(String protocol) {
        SSLContext context = null;
        try {
            context = SSLContext.getInstance(protocol);
            context.init(null, new TrustManager[]{getTrustManager()}, null);
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            LOG.error("创建SSLContext失败",e);
        }
        return context;
    }

    /**
     * 实现一个X509TrustManager接口，用于绕过验证，不用修改里面的方法
     * @return X509TrustManager
     */
    private static X509TrustManager getTrustManager() {
        return new X509TrustManager() {
            @Override
            public void checkClientTrusted(
                    java.security.cert.X509Certificate[] paramArrayOfX509Certificate,
                    String paramString) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(
                    java.security.cert.X509Certificate[] paramArrayOfX509Certificate,
                    String paramString) throws CertificateException {
            }

            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
        };
    }
}
