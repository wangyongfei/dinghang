package com.dh.sys.controller.fileupload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created by Administrator on 2016/11/12.
 */

@Controller
public class AttachedController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AttachedController.class);

    @RequestMapping("/attached/{fileType}/{uploadDate}/{fileName}.{suffix}")
    public void attached(HttpServletRequest request, HttpServletResponse response,
                         @PathVariable String fileType,
                         @PathVariable String uploadDate,
                         @PathVariable String suffix,
                         @PathVariable String fileName) {
        //根据suffix设置响应ContentType
        //response.setContentType("text/html; charset=UTF-8");

        InputStream is = null;
        OutputStream os = null;
        try {
            File file = new File("d:/attached/" + fileType + "/" + uploadDate + "/" + fileName + "." + suffix);
            is = new FileInputStream(file);
            byte[] buffer = new byte[is.available()];
            is.read(buffer);

            os = new BufferedOutputStream(response.getOutputStream());
            os.write(buffer);
            os.flush();
        } catch (Exception e) {
            //判断suffix
            //图片请求可以在此显示一个默认图片
            //file显示文件已损坏等错误提示...
            LOGGER.error("读取文件失败", e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    LOGGER.error("读取文件失败", e);
                }

                if (os != null) {
                    try {
                        os.close();
                    } catch (IOException e) {
                        LOGGER.error("读取文件失败", e);
                    }
                }
            }
        }

    }
}
