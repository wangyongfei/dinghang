package com.dh.sys.controller.sysconfigcenter;

import java.util.Date;

/**
 * Created by Administrator on 2016/12/11.
 */
public class Json2Xml {
    private String id;
    private String xml;
    private Date date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Json2Xml{" +
                "id='" + id + '\'' +
                ", xml='" + xml + '\'' +
                ", date=" + date +
                '}';
    }
}
