/**
 * 模态窗口
 * @param title
 * @param href
 * @param width
 * @param height
 * @param modal
 * @param minimizable
 * @param maximizable
 */

function showMyWindow(id, title, href, width, height, modal, minimizable,
                      maximizable) {
    $('#'+id).window(
        {
            title : title,
            width : width === undefined ? 800 : width,
            height : height === undefined ? 500 : height,
            content : '<iframe scrolling="yes" frameborder="0"  src="'
            + href
            + '" style="width:100%;height:98%;"></iframe>',
            modal : modal === undefined ? true : modal,
            minimizable : minimizable === undefined ? false
                : minimizable,
            maximizable : maximizable === undefined ? false
                : maximizable,
            shadow : false,
            cache : false,
            closed : false,
            collapsible : false,
            resizable : true,
            left: 159,
            top: 28,
            loadingMessage : '正在加载数据，请稍等片刻......'
        });
}