<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@include file="/WEB-INF/views/common/layout/meta.jsp" %>
    <%@include file="/WEB-INF/views/common/layout/links.jsp" %>
    <style type="text/css">
        #list_data td{word-break: keep-all;white-space:nowrap;}
        body{
            padding: 0px;
            margin: 0px;
        }
    </style>
    <script type="text/javascript">
        $(function(){
            $('#list_data').datagrid({
                title:"查询列表",
                class:"easyui-datagrid",
                url:"search",
                toolbar:"#toolbar",
                pagination:"true",
                rownumbers:"true",
                singleSelect:"true"
            });
            $('body').append('<div id="myWindow" class="easyui-dialog" closed="true"></div>');
            // $($("#myWindow").parent()).css("width","1000px");
        });

        function _add(){
            showMyWindow(
                    "myWindow",
                    "编辑",
                    'edit',
                     1000,
                     600
                     );


        }
        function _delete(){
            var row = $('#list_data').datagrid('getSelected');
            if (row){
                _delValid(row.id);
            }else{
                $.messager.alert("操作提示", "请选择一条数据");
            }
        }

        function _delValid(id){
            $.messager.confirm("操作提示", "您确定要执行操作吗？", function (data) {
                if (data) {
                    $.ajax({
                        type:"POST",
                        url:"delete",
                        data:{"id":id},
                        dataType:"json",
                        success:function(data){
                            $.messager.alert("操作提示", data.msg, "info", function () {
                                $('#list_data').datagrid('load',{});
                            });
                        },
                        error:function(e){
                            $.messager.alert("操作提示", "操作失败");
                        }
                    });
                }
            });
        }

        function _update(){
            var row = $('#list_data').datagrid('getSelected');
            if (row){
//                openWin(row.id);
                showMyWindow(
                        "myWindow",
                        "编辑",
                        'edit?id='+row.id,
                        1000);

            }else{
                $.messager.alert("操作提示", "请选择一条数据");
            }
        }
        function searchButton(){
            $('#list_data').datagrid('load',{
//                itemid: $('#itemid').val(),
//                productid: $('#productid').val()
            });
        }

        function formatStatus(val,row){
            if (val==0){
                return '禁用';

            } else {
                // return '<input type="button"  value=""/>';
                return '启用';
            }
        }

        function delStatus(val,row){
            if (val==0){
                return '未删除';

            } else {
                // return '<input type="button"  value=""/>';
                return '已删除';
            }
        }

        function pubStatus(val,row){
            if (val==0){
                return '未发布';

            } else {
                // return '<input type="button"  value=""/>';
                return '已发布';
            }
        }

        function formatOper(val,row){
            return '<a href="#" onclick="_deleteData(\''+row.id+'\')">删除</a>&nbsp;&nbsp;<a href="#">启用</a>&nbsp;&nbsp;<a href="#">发布</a>';
        }

        function _deleteData(id){
            _delValid(id);
        }

        function _reload(){
            $("#list_data").datagrid("reload");
        }



    </script>
</head>

<body style="padding: 0px;">
<div class="easyui-layout" data-options="fit:true" >
    <div data-options="region:'north'" style="height:115px">
        <fieldset>
            <legend>信息查询</legend>
            <%--<form id="ffSearch" method="post">--%>
                <div >
                    <table cellspacing="5" cellpadding="0">
                        <tr>
                            <td>
                                <label for="title">标题头：</label>
                            </td>
                            <td>
                                <input type="text" id="title"  name="txtProvinceName" style="width:100px" />
                            </td>
                            <td>
                                <label for="titleImg">图片地址：</label>
                            </td>
                            <td>
                                <input type="text" id="titleImg" name="titleImg" style="width:100px" />
                            </td>
                            <td>
                                <label for="flag">发布状态：</label>
                            </td>
                            <td>
                                <input type="text" id="flag" name="flag" style="width:100px" />
                            </td>
                            <td>
                                <label for="enableStatus">启用状态：</label>
                            </td>
                            <td>
                                <input type="text" id="enableStatus" name="enableStatus" style="width:100px" />
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <label>日期:</label>
                            </td>
                            <td>
                                <input class="easyui-datebox" style="width:110px;height: 30px; border-color: #ffffff;">
                            </td>
                            <td>
                                <label>至:</label>
                            </td>
                            <td>
                                <input class="easyui-datebox" style="width:110px;height: 30px;">
                            </td>
                            <td colspan="3" align="right">
                                <a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="searchButton();">查询</a>
                                 <a href="javascript:void(0)" class="easyui-linkbutton" id="btnImport" iconcls="icon-package_add">导入</a>
                                 <a href="javascript:void(0)" class="easyui-linkbutton" id="btnExport" iconcls="icon-package_go">导出</a>
                            </td>
                        </tr>
                    </table>
                </div>
            <%--</form>--%>
        </fieldset>
        </div>
<div id="toolbar" style="padding:5px;height:auto">
    <div style="margin-bottom:5px">
        <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="_add();" style="color: black;">新增</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true"  onclick="_update();" style="color: black;">修改</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-pencil_delete" plain="true" onclick="_delete();" style="color: black;">删除</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-table" plain="true" style="color: black;">查看</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="_reload();" style="color: black;">刷新</a>
    </div>
</div>
    <div data-options="region:'center'">
<table id="list_data">
    <thead>
    <tr>
                <th field="id" width="120">编号</th>
                <th field="title" width="220">标题头</th>
                <th field="titleImg" width="220">图片地址</th>
                <%--<th field="newContent" width="120">文本内容</th>--%>
                <th field="createDate" width="120">创建时间</th>
                <th field="updateDate" width="120">修改时间</th>
                <th field="flag" width="120" formatter="pubStatus">发布状态</th>
                <th field="deleteFlag" width="120" formatter="delStatus">是否删除</th>
                <th field="enableStatus" width="120" formatter="formatStatus">启用状态</th>
                <th data-options="field:'_operate',width:180,align:'center',formatter:formatOper">操作</th>
    </tr>
    </thead>
</table>
        </div>
<%--<textarea name="content" cols="100" rows="8" style="width:700px;height:200px;visibility:hidden;"></textarea>--%>
</div>
</body>
</html>