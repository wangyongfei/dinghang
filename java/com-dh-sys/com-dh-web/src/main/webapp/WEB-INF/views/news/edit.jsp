<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ include file="/WEB-INF/views/common/base.jsp"%>
    <%@include file="/WEB-INF/views/common/layout/meta.jsp" %>
    <%@include file="/WEB-INF/views/common/taglib.jsp" %>
    <%@include file="/WEB-INF/views/common/layout/links.jsp" %>
    <link rel="stylesheet" href="${cssBasePath}/common/kindeditor/themes/qq/qq.css" />
    <link rel="stylesheet" href="${scriptBasePath}/common/kindeditor/plugins/code/prettify.css" />
    <script charset="utf-8" src="${scriptBasePath}/common/kindeditor/kindeditor-all-min.js"></script>
    <script charset="utf-8" src="${scriptBasePath}/common/kindeditor/lang/zh_CN.js"></script>
    <style type="text/css">
        #preview{width:100px;height:100px;border:1px solid #000;overflow:hidden;}
        #imghead {filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=image);}
    </style>
    <script type="text/javascript">
        var editor;
        KindEditor.ready(function(K) {
            editor = K.create('textarea[name="content"]', {
                cssPath : '${scriptBasePath}/common/kindeditor/plugins/code/prettify.css',
                uploadJson : '<%=request.getContextPath()%>/fileupload/file_upload',
                fileManagerJson : '<%=request.getContextPath()%>/fileupload/fileManager',
                allowFileManager : true,
                afterCreate : function() {
                    var self = this;
                    K.ctrl(document, 13, function() {
                        self.sync();
                        document.forms['example'].submit();
                    });
                    K.ctrl(self.edit.doc, 13, function() {
                        self.sync();
                        document.forms['example'].submit();
                    });
                }
            });
        });

        $(function(){
            var _img = "${vo.titleImg}";
            $("#imghead").attr("src", _img?_img:"../images/default.jpg");
        });

        function save(){
            $("#newscontent").val(editor.html());
            var oData = new FormData(document.forms.namedItem("editForm2" ));
            //        oData.append( "CustomField", "This is some extra data" );
            var oReq = new XMLHttpRequest();
            oReq.open( "POST", "save" , true );
            oReq.onload = function(data) {
                if (oReq.status == 200) {
                    $.messager.alert("操作提示", "保存成功", "info", function () {
                    parent.$('#myWindow').window('close');
                    parent.$('#list_data').datagrid('load',{});
                    });
                } else {
                    $.messager.alert("操作提示", "操作失败");
                }
            };
            oReq.send(oData);
        }

        function cancel(){
            parent.$('#myWindow').window('close');
        }

        function previewImage(file)
        {
                var MAXWIDTH  = 100;
                var MAXHEIGHT = 100;
                var div = document.getElementById('preview');
                if (file.files && file.files[0])
                {
                    div.innerHTML = '<img id=imghead>';
                    var img = document.getElementById('imghead');
                    img.onload = function(){
                        var rect = clacImgZoomParam(MAXWIDTH, MAXHEIGHT, img.offsetWidth, img.offsetHeight);
                        img.width = rect.width;
                        img.height = rect.height;
                        img.style.marginLeft = rect.left+'px';
                        img.style.marginTop = rect.top+'px';
                    }
                var reader = new FileReader();
                reader.onload = function(evt){img.src = evt.target.result;}
                reader.readAsDataURL(file.files[0]);
                }
                else
                {
                var sFilter='filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src="';
                file.select();
                var src = document.selection.createRange().text;
                div.innerHTML = '<img id=imghead>';
                var img = document.getElementById('imghead');
                img.filters.item('DXImageTransform.Microsoft.AlphaImageLoader').src = src;
                var rect = clacImgZoomParam(MAXWIDTH, MAXHEIGHT, img.offsetWidth, img.offsetHeight);
                status =('rect:'+rect.top+','+rect.left+','+rect.width+','+rect.height);
                div.innerHTML = "<div id=divhead style='width:"+rect.width+"px;height:"+rect.height+"px;margin-top:"+rect.top+"px;margin-left:"+rect.left+"px;"+sFilter+src+"\"'></div>";
            }
        }
        function clacImgZoomParam( maxWidth, maxHeight, width, height ){
            var param = {top:0, left:0, width:width, height:height};
            if( width>maxWidth || height>maxHeight )
            {
            rateWidth = width / maxWidth;
            rateHeight = height / maxHeight;

            if( rateWidth > rateHeight )
            {
            param.width =  maxWidth;
            param.height = Math.round(height / rateWidth);
            }else
            {
            param.width = Math.round(width / rateHeight);
            param.height = maxHeight;
            }
            }

            param.left = Math.round((maxWidth - param.width) / 2);
            param.top = Math.round((maxHeight - param.height) / 2);
            return param;
        }
    </script>
</head>
<body>
<div style="height: 91%; margin: 0px; text-align: left; padding-top: 10px; float:none;">
<form id="editForm2"  method="post" action="<%=request.getContextPath()%>/news/save"  enctype="multipart/form-data">
<input type="hidden" name="id" id="id"  value="${vo.id}"/>
<input type="hidden" name="newscontent" id="newscontent"  value=""/>
<table style="font-size: 12px; padding: 0px;">
<tr>
<td>
<label>标题头</label>
</td>
<td>
<input type="text" name="title" id="title"  value="${vo.title}"/>
</td>
</tr>
<tr>
<td colspan="2">
<table>
<tr>
<td>
<label>图片地址</label>
<input type="file" name="myfiles" onchange="previewImage(this)"/>
</td>
<td>
<div id="preview">
<img id="imghead" width=200 height=100 border=1 src=''>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<label>文本内容</label>
</td>
<td>
<textarea name="content" style="width:800px;height:400px;">
${newscontent}
</textarea>
</td>
</tr>
<tr>
<td>
<label>发布状态</label>
</td>
<td>
<%--<input type="text" name="flag" id="flag"  value="${vo.flag}"/>--%>
<code:select codeType="pub_status"></code:select>
</td>
</tr>
<tr>
<td>
<label>启用状态</label>
</td>
<td>
<code:select codeType="status"></code:select>
<%--<input type="text" name="enableStatus" id="enableStatus"  value="${vo.enableStatus}"/>--%>
</td>
</tr>
</table>

<%--<%@include file="/WEB-INF/views/util/fileupload.jsp"%>--%>

</form>
</div>
<div id="dlg-buttons" style="text-align: right; padding-right: 20px;">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="save()" iconcls="icon-save">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="cancel()" iconCls="icon-cancel">取消</a>
</div>
</body>
<%--<body style="padding: 0px;">--%>
<%--<textarea name="content" style="width:800px;height:400px;visibility:hidden;">KindEditor</textarea>--%>
<%--</body>--%>
</html>
