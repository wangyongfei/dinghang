<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@include file="/WEB-INF/views/common/layout/meta.jsp" %>
    <%@include file="/WEB-INF/views/common/layout/links.jsp" %>
    <style type="text/css">
        #list_data td{word-break: keep-all;white-space:nowrap;}
        body{
            padding: 0px;
            margin: 0px;
        }
    </style>
    <script type="text/javascript">
        $(function(){
            $('#list_data').datagrid({
                title:"查询列表",
                class:"easyui-datagrid",
                url:"search",
                toolbar:"#toolbar",
                pagination:"true",
                rownumbers:"true",
                singleSelect:"true"
            });

            $('body').append('<div id="myWindow" class="easyui-dialog" closed="true"></div>');
        });

        function _add(){
            showMyWindow(
                    "myWindow",
                    "编辑",
                    'edit');
        }

        function _delete(){
            var row = $('#list_data').datagrid('getSelected');
            if (row){
                $.messager.confirm("操作提示", "您确定要执行操作吗？", function (data) {
                    if (data) {
                        $.ajax({
                            type:"POST",
                            url:"delete",
                            data:{"id":row.id},
                            dataType:"json",
                            success:function(data){
                                $.messager.alert("操作提示", data.msg, "info", function () {
                                    $('#list_data').datagrid('load',{});
                                });
                            },
                            error:function(e){
                                $.messager.alert("操作提示", "操作失败");
                            }
                        });
                    }
                });

            }else{
                $.messager.alert("操作提示", "请选择一条数据");
            }
        }

        function _update(){
            var row = $('#list_data').datagrid('getSelected');
            if (row){
                showMyWindow(
                        "myWindow",
                        "编辑",
                        'edit?id='+row.id);
            }else{
                $.messager.alert("操作提示", "请选择一条数据");
            }
        }
        function searchButton(){
            $('#list_data').datagrid('load',{
//                itemid: $('#itemid').val(),
//                productid: $('#productid').val()
            });
        }

        function _reload(){
            $("#list_data").datagrid("reload");
        }

        function _createXml(id){
            $.messager.confirm("操作提示", "您确定要执行操作吗？", function (data) {
                if (data) {
                    $.ajax({
                        type:"POST",
                        url:"json2Xml",
                        data:{"id":id},
                        dataType:"json",
                        success:function(data){
                            $.messager.alert("操作提示", data.msg, "info", function () {
                                $('#list_data').datagrid('load',{});
                            });
                        },
                        error:function(e){
                            $.messager.alert("操作提示", "操作失败");
                        }
                    });
                }
            });
        }

        function _jsonZip(val,row){
            var re = val.replace(/\s+/g,"");//删除所有空格;
            return re;
        }

        function formatOper(val,row){
            return '<a href="#" onclick="_deleteData(\''+row.id+'\')">删除</a>&nbsp;&nbsp;<a href="#">启用</a>&nbsp;&nbsp;<a href="#" onclick="_createXml(\''+row.id+'\')">生成XML</a>';
        }

    </script>
</head>
<body style="padding: 0px;">

<div class="easyui-layout" data-options="fit:true" >
    <div data-options="region:'north'" style="height:80px">
        <fieldset>
            <legend>信息查询</legend>
            <%--<form id="ffSearch" method="post">--%>
            <div >
                <table cellspacing="5" cellpadding="0">
                    <tr>
                        <td>
                            <label>日期:</label>
                        </td>
                        <td>
                            <input class="easyui-datebox" style="width:110px;height: 30px; border-color: #ffffff;">
                        </td>
                        <td>
                            <label>至:</label>
                        </td>
                        <td>
                            <input class="easyui-datebox" style="width:110px;height: 30px;">
                        </td>
                        <td colspan="3" align="right">
                            <a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="searchButton();">查询</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" id="btnImport" iconcls="icon-package_add">导入</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" id="btnExport" iconcls="icon-package_go">导出</a>
                        </td>
                    </tr>
                </table>
            </div>
            <%--</form>--%>
        </fieldset>
    </div>
    <div id="toolbar" style="padding:5px;height:auto">
        <div style="margin-bottom:5px">
            <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="_add();" style="color: black;">新增</a>
            <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true"  onclick="_update();" style="color: black;">修改</a>
            <a href="#" class="easyui-linkbutton" iconCls="icon-pencil_delete" plain="true" onclick="_delete();" style="color: black;">删除</a>
            <a href="#" class="easyui-linkbutton" iconCls="icon-table" plain="true" onclick="_function_edit();" style="color: black;">功能查看</a>
            <a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="_reload();" style="color: black;">刷新</a>
        </div>
    </div>
    <div data-options="region:'center'">
        <table id="list_data">
            <thead>
            <tr>
                                    <th field="id" width="120">主键</th>
                                    <th field="sysType" width="120">系统类型</th>
                                    <th field="describeContent" width="120">描述</th>
                                    <th data-options="field:'jsonStr',width:180,align:'center',formatter:_jsonZip">JSON串</th>
                                    <th field="createDate" width="120">创建时间</th>
                                    <th field="updateDate" width="120">修改时间</th>
                                    <th field="paramSet" width="120">参数设置</th>
                                    <th field="createUser" width="120">创建人</th>
                                    <th field="updateUser" width="120">修改人</th>
                                    <th data-options="field:'_operate',width:180,align:'center',formatter:formatOper">操作</th>
                            </tr>
            </thead>
        </table>
    </div>
</div>
<div id="myWindow" class="easyui-dialog" closed="true"></div>
</body>
</html>