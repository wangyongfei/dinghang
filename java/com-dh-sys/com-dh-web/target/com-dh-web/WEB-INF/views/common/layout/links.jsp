<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/base.jsp"%>
<%-- <efa:resourceGroup type="css">
	<efa:resource path="/styles/wx/css/website.css"/> --%>
<%-- <efa:resourceGroup type="js">
	<efa:resource path="/resources/base/sea.js"/>	
	<efa:resource path="/resources/base/config.js"/>
	<efa:resource path="/scripts/wx/js/jquery.js"/>
	<efa:resource path="/scripts/wx/js/website.js"/>
</efa:resourceGroup> --%>

<%--style--%>
<link rel="stylesheet" type="text/css" href="${cssBasePath}/common/easyui/themes/ui-sunny/easyui.css">
<link rel="stylesheet" type="text/css" href="${cssBasePath}/common/easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${cssBasePath}/common/css3/style.css">
<link rel="stylesheet" type="text/css" href="${scriptBasePath}/common/multiselect2side/jquery.multiselect2side.css">

<%--script--%>
<script type="text/javascript" src="${scriptBasePath}/common/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${scriptBasePath}/common/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${scriptBasePath}/common/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${scriptBasePath}/common/model_windows.js"></script>
<script type="text/javascript" src="${scriptBasePath}/common/loading.js"></script>
<script type="text/javascript" src="${scriptBasePath}/common/multiselect2side/jquery.multiselect2side.js"></script>
<script type="text/javascript" src="${scriptBasePath}/common/jsonformat/jsonformat.js"></script>

<script type="text/javascript">
var jscontextPath="${contextPath}";
var jsPath="${path}";
var jsimagesBasePath="${imagesBasePath}";
var jsheadPath="${imagesHeadPath}";
var jsscriptBasePath="${scriptBasePath}";
var jsimagesServerPath="${imagesServerPath}";
var jsvideoPath = "${videoPath}";
</script>