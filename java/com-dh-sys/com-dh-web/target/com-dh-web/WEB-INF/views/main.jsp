<%@ page import="com.dh.sys.sysuser.model.SysUser" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/WEB-INF/views/common/layout/meta.jsp" %>
<%@include file="/WEB-INF/views/common/layout/links.jsp" %>
    <style type="text/css">
        body{
            font-family: "Microsoft YaHei" ! important;
            color: #817865;
        }
        .nav-item
        {
            text-align: center;
        }

        .nav-item a
        {
            color: #000;
            text-decoration: none;
        }
        /*菜单js打开tab出现的滚动条隐藏*/
        .panel-body-noborder
        {
            overflow: hidden;
        }

    </style>
    <script type="text/javascript">

        var user =  "${system_user.userName}";
        if(!user){
            window.location.href = "index";
        }

        function addTab(title, href) {
            var tt = $('#main-center');
            if (tt.tabs('exists', title)) {
                tt.tabs('select', title);
            } else {
                if (href) {
                    var content = '<iframe scrolling="yes" frameborder="0"  src="' + href + '" style="width:100%;height:100%;"></iframe>';
                } else {
                    var content = '未实现';
                }
                tt.tabs('add', {
                    title: title,
                    closable: true,
                    content: content
                });
            }
        }

        $(function(){
            // 伸缩时刷新当前tab页面
            $(".layout-button-left").bind("click", function(){
                var current_tab = $('#main-center').tabs('getSelected');
                var url = $(current_tab.panel('options').content).attr('src');
                $('#main-center').tabs('update',{
                    tab:current_tab,
                    options : {
                       content : '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>'
                       // href : ''+url+''
                    }
                });
            });

            //退出事件
            $("#clock").bind("click", function(){
                //退出时处理session
                $.messager.confirm("操作提示", "您确定要退出吗？", function (data) {
                    if (data) {
                        window.location.href = "index";
                    }
                });
            });

            //默认菜单全部折叠
            $("#main_tree").tree("collapseAll");
        });

    </script>
</head>
<body class="easyui-layout">
<div region="north" title="" split="true" style="height:50px;padding:10px;background-color: #817865; line-height: 20px; color: snow;">
    <div id="top">
        <div id="toppiz" style="line-height: 20px;">
            <div style="float: left; font-size: 16px; ">鼎航后台管理系统</div>
            <div style="float: right;">
               <span id="lblBra">【${system_user.userName}】</span> | <a href="#" id="clock" style="cursor: hand; color: #ffffff; text-decoration: none;">退出</a>
            </div>
        </div>
    </div>
</div>
<div region="south"  style="height:30px;background:#efefef;">
    <div class="easyui-layout" fit="true" style="background:#ccc; text-align: center;">
        <div region="center" style="text-align: center; background-color: #a69e8d">鼎航后台管理系统 @ www.fcworld.net 版本V1.0</div>
    </div>
</div>
<%--<div region="east" iconCls="icon-reload" title="Tree Menu" split="true" style="width:180px;">--%>
    <%--<ul class="easyui-tree" url="tree_data.json"></ul>--%>
<%--</div>--%>
<div region="west" id="west_id" split="true" title="菜单" style="width:280px;padding1:1px;overflow:hidden; color: #1a4b78;">
    <div class="easyui-accordion" fit="true" border="true">
        <div title="系统功能" style="overflow:auto;">
            <ul id="main_tree" class="easyui-tree" data-options="state:closed">
                <c:forEach var="item" items="${list}" varStatus="status">
                    <li>
                        <span>${item.sysMenu.name}</span>
                        <ul>
                            <c:forEach var="item2" items="${item.list}" varStatus="status">
                                <li><span><a href="#" style="text-decoration: none;color: #000000;" onclick="addTab('${item2.name}','${item2.url}')">${item2.name}</a></span></li>
                            </c:forEach>
                        </ul>
                    </li>
                </c:forEach>
            </ul>
        </div>
    </div>
</div>
<div region="center" title="鼎航管理系统" style="overflow:hidden;">
    <div id="main-center" class="easyui-tabs" fit="true" border="false">
        <div title="首页" style="overflow:hidden;">
            <div style="margin-top:20px;">
                欢迎进入鼎航管理系统
            </div>
        </div>

    </div>
</div>
</body>
<%--<body id="cc" class="easyui-layout">
<div data-options="region:'north',split:true" style="height: 50px; background-color: #3892D3"></div>
<div data-options="region:'west',title:'导航菜单',split:true" id="main_title" style="width: 150px;">
    <div id="aa" class="easyui-accordion" fit="true" style="height: auto;">
        <ul class="easyui-tree">
            <c:forEach var="item" items="${list}" varStatus="status">
            <li>
                <span>${item.sysMenu.name}</span>
                <ul>
                    <c:forEach var="item2" items="${item.list}" varStatus="status">
                    <li><span><a href="#" onclick="addTab('${item2.name}','${item2.url}')">${item2.name}</a></span></li>
                    </c:forEach>
                </ul>
            </li>
            </c:forEach>
        </ul>
    </div>
</div>
<div data-options="region:'center'" style="padding: 5px; background: #eee;">
    <div id="main-center" class="easyui-tabs" fit="true" border="false">
        <div title="首页" style="padding: 20px;">
            <img src="images/banner.gif"></img>
            <div style="margin-top: 20px;">
                <p>xxxx有限公司。</p>
                <p>&nbsp;</p>
                <p>监控平台。</p>
                <p>&nbsp;</p>
            </div>
        </div>
    </div>
</div>
</body>--%>
</html>