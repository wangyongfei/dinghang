<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@include file="/WEB-INF/views/common/layout/meta.jsp" %>
    <%@include file="/WEB-INF/views/common/layout/links.jsp" %>
    <style type="text/css">
        body{
            padding: 0px;
            margin: 0px;
        }
    </style>
    <script type="text/javascript">
        $(function(){
            $('#list_data').datagrid({
                title:"查询列表",
                class:"easyui-datagrid",
                url:"search",
                toolbar:"#toolbar",
                pagination:"true",
                rownumbers:"true",
                singleSelect:"true"
            });
        });

        function _add(){
            showMyWindow(
                    "myWindow",
                    "编辑",
                    'edit');
        }

        function _delete(){
            var row = $('#list_data').datagrid('getSelected');
            if (row){
                $.messager.confirm("操作提示", "您确定要执行操作吗？", function (data) {
                    if (data) {
                        $.ajax({
                            type:"POST",
                            url:"delete",
                            data:{"id":row.id},
                            dataType:"json",
                            success:function(data){
                                $.messager.alert("操作提示", data.msg, "info", function () {
                                    $('#list_data').datagrid('load',{});
                                });
                            },
                            error:function(e){
                                $.messager.alert("操作提示", "操作失败");
                            }
                        });
                    }
                });

            }else{
                $.messager.alert("操作提示", "请选择一条数据");
            }
        }

        function _update(){
            var row = $('#list_data').datagrid('getSelected');
            if (row){
                showMyWindow(
                        "myWindow",
                        "编辑",
                        'edit?id='+row.id);
            }else{
                $.messager.alert("操作提示", "请选择一条数据");
            }
        }
        function searchButton(){
            $('#list_data').datagrid('load',{
//                itemid: $('#itemid').val(),
//                productid: $('#productid').val()
            });
        }

    </script>
</head>
<body>
<div id="toolbar" style="padding:5px;height:auto">
    <div>
        日期: <input class="easyui-datebox" style="width:80px">至: <input class="easyui-datebox" style="width:80px">
        <a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="searchButton();">查询</a>
    </div>
    <div style="margin-bottom:5px">
        <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="_add();" style="color: black;">新增</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true"  onclick="_update();" style="color: black;">修改</a>
        <%--<a href="#" class="easyui-linkbutton" iconCls="icon-save" plain="true"></a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-cut" plain="true"></a>--%>
        <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="_delete();" style="color: black;">删除</a>
    </div>
</div>
<table id="list_data">
    <thead>
    <tr>
                <th field="id" width="120">用户编号</th>
                <th field="userName" width="120">用户名称</th>
                <th field="password" width="120">用户密码</th>
                <th field="createDate" width="120">创建时间</th>
                <th field="flag" width="120">是否启用 0：未启用 1：启用</th>
            </tr>
    </thead>
</table>
<div id="myWindow" class="easyui-dialog" closed="true"></div>
</body>
</html>