<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@include file="/WEB-INF/views/common/layout/meta.jsp" %>
    <%@include file="/WEB-INF/views/common/layout/links.jsp" %>
    <style type="text/css">
        #list_data td{word-break: keep-all;white-space:nowrap;}
        body{
            padding: 0px;
            margin: 0px;
        }
    </style>
    <script type="text/javascript">
        $(function(){
            $('#list_data').datagrid({
                title:"查询列表",
                class:"easyui-datagrid",
                url:"search",
                toolbar:"#toolbar",
                pagination:"true",
                rownumbers:"true",
                singleSelect:"true"
            });

            $('body').append('<div id="myWindow" class="easyui-dialog" closed="true"></div>');
        });

        function _add(){
            showMyWindow(
                    "myWindow",
                    "编辑",
                    'edit');
        }

        function _delete(){
            var row = $('#list_data').datagrid('getSelected');
            if (row){
                $.messager.confirm("操作提示", "您确定要执行操作吗？", function (data) {
                    if (data) {
                        $.ajax({
                            type:"POST",
                            url:"delete",
                            data:{"id":row.id},
                            dataType:"json",
                            success:function(data){
                                $.messager.alert("操作提示", data.msg, "info", function () {
                                    $('#list_data').datagrid('load',{});
                                });
                            },
                            error:function(e){
                                $.messager.alert("操作提示", "操作失败");
                            }
                        });
                    }
                });

            }else{
                $.messager.alert("操作提示", "请选择一条数据");
            }
        }

        function _update(){
            var row = $('#list_data').datagrid('getSelected');
            if (row){
                showMyWindow(
                        "myWindow",
                        "编辑",
                        'edit?id='+row.id);
            }else{
                $.messager.alert("操作提示", "请选择一条数据");
            }
        }
        function searchButton(){
            $('#list_data').datagrid('load',{
//                itemid: $('#itemid').val(),
//                productid: $('#productid').val()
            });
        }

        function _reload(){
            $("#list_data").datagrid("reload");
        }

        function _job_sync(){
            $.messager.confirm("操作提示", "您确定要执行操作吗？", function (data) {
                if (data) {
                    $.ajax({
                        type: "POST",
                        url: "jobSync",
                        success: function (data) {
                            debugger;
                            $.messager.alert("操作提示", data.msg, "info", function () {
                              //  $.messager.alert("操作提示", "操作成功");
                            });
                        },
                        error: function (e) {
                            $.messager.alert("操作提示", "操作失败");
                        }
                    });
                }
            });
        }

        function formatStatus(val,row){
            if(1==val){
                return "<span style='color: #CC0000;'>未处理</span>";
            }else if(0==val){
                return "<span style='color: green;'>成功</span>";
            }else if(2==val){
                return "<span style='color: #00ee00;'>执行中</span>";
            }else if(3==val){
                return "<span style='color:darkred;'>执行异常</span>";
            }else if(4==val){
                return "<span style='color: violet;'>执行超时</span>";
            }
        }

        function formatStatus2(val,row){
            return '<button>发送通知</button>';
        }

    </script>
</head>
<body style="padding: 0px;">

<div class="easyui-layout" data-options="fit:true" >
    <div data-options="region:'north'" style="height:80px">
        <fieldset>
            <legend>信息查询</legend>
            <%--<form id="ffSearch" method="post">--%>
            <div >
                <table cellspacing="5" cellpadding="0">
                    <tr>
                        <td>
                            <label>日期:</label>
                        </td>
                        <td>
                            <input class="easyui-datebox" style="width:110px;height: 30px; border-color: #ffffff;">
                        </td>
                        <td>
                            <label>至:</label>
                        </td>
                        <td>
                            <input class="easyui-datebox" style="width:110px;height: 30px;">
                        </td>
                        <td colspan="3" align="right">
                            <a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="searchButton();">查询</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" id="btnImport" iconcls="icon-package_add">导入</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" id="btnExport" iconcls="icon-package_go">导出</a>
                        </td>
                    </tr>
                </table>
            </div>
            <%--</form>--%>
        </fieldset>
    </div>
    <div id="toolbar" style="padding:5px;height:auto">
        <div style="margin-bottom:5px">
            <%--<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="_add();" style="color: black;">新增</a>--%>
            <%--<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true"  onclick="_update();" style="color: black;">修改</a>--%>
            <%--<a href="#" class="easyui-linkbutton" iconCls="icon-pencil_delete" plain="true" onclick="_delete();" style="color: black;">删除</a>--%>
            <a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="_job_sync();" style="color: black;">执行JOB</a>
            <a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="_reload();" style="color: black;">刷新</a>
        </div>
    </div>
    <div data-options="region:'center'">
        <table id="list_data">
            <thead>
            <tr>
                                    <th field="id" width="120">主键</th>
                                    <th field="loanId" width="120">进件号</th>
                                    <th field="processinstanceid" width="120">流程实例</th>
                                    <th field="uid" width="120">用户编号</th>
                                    <th field="creteDate" width="120">创建时间</th>
                                    <th field="optDate" width="120">操作时间</th>
                                    <th field="finishDate" width="120">完成时间</th>
                                    <th field="status" align="center" width="120" formatter="formatStatus">任务状态</th>
                                    <th field="callCount" width="120">调用次数</th>
                                    <th data-options="field:'opt', width:150,align:'center',formatter:formatStatus2">操作</th>
                            </tr>
            </thead>
        </table>
    </div>
</div>
<div id="myWindow" class="easyui-dialog" closed="true"></div>
</body>
</html>