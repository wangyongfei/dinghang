<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/base.jsp"%>
<%@include file="/WEB-INF/views/common/taglib.jsp" %>
<%@include file="/WEB-INF/views/common/layout/meta.jsp" %>
<%@include file="/WEB-INF/views/common/layout/links.jsp" %>
<script type="text/javascript">
    function save(){
        $.ajax({
            type:"POST",
            url:"save",
            data:$("#editForm").serialize(),
            dataType:"json",
            success:function(data){
                $.messager.alert("操作提示", data.msg, "info", function () {
                    parent.$('#myWindow').window('close');
                    parent.$('#list_data').datagrid('load',{});
                });
            },
            error:function(e){

                $.messager.alert("操作提示", "操作失败");
            }
        });
    }

    function cancel(){
        parent.$('#myWindow').window('close');
    }
</script>
<div style="height: 91%; margin: 0px; text-align: center; padding-top: 20px;">
    <form id="editForm" method="post">
        <table align="center" style="font-size: 12px; padding: 0px;">
                        <tr>
                <td>
                    <label>主键</label>
                </td>
                <td>
                    <input type="text" name="id" id="id"  value="${vo.id}"/>
                </td>
            </tr>
                        <tr>
                <td>
                    <label>进件号</label>
                </td>
                <td>
                    <input type="text" name="loanId" id="loanId"  value="${vo.loanId}"/>
                </td>
            </tr>
                        <tr>
                <td>
                    <label>流程实例</label>
                </td>
                <td>
                    <input type="text" name="processinstanceid" id="processinstanceid"  value="${vo.processinstanceid}"/>
                </td>
            </tr>
                        <tr>
                <td>
                    <label>创建时间</label>
                </td>
                <td>
                    <input type="text" name="creteDate" id="creteDate"  value="${vo.creteDate}"/>
                </td>
            </tr>
                        <tr>
                <td>
                    <label>操作时间</label>
                </td>
                <td>
                    <input type="text" name="optDate" id="optDate"  value="${vo.optDate}"/>
                </td>
            </tr>
                        <tr>
                <td>
                    <label>完成时间</label>
                </td>
                <td>
                    <input type="text" name="finishDate" id="finishDate"  value="${vo.finishDate}"/>
                </td>
            </tr>
                        <tr>
                <td>
                    <label>任务状态</label>
                </td>
                <td>
                    <input type="text" name="status" id="status"  value="${vo.status}"/>
                </td>
            </tr>
                        <tr>
                <td>
                    <label>调用次数</label>
                </td>
                <td>
                    <input type="text" name="callCount" id="callCount"  value="${vo.callCount}"/>
                </td>
            </tr>
                    </table>
    </form>
</div>
<div id="dlg-buttons" style="text-align: right; padding-right: 20px;">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="save()" iconcls="icon-save">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="cancel()" iconCls="icon-cancel">取消</a>
</div>
</html>