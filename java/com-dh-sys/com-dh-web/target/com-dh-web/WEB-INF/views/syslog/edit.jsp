<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/base.jsp"%>
<%@include file="/WEB-INF/views/common/taglib.jsp" %>
<%@include file="/WEB-INF/views/common/layout/meta.jsp" %>
<%@include file="/WEB-INF/views/common/layout/links.jsp" %>
<script type="text/javascript">
    function save(){
        $.ajax({
            type:"POST",
            url:"save",
            data:$("#editForm").serialize(),
            dataType:"json",
            success:function(data){
                $.messager.alert("操作提示", data.msg, "info", function () {
                    parent.$('#myWindow').window('close');
                    parent.$('#list_data').datagrid('load',{});
                });
            },
            error:function(e){

                $.messager.alert("操作提示", "操作失败");
            }
        });
    }

    function cancel(){
        parent.$('#myWindow').window('close');
    }
</script>
<div style="height: 91%; margin: 0px; text-align: center; padding-top: 20px;">
    <form id="editForm" method="post">
        <table align="center" style="font-size: 12px; padding: 0px;">
                        <tr>
                <td>
                    <label>主键</label>
                </td>
                <td>
                    <input type="text" name="id" id="id"  value="${vo.id}"/>
                </td>
            </tr>
                        <tr>
                <td>
                    <label>日志名称</label>
                </td>
                <td>
                    <input type="text" name="logName" id="logName"  value="${vo.logName}"/>
                </td>
            </tr>
                        <tr>
                <td>
                    <label>创建日期</label>
                </td>
                <td>
                    <input type="text" name="createDate" id="createDate"  value="${vo.createDate}"/>
                </td>
            </tr>
                    </table>
    </form>
</div>
<div id="dlg-buttons" style="text-align: right; padding-right: 20px;">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="save()" iconcls="icon-save">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="cancel()" iconCls="icon-cancel">取消</a>
</div>
</html>