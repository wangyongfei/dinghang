package com.dh.sys.custinfo.dao;

import com.dh.sys.custinfo.model.CustInfo;
import com.dh.sys.custinfo.model.CustInfoCriteria;
import java.util.List;

public interface CustInfoDao {

    int countByExample(CustInfoCriteria example);


    int deleteByExample(CustInfoCriteria example);


    int deleteByPrimaryKey(String id);


    void insert(CustInfo record);


    void insertSelective(CustInfo record);


    List<CustInfo> selectByExample(CustInfoCriteria example,Integer page, Integer rows);


    CustInfo selectByPrimaryKey(String id);


    int updateByExampleSelective(CustInfo record, CustInfoCriteria example);


    int updateByExample(CustInfo record, CustInfoCriteria example);


    int updateByPrimaryKeySelective(CustInfo record);


    int updateByPrimaryKey(CustInfo record);

    List<CustInfo> selectByExample(CustInfoCriteria example);

    
}