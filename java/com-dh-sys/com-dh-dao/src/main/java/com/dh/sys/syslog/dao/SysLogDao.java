package com.dh.sys.syslog.dao;

import com.dh.sys.syslog.model.SysLog;
import com.dh.sys.syslog.model.SysLogCriteria;
import java.util.List;

public interface SysLogDao {

    int countByExample(SysLogCriteria example);


    int deleteByExample(SysLogCriteria example);


    int deleteByPrimaryKey(String id);


    void insert(SysLog record);


    void insertSelective(SysLog record);


    List<SysLog> selectByExample(SysLogCriteria example,Integer page, Integer rows);


    SysLog selectByPrimaryKey(String id);


    int updateByExampleSelective(SysLog record, SysLogCriteria example);


    int updateByExample(SysLog record, SysLogCriteria example);


    int updateByPrimaryKeySelective(SysLog record);


    int updateByPrimaryKey(SysLog record);

    List<SysLog> selectByExample(SysLogCriteria example);

    
}