package com.dh.sys.syssync.dao.impl;

import java.util.List;
import com.dh.sys.common.BaseDao;
import org.springframework.stereotype.Repository;

import com.dh.sys.syssync.dao.SysSyncDao;
import com.dh.sys.syssync.model.SysSync;
import com.dh.sys.syssync.model.SysSyncCriteria;

@Repository("SysSyncDao")
public class SysSyncDaoImpl extends BaseDao implements SysSyncDao {

    public SysSyncDaoImpl() {
        super();
    }

    public int countByExample(SysSyncCriteria example) {
        Integer count = (Integer)  getSqlMapClientTemplate().queryForObject("sys_sync.ibatorgenerated_countByExample", example);
        return count;
    }

    public int deleteByExample(SysSyncCriteria example) {
        int rows = getSqlMapClientTemplate().delete("sys_sync.ibatorgenerated_deleteByExample", example);
        return rows;
    }

    public int deleteByPrimaryKey(String id) {
    SysSync key = new SysSync();
        key.setId(id);
        int rows = getSqlMapClientTemplate().delete("sys_sync.ibatorgenerated_deleteByPrimaryKey", key);
        return rows;
    }

    public void insert(SysSync record) {
        getSqlMapClientTemplate().insert("sys_sync.ibatorgenerated_insert", record);
    }

    public void insertSelective(SysSync record) {
        getSqlMapClientTemplate().insert("sys_sync.ibatorgenerated_insertSelective", record);
    }

    @SuppressWarnings("unchecked")
    public List<SysSync> selectByExample(SysSyncCriteria example,Integer page, Integer rows) {
        List<SysSync> list = getSqlMapClientTemplate().queryForList("sys_sync.ibatorgenerated_selectByExample", example,page, rows);
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<SysSync> selectByExample(SysSyncCriteria example) {
        List<SysSync> list = getSqlMapClientTemplate().queryForList("sys_sync.ibatorgenerated_selectByExample", example);
        return list;
    }

    public SysSync selectByPrimaryKey(String id) {
        SysSync key = new SysSync();
        key.setId(id);
        SysSync record = (SysSync) getSqlMapClientTemplate().queryForObject("sys_sync.ibatorgenerated_selectByPrimaryKey", key);
        return record;
    }

    public int updateByExampleSelective(SysSync record, SysSyncCriteria example) {
        UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
        int rows = getSqlMapClientTemplate().update("sys_sync.ibatorgenerated_updateByExampleSelective", parms);
        return rows;
    }

    public int updateByExample(SysSync record, SysSyncCriteria example) {
        UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
        int rows = getSqlMapClientTemplate().update("sys_sync.ibatorgenerated_updateByExample", parms);
        return rows;
    }

    public int updateByPrimaryKeySelective(SysSync record) {
        int rows = getSqlMapClientTemplate().update("sys_sync.ibatorgenerated_updateByPrimaryKeySelective", record);
        return rows;
    }

    public int updateByPrimaryKey(SysSync record) {
        int rows = getSqlMapClientTemplate().update("sys_sync.ibatorgenerated_updateByPrimaryKey", record);
        return rows;
    }

     private static class UpdateByExampleParms extends SysSyncCriteria {
        private Object record;

        public UpdateByExampleParms(Object record, SysSyncCriteria example) {
            super(example);
            this.record = record;
        }

        public Object getRecord() {
            return record;
        }
    }
}