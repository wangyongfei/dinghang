package com.dh.sys.news.model;

import java.io.Serializable;

public class DhNews implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
    * 
    */
    private String id;


    /**
    * 标题头
    */
    private String title;


    /**
    * 图片地址
    */
    private String titleImg;


    /**
    * 文本内容
    */
    private byte[] newContent;


    /**
    * 
    */
    private String createDate;


    /**
    * 
    */
    private String updateDate;


    /**
    * 发布状态 0：未发布；1：已发布
    */
    private Integer flag;


    /**
    * 是否删除 0:未删除；1：已删除
    */
    private Integer deleteFlag;


    /**
    * 启用状态
    */
    private Integer enableStatus;


    /**
    * 资讯编号
    */
    private String newsCode;


    /**
    * 备注
    */
    private String mark;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getTitleImg() {
        return titleImg;
    }

    public void setTitleImg(String titleImg) {
        this.titleImg = titleImg;
    }
    
    public byte[] getNewContent() {
        return newContent;
    }

    public void setNewContent(byte[] newContent) {
        this.newContent = newContent;
    }
    
    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    
    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
    
    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }
    
    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
    
    public Integer getEnableStatus() {
        return enableStatus;
    }

    public void setEnableStatus(Integer enableStatus) {
        this.enableStatus = enableStatus;
    }
    
    public String getNewsCode() {
        return newsCode;
    }

    public void setNewsCode(String newsCode) {
        this.newsCode = newsCode;
    }
    
    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }


    public DhNews() {
    }

    public DhNews(String id, String title) {
        this.id = id;
        this.title = title;
    }
}