package com.dh.sys.syslog.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 数据库条件操作类
*  create by  Admin at $date.format('yyyy-MM-dd HH:mm:ss',$createDate)
*/
public class SysLogCriteria {

    /**
    * 排序字段
    */
    protected String orderByClause;

    /**
    * 数据库字段拼接集合
    */
    protected List<Criteria> oredCriteria;

    /**
    * 实例化对象的同时，为oredCriteria实例化
    */
    public SysLogCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
    * 1.实例化对象的同时，为oredCriteria实例化
    * 2.实例化对象的同时，为排序字段orderByClause赋值
    * @param example
    */
    protected SysLogCriteria(SysLogCriteria example) {
        this.orderByClause = example.orderByClause;
        this.oredCriteria = example.oredCriteria;
    }


    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }


    public String getOrderByClause() {
        return orderByClause;
    }


    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }


    public void or(Criteria criteria) {
     oredCriteria.add(criteria);
    }


    public Criteria createCriteria() {
        /**
        * 获取criteria实例化对象
        */
        Criteria criteria = createCriteriaInternal();

        /**
        * 首次调用时如果数据库字段拼接集合为空，
        * 则获取一个新的实例对象加到该结合中
        */
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
    * 获取一个Criteria实例对象
    * @return
    */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }


    /**
    * 清空数据库字段拼接集合
    */
    public void clear() {
        oredCriteria.clear();
    }


    /**
    * 数据库字段拼接实体类
    */
    public static class Criteria {
    protected List<String> criteriaWithoutValue;

    protected List<Map<String, Object>> criteriaWithSingleValue;

    protected List<Map<String, Object>> criteriaWithListValue;

    protected List<Map<String, Object>> criteriaWithBetweenValue;

    protected Criteria() {
        super();
        criteriaWithoutValue = new ArrayList<String>();
        criteriaWithSingleValue = new ArrayList<Map<String, Object>>();
        criteriaWithListValue = new ArrayList<Map<String, Object>>();
        criteriaWithBetweenValue = new ArrayList<Map<String, Object>>();
    }

    public boolean isValid() {
        return criteriaWithoutValue.size() > 0
        || criteriaWithSingleValue.size() > 0
        || criteriaWithListValue.size() > 0
        || criteriaWithBetweenValue.size() > 0;
    }

    public List<String> getCriteriaWithoutValue() {
        return criteriaWithoutValue;
    }

    public List<Map<String, Object>> getCriteriaWithSingleValue() {
        return criteriaWithSingleValue;
    }

    public List<Map<String, Object>> getCriteriaWithListValue() {
        return criteriaWithListValue;
    }

    public List<Map<String, Object>> getCriteriaWithBetweenValue() {
        return criteriaWithBetweenValue;
    }

    protected void addCriterion(String condition) {
        if (condition == null) {
            throw new RuntimeException("Value for condition cannot be null");
        }
        criteriaWithoutValue.add(condition);
    }

    protected void addCriterion(String condition, Object value, String property) {
        if (value == null) {
            throw new RuntimeException("Value for " + property + " cannot be null");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("value", value);
        criteriaWithSingleValue.add(map);
    }

    protected void addCriterion(String condition, List<? extends Object> values, String property) {
        if (values == null || values.size() == 0) {
            throw new RuntimeException("Value list for " + property + " cannot be null or empty");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", values);
        criteriaWithListValue.add(map);
    }

    protected void addCriterion(String condition, Object value1, Object value2, String property) {
        if (value1 == null || value2 == null) {
            throw new RuntimeException("Between values for " + property + " cannot be null");
        }
        List<Object> list = new ArrayList<Object>();
        list.add(value1);
        list.add(value2);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", list);
        criteriaWithBetweenValue.add(map);
    }


    
    public Criteria andIdIsNull() {
        addCriterion("id is null");
        return this;
    }

    public Criteria andIdIsNotNull() {
        addCriterion("id is not null");
        return this;
    }

    public Criteria andIdEqualTo(String value) {
        addCriterion("id =", value, "id");
        return this;
    }

    public Criteria andIdNotEqualTo(String value) {
        addCriterion("id <>", value, "id");
        return this;
    }

    public Criteria andIdGreaterThan(String value) {
        addCriterion("id >", value, "id");
        return this;
    }

    public Criteria andIdGreaterThanOrEqualTo(String value) {
        addCriterion("id >=", value, "id");
        return this;
    }

    public Criteria andIdLessThan(String value) {
        addCriterion("id <", value, "id");
        return this;
    }

    public Criteria andIdLessThanOrEqualTo(String value) {
        addCriterion("id <=", value, "id");
        return this;
    }

    public Criteria andIdLike(String value) {
        addCriterion("id like", value, "id");
        return this;
    }

    public Criteria andIdNotLike(String value) {
        addCriterion("id not like", value, "id");
        return this;
    }

    public Criteria andIdIn(List<String> values) {
        addCriterion("id in", values, "id");
        return this;
    }

    public Criteria andIdNotIn(List<String> values) {
        addCriterion("id not in", values, "id");
        return this;
    }

    public Criteria andIdBetween(String value1, String value2) {
        addCriterion("id between", value1, value2, "id");
        return this;
    }

    public Criteria andIdNotBetween(String value1, String value2) {
        addCriterion("id not between", value1, value2, "id");
        return this;
    }

    
    public Criteria andLogNameIsNull() {
        addCriterion("log_name is null");
        return this;
    }

    public Criteria andLogNameIsNotNull() {
        addCriterion("log_name is not null");
        return this;
    }

    public Criteria andLogNameEqualTo(String value) {
        addCriterion("log_name =", value, "logName");
        return this;
    }

    public Criteria andLogNameNotEqualTo(String value) {
        addCriterion("log_name <>", value, "logName");
        return this;
    }

    public Criteria andLogNameGreaterThan(String value) {
        addCriterion("log_name >", value, "logName");
        return this;
    }

    public Criteria andLogNameGreaterThanOrEqualTo(String value) {
        addCriterion("log_name >=", value, "logName");
        return this;
    }

    public Criteria andLogNameLessThan(String value) {
        addCriterion("log_name <", value, "logName");
        return this;
    }

    public Criteria andLogNameLessThanOrEqualTo(String value) {
        addCriterion("log_name <=", value, "logName");
        return this;
    }

    public Criteria andLogNameLike(String value) {
        addCriterion("log_name like", value, "logName");
        return this;
    }

    public Criteria andLogNameNotLike(String value) {
        addCriterion("log_name not like", value, "logName");
        return this;
    }

    public Criteria andLogNameIn(List<String> values) {
        addCriterion("log_name in", values, "logName");
        return this;
    }

    public Criteria andLogNameNotIn(List<String> values) {
        addCriterion("log_name not in", values, "logName");
        return this;
    }

    public Criteria andLogNameBetween(String value1, String value2) {
        addCriterion("log_name between", value1, value2, "logName");
        return this;
    }

    public Criteria andLogNameNotBetween(String value1, String value2) {
        addCriterion("log_name not between", value1, value2, "logName");
        return this;
    }

    
    public Criteria andCreateDateIsNull() {
        addCriterion("create_date is null");
        return this;
    }

    public Criteria andCreateDateIsNotNull() {
        addCriterion("create_date is not null");
        return this;
    }

    public Criteria andCreateDateEqualTo(String value) {
        addCriterion("create_date =", value, "createDate");
        return this;
    }

    public Criteria andCreateDateNotEqualTo(String value) {
        addCriterion("create_date <>", value, "createDate");
        return this;
    }

    public Criteria andCreateDateGreaterThan(String value) {
        addCriterion("create_date >", value, "createDate");
        return this;
    }

    public Criteria andCreateDateGreaterThanOrEqualTo(String value) {
        addCriterion("create_date >=", value, "createDate");
        return this;
    }

    public Criteria andCreateDateLessThan(String value) {
        addCriterion("create_date <", value, "createDate");
        return this;
    }

    public Criteria andCreateDateLessThanOrEqualTo(String value) {
        addCriterion("create_date <=", value, "createDate");
        return this;
    }

    public Criteria andCreateDateLike(String value) {
        addCriterion("create_date like", value, "createDate");
        return this;
    }

    public Criteria andCreateDateNotLike(String value) {
        addCriterion("create_date not like", value, "createDate");
        return this;
    }

    public Criteria andCreateDateIn(List<String> values) {
        addCriterion("create_date in", values, "createDate");
        return this;
    }

    public Criteria andCreateDateNotIn(List<String> values) {
        addCriterion("create_date not in", values, "createDate");
        return this;
    }

    public Criteria andCreateDateBetween(String value1, String value2) {
        addCriterion("create_date between", value1, value2, "createDate");
        return this;
    }

    public Criteria andCreateDateNotBetween(String value1, String value2) {
        addCriterion("create_date not between", value1, value2, "createDate");
        return this;
    }

        }
}