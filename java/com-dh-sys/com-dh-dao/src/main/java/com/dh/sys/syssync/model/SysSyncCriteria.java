package com.dh.sys.syssync.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 数据库条件操作类
*  create by  Admin at $date.format('yyyy-MM-dd HH:mm:ss',$createDate)
*/
public class SysSyncCriteria {

    /**
    * 排序字段
    */
    protected String orderByClause;

    /**
    * 数据库字段拼接集合
    */
    protected List<Criteria> oredCriteria;

    /**
    * 实例化对象的同时，为oredCriteria实例化
    */
    public SysSyncCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
    * 1.实例化对象的同时，为oredCriteria实例化
    * 2.实例化对象的同时，为排序字段orderByClause赋值
    * @param example
    */
    protected SysSyncCriteria(SysSyncCriteria example) {
        this.orderByClause = example.orderByClause;
        this.oredCriteria = example.oredCriteria;
    }


    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }


    public String getOrderByClause() {
        return orderByClause;
    }


    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }


    public void or(Criteria criteria) {
     oredCriteria.add(criteria);
    }


    public Criteria createCriteria() {
        /**
        * 获取criteria实例化对象
        */
        Criteria criteria = createCriteriaInternal();

        /**
        * 首次调用时如果数据库字段拼接集合为空，
        * 则获取一个新的实例对象加到该结合中
        */
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
    * 获取一个Criteria实例对象
    * @return
    */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }


    /**
    * 清空数据库字段拼接集合
    */
    public void clear() {
        oredCriteria.clear();
    }


    /**
    * 数据库字段拼接实体类
    */
    public static class Criteria {
    protected List<String> criteriaWithoutValue;

    protected List<Map<String, Object>> criteriaWithSingleValue;

    protected List<Map<String, Object>> criteriaWithListValue;

    protected List<Map<String, Object>> criteriaWithBetweenValue;

    protected Criteria() {
        super();
        criteriaWithoutValue = new ArrayList<String>();
        criteriaWithSingleValue = new ArrayList<Map<String, Object>>();
        criteriaWithListValue = new ArrayList<Map<String, Object>>();
        criteriaWithBetweenValue = new ArrayList<Map<String, Object>>();
    }

    public boolean isValid() {
        return criteriaWithoutValue.size() > 0
        || criteriaWithSingleValue.size() > 0
        || criteriaWithListValue.size() > 0
        || criteriaWithBetweenValue.size() > 0;
    }

    public List<String> getCriteriaWithoutValue() {
        return criteriaWithoutValue;
    }

    public List<Map<String, Object>> getCriteriaWithSingleValue() {
        return criteriaWithSingleValue;
    }

    public List<Map<String, Object>> getCriteriaWithListValue() {
        return criteriaWithListValue;
    }

    public List<Map<String, Object>> getCriteriaWithBetweenValue() {
        return criteriaWithBetweenValue;
    }

    protected void addCriterion(String condition) {
        if (condition == null) {
            throw new RuntimeException("Value for condition cannot be null");
        }
        criteriaWithoutValue.add(condition);
    }

    protected void addCriterion(String condition, Object value, String property) {
        if (value == null) {
            throw new RuntimeException("Value for " + property + " cannot be null");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("value", value);
        criteriaWithSingleValue.add(map);
    }

    protected void addCriterion(String condition, List<? extends Object> values, String property) {
        if (values == null || values.size() == 0) {
            throw new RuntimeException("Value list for " + property + " cannot be null or empty");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", values);
        criteriaWithListValue.add(map);
    }

    protected void addCriterion(String condition, Object value1, Object value2, String property) {
        if (value1 == null || value2 == null) {
            throw new RuntimeException("Between values for " + property + " cannot be null");
        }
        List<Object> list = new ArrayList<Object>();
        list.add(value1);
        list.add(value2);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", list);
        criteriaWithBetweenValue.add(map);
    }


    
    public Criteria andIdIsNull() {
        addCriterion("id is null");
        return this;
    }

    public Criteria andIdIsNotNull() {
        addCriterion("id is not null");
        return this;
    }

    public Criteria andIdEqualTo(String value) {
        addCriterion("id =", value, "id");
        return this;
    }

    public Criteria andIdNotEqualTo(String value) {
        addCriterion("id <>", value, "id");
        return this;
    }

    public Criteria andIdGreaterThan(String value) {
        addCriterion("id >", value, "id");
        return this;
    }

    public Criteria andIdGreaterThanOrEqualTo(String value) {
        addCriterion("id >=", value, "id");
        return this;
    }

    public Criteria andIdLessThan(String value) {
        addCriterion("id <", value, "id");
        return this;
    }

    public Criteria andIdLessThanOrEqualTo(String value) {
        addCriterion("id <=", value, "id");
        return this;
    }

    public Criteria andIdLike(String value) {
        addCriterion("id like", value, "id");
        return this;
    }

    public Criteria andIdNotLike(String value) {
        addCriterion("id not like", value, "id");
        return this;
    }

    public Criteria andIdIn(List<String> values) {
        addCriterion("id in", values, "id");
        return this;
    }

    public Criteria andIdNotIn(List<String> values) {
        addCriterion("id not in", values, "id");
        return this;
    }

    public Criteria andIdBetween(String value1, String value2) {
        addCriterion("id between", value1, value2, "id");
        return this;
    }

    public Criteria andIdNotBetween(String value1, String value2) {
        addCriterion("id not between", value1, value2, "id");
        return this;
    }

    
    public Criteria andLoanIdIsNull() {
        addCriterion("loan_id is null");
        return this;
    }

    public Criteria andLoanIdIsNotNull() {
        addCriterion("loan_id is not null");
        return this;
    }

    public Criteria andLoanIdEqualTo(String value) {
        addCriterion("loan_id =", value, "loanId");
        return this;
    }

    public Criteria andLoanIdNotEqualTo(String value) {
        addCriterion("loan_id <>", value, "loanId");
        return this;
    }

    public Criteria andLoanIdGreaterThan(String value) {
        addCriterion("loan_id >", value, "loanId");
        return this;
    }

    public Criteria andLoanIdGreaterThanOrEqualTo(String value) {
        addCriterion("loan_id >=", value, "loanId");
        return this;
    }

    public Criteria andLoanIdLessThan(String value) {
        addCriterion("loan_id <", value, "loanId");
        return this;
    }

    public Criteria andLoanIdLessThanOrEqualTo(String value) {
        addCriterion("loan_id <=", value, "loanId");
        return this;
    }

    public Criteria andLoanIdLike(String value) {
        addCriterion("loan_id like", value, "loanId");
        return this;
    }

    public Criteria andLoanIdNotLike(String value) {
        addCriterion("loan_id not like", value, "loanId");
        return this;
    }

    public Criteria andLoanIdIn(List<String> values) {
        addCriterion("loan_id in", values, "loanId");
        return this;
    }

    public Criteria andLoanIdNotIn(List<String> values) {
        addCriterion("loan_id not in", values, "loanId");
        return this;
    }

    public Criteria andLoanIdBetween(String value1, String value2) {
        addCriterion("loan_id between", value1, value2, "loanId");
        return this;
    }

    public Criteria andLoanIdNotBetween(String value1, String value2) {
        addCriterion("loan_id not between", value1, value2, "loanId");
        return this;
    }

    
    public Criteria andProcessinstanceidIsNull() {
        addCriterion("processInstanceId is null");
        return this;
    }

    public Criteria andProcessinstanceidIsNotNull() {
        addCriterion("processInstanceId is not null");
        return this;
    }

    public Criteria andProcessinstanceidEqualTo(String value) {
        addCriterion("processInstanceId =", value, "processinstanceid");
        return this;
    }

    public Criteria andProcessinstanceidNotEqualTo(String value) {
        addCriterion("processInstanceId <>", value, "processinstanceid");
        return this;
    }

    public Criteria andProcessinstanceidGreaterThan(String value) {
        addCriterion("processInstanceId >", value, "processinstanceid");
        return this;
    }

    public Criteria andProcessinstanceidGreaterThanOrEqualTo(String value) {
        addCriterion("processInstanceId >=", value, "processinstanceid");
        return this;
    }

    public Criteria andProcessinstanceidLessThan(String value) {
        addCriterion("processInstanceId <", value, "processinstanceid");
        return this;
    }

    public Criteria andProcessinstanceidLessThanOrEqualTo(String value) {
        addCriterion("processInstanceId <=", value, "processinstanceid");
        return this;
    }

    public Criteria andProcessinstanceidLike(String value) {
        addCriterion("processInstanceId like", value, "processinstanceid");
        return this;
    }

    public Criteria andProcessinstanceidNotLike(String value) {
        addCriterion("processInstanceId not like", value, "processinstanceid");
        return this;
    }

    public Criteria andProcessinstanceidIn(List<String> values) {
        addCriterion("processInstanceId in", values, "processinstanceid");
        return this;
    }

    public Criteria andProcessinstanceidNotIn(List<String> values) {
        addCriterion("processInstanceId not in", values, "processinstanceid");
        return this;
    }

    public Criteria andProcessinstanceidBetween(String value1, String value2) {
        addCriterion("processInstanceId between", value1, value2, "processinstanceid");
        return this;
    }

    public Criteria andProcessinstanceidNotBetween(String value1, String value2) {
        addCriterion("processInstanceId not between", value1, value2, "processinstanceid");
        return this;
    }

    
    public Criteria andCreteDateIsNull() {
        addCriterion("crete_date is null");
        return this;
    }

    public Criteria andCreteDateIsNotNull() {
        addCriterion("crete_date is not null");
        return this;
    }

    public Criteria andCreteDateEqualTo(String value) {
        addCriterion("crete_date =", value, "creteDate");
        return this;
    }

    public Criteria andCreteDateNotEqualTo(String value) {
        addCriterion("crete_date <>", value, "creteDate");
        return this;
    }

    public Criteria andCreteDateGreaterThan(String value) {
        addCriterion("crete_date >", value, "creteDate");
        return this;
    }

    public Criteria andCreteDateGreaterThanOrEqualTo(String value) {
        addCriterion("crete_date >=", value, "creteDate");
        return this;
    }

    public Criteria andCreteDateLessThan(String value) {
        addCriterion("crete_date <", value, "creteDate");
        return this;
    }

    public Criteria andCreteDateLessThanOrEqualTo(String value) {
        addCriterion("crete_date <=", value, "creteDate");
        return this;
    }

    public Criteria andCreteDateLike(String value) {
        addCriterion("crete_date like", value, "creteDate");
        return this;
    }

    public Criteria andCreteDateNotLike(String value) {
        addCriterion("crete_date not like", value, "creteDate");
        return this;
    }

    public Criteria andCreteDateIn(List<String> values) {
        addCriterion("crete_date in", values, "creteDate");
        return this;
    }

    public Criteria andCreteDateNotIn(List<String> values) {
        addCriterion("crete_date not in", values, "creteDate");
        return this;
    }

    public Criteria andCreteDateBetween(String value1, String value2) {
        addCriterion("crete_date between", value1, value2, "creteDate");
        return this;
    }

    public Criteria andCreteDateNotBetween(String value1, String value2) {
        addCriterion("crete_date not between", value1, value2, "creteDate");
        return this;
    }

    
    public Criteria andOptDateIsNull() {
        addCriterion("opt_date is null");
        return this;
    }

    public Criteria andOptDateIsNotNull() {
        addCriterion("opt_date is not null");
        return this;
    }

    public Criteria andOptDateEqualTo(String value) {
        addCriterion("opt_date =", value, "optDate");
        return this;
    }

    public Criteria andOptDateNotEqualTo(String value) {
        addCriterion("opt_date <>", value, "optDate");
        return this;
    }

    public Criteria andOptDateGreaterThan(String value) {
        addCriterion("opt_date >", value, "optDate");
        return this;
    }

    public Criteria andOptDateGreaterThanOrEqualTo(String value) {
        addCriterion("opt_date >=", value, "optDate");
        return this;
    }

    public Criteria andOptDateLessThan(String value) {
        addCriterion("opt_date <", value, "optDate");
        return this;
    }

    public Criteria andOptDateLessThanOrEqualTo(String value) {
        addCriterion("opt_date <=", value, "optDate");
        return this;
    }

    public Criteria andOptDateLike(String value) {
        addCriterion("opt_date like", value, "optDate");
        return this;
    }

    public Criteria andOptDateNotLike(String value) {
        addCriterion("opt_date not like", value, "optDate");
        return this;
    }

    public Criteria andOptDateIn(List<String> values) {
        addCriterion("opt_date in", values, "optDate");
        return this;
    }

    public Criteria andOptDateNotIn(List<String> values) {
        addCriterion("opt_date not in", values, "optDate");
        return this;
    }

    public Criteria andOptDateBetween(String value1, String value2) {
        addCriterion("opt_date between", value1, value2, "optDate");
        return this;
    }

    public Criteria andOptDateNotBetween(String value1, String value2) {
        addCriterion("opt_date not between", value1, value2, "optDate");
        return this;
    }

    
    public Criteria andFinishDateIsNull() {
        addCriterion("finish_date is null");
        return this;
    }

    public Criteria andFinishDateIsNotNull() {
        addCriterion("finish_date is not null");
        return this;
    }

    public Criteria andFinishDateEqualTo(String value) {
        addCriterion("finish_date =", value, "finishDate");
        return this;
    }

    public Criteria andFinishDateNotEqualTo(String value) {
        addCriterion("finish_date <>", value, "finishDate");
        return this;
    }

    public Criteria andFinishDateGreaterThan(String value) {
        addCriterion("finish_date >", value, "finishDate");
        return this;
    }

    public Criteria andFinishDateGreaterThanOrEqualTo(String value) {
        addCriterion("finish_date >=", value, "finishDate");
        return this;
    }

    public Criteria andFinishDateLessThan(String value) {
        addCriterion("finish_date <", value, "finishDate");
        return this;
    }

    public Criteria andFinishDateLessThanOrEqualTo(String value) {
        addCriterion("finish_date <=", value, "finishDate");
        return this;
    }

    public Criteria andFinishDateLike(String value) {
        addCriterion("finish_date like", value, "finishDate");
        return this;
    }

    public Criteria andFinishDateNotLike(String value) {
        addCriterion("finish_date not like", value, "finishDate");
        return this;
    }

    public Criteria andFinishDateIn(List<String> values) {
        addCriterion("finish_date in", values, "finishDate");
        return this;
    }

    public Criteria andFinishDateNotIn(List<String> values) {
        addCriterion("finish_date not in", values, "finishDate");
        return this;
    }

    public Criteria andFinishDateBetween(String value1, String value2) {
        addCriterion("finish_date between", value1, value2, "finishDate");
        return this;
    }

    public Criteria andFinishDateNotBetween(String value1, String value2) {
        addCriterion("finish_date not between", value1, value2, "finishDate");
        return this;
    }

    
    public Criteria andStatusIsNull() {
        addCriterion("status is null");
        return this;
    }

    public Criteria andStatusIsNotNull() {
        addCriterion("status is not null");
        return this;
    }

    public Criteria andStatusEqualTo(Integer value) {
        addCriterion("status =", value, "status");
        return this;
    }

    public Criteria andStatusNotEqualTo(Integer value) {
        addCriterion("status <>", value, "status");
        return this;
    }

    public Criteria andStatusGreaterThan(Integer value) {
        addCriterion("status >", value, "status");
        return this;
    }

    public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
        addCriterion("status >=", value, "status");
        return this;
    }

    public Criteria andStatusLessThan(Integer value) {
        addCriterion("status <", value, "status");
        return this;
    }

    public Criteria andStatusLessThanOrEqualTo(Integer value) {
        addCriterion("status <=", value, "status");
        return this;
    }

    public Criteria andStatusLike(Integer value) {
        addCriterion("status like", value, "status");
        return this;
    }

    public Criteria andStatusNotLike(Integer value) {
        addCriterion("status not like", value, "status");
        return this;
    }

    public Criteria andStatusIn(List<Integer> values) {
        addCriterion("status in", values, "status");
        return this;
    }

    public Criteria andStatusNotIn(List<Integer> values) {
        addCriterion("status not in", values, "status");
        return this;
    }

    public Criteria andStatusBetween(Integer value1, Integer value2) {
        addCriterion("status between", value1, value2, "status");
        return this;
    }

    public Criteria andStatusNotBetween(Integer value1, String value2) {
        addCriterion("status not between", value1, value2, "status");
        return this;
    }

    
    public Criteria andCallCountIsNull() {
        addCriterion("call_count is null");
        return this;
    }

    public Criteria andCallCountIsNotNull() {
        addCriterion("call_count is not null");
        return this;
    }

    public Criteria andCallCountEqualTo(Integer value) {
        addCriterion("call_count =", value, "callCount");
        return this;
    }

    public Criteria andCallCountNotEqualTo(Integer value) {
        addCriterion("call_count <>", value, "callCount");
        return this;
    }

    public Criteria andCallCountGreaterThan(Integer value) {
        addCriterion("call_count >", value, "callCount");
        return this;
    }

    public Criteria andCallCountGreaterThanOrEqualTo(Integer value) {
        addCriterion("call_count >=", value, "callCount");
        return this;
    }

    public Criteria andCallCountLessThan(Integer value) {
        addCriterion("call_count <", value, "callCount");
        return this;
    }

    public Criteria andCallCountLessThanOrEqualTo(Integer value) {
        addCriterion("call_count <=", value, "callCount");
        return this;
    }

    public Criteria andCallCountLike(Integer value) {
        addCriterion("call_count like", value, "callCount");
        return this;
    }

    public Criteria andCallCountNotLike(Integer value) {
        addCriterion("call_count not like", value, "callCount");
        return this;
    }

    public Criteria andCallCountIn(List<Integer> values) {
        addCriterion("call_count in", values, "callCount");
        return this;
    }

    public Criteria andCallCountNotIn(List<Integer> values) {
        addCriterion("call_count not in", values, "callCount");
        return this;
    }

    public Criteria andCallCountBetween(Integer value1, Integer value2) {
        addCriterion("call_count between", value1, value2, "callCount");
        return this;
    }

    public Criteria andCallCountNotBetween(Integer value1, String value2) {
        addCriterion("call_count not between", value1, value2, "callCount");
        return this;
    }

    
    public Criteria andUidIsNull() {
        addCriterion("uid is null");
        return this;
    }

    public Criteria andUidIsNotNull() {
        addCriterion("uid is not null");
        return this;
    }

    public Criteria andUidEqualTo(String value) {
        addCriterion("uid =", value, "uid");
        return this;
    }

    public Criteria andUidNotEqualTo(String value) {
        addCriterion("uid <>", value, "uid");
        return this;
    }

    public Criteria andUidGreaterThan(String value) {
        addCriterion("uid >", value, "uid");
        return this;
    }

    public Criteria andUidGreaterThanOrEqualTo(String value) {
        addCriterion("uid >=", value, "uid");
        return this;
    }

    public Criteria andUidLessThan(String value) {
        addCriterion("uid <", value, "uid");
        return this;
    }

    public Criteria andUidLessThanOrEqualTo(String value) {
        addCriterion("uid <=", value, "uid");
        return this;
    }

    public Criteria andUidLike(String value) {
        addCriterion("uid like", value, "uid");
        return this;
    }

    public Criteria andUidNotLike(String value) {
        addCriterion("uid not like", value, "uid");
        return this;
    }

    public Criteria andUidIn(List<String> values) {
        addCriterion("uid in", values, "uid");
        return this;
    }

    public Criteria andUidNotIn(List<String> values) {
        addCriterion("uid not in", values, "uid");
        return this;
    }

    public Criteria andUidBetween(String value1, String value2) {
        addCriterion("uid between", value1, value2, "uid");
        return this;
    }

    public Criteria andUidNotBetween(String value1, String value2) {
        addCriterion("uid not between", value1, value2, "uid");
        return this;
    }

        }
}