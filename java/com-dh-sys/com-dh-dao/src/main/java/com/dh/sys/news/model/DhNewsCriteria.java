package com.dh.sys.news.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 数据库条件操作类
*  create by  Admin at $date.format('yyyy-MM-dd HH:mm:ss',$createDate)
*/
public class DhNewsCriteria {

    /**
    * 排序字段
    */
    protected String orderByClause;

    /**
    * 数据库字段拼接集合
    */
    protected List<Criteria> oredCriteria;

    /**
    * 实例化对象的同时，为oredCriteria实例化
    */
    public DhNewsCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
    * 1.实例化对象的同时，为oredCriteria实例化
    * 2.实例化对象的同时，为排序字段orderByClause赋值
    * @param example
    */
    protected DhNewsCriteria(DhNewsCriteria example) {
        this.orderByClause = example.orderByClause;
        this.oredCriteria = example.oredCriteria;
    }


    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }


    public String getOrderByClause() {
        return orderByClause;
    }


    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }


    public void or(Criteria criteria) {
     oredCriteria.add(criteria);
    }


    public Criteria createCriteria() {
        /**
        * 获取criteria实例化对象
        */
        Criteria criteria = createCriteriaInternal();

        /**
        * 首次调用时如果数据库字段拼接集合为空，
        * 则获取一个新的实例对象加到该结合中
        */
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
    * 获取一个Criteria实例对象
    * @return
    */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }


    /**
    * 清空数据库字段拼接集合
    */
    public void clear() {
        oredCriteria.clear();
    }


    /**
    * 数据库字段拼接实体类
    */
    public static class Criteria {
    protected List<String> criteriaWithoutValue;

    protected List<Map<String, Object>> criteriaWithSingleValue;

    protected List<Map<String, Object>> criteriaWithListValue;

    protected List<Map<String, Object>> criteriaWithBetweenValue;

    protected Criteria() {
        super();
        criteriaWithoutValue = new ArrayList<String>();
        criteriaWithSingleValue = new ArrayList<Map<String, Object>>();
        criteriaWithListValue = new ArrayList<Map<String, Object>>();
        criteriaWithBetweenValue = new ArrayList<Map<String, Object>>();
    }

    public boolean isValid() {
        return criteriaWithoutValue.size() > 0
        || criteriaWithSingleValue.size() > 0
        || criteriaWithListValue.size() > 0
        || criteriaWithBetweenValue.size() > 0;
    }

    public List<String> getCriteriaWithoutValue() {
        return criteriaWithoutValue;
    }

    public List<Map<String, Object>> getCriteriaWithSingleValue() {
        return criteriaWithSingleValue;
    }

    public List<Map<String, Object>> getCriteriaWithListValue() {
        return criteriaWithListValue;
    }

    public List<Map<String, Object>> getCriteriaWithBetweenValue() {
        return criteriaWithBetweenValue;
    }

    protected void addCriterion(String condition) {
        if (condition == null) {
            throw new RuntimeException("Value for condition cannot be null");
        }
        criteriaWithoutValue.add(condition);
    }

    protected void addCriterion(String condition, Object value, String property) {
        if (value == null) {
            throw new RuntimeException("Value for " + property + " cannot be null");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("value", value);
        criteriaWithSingleValue.add(map);
    }

    protected void addCriterion(String condition, List<? extends Object> values, String property) {
        if (values == null || values.size() == 0) {
            throw new RuntimeException("Value list for " + property + " cannot be null or empty");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", values);
        criteriaWithListValue.add(map);
    }

    protected void addCriterion(String condition, Object value1, Object value2, String property) {
        if (value1 == null || value2 == null) {
            throw new RuntimeException("Between values for " + property + " cannot be null");
        }
        List<Object> list = new ArrayList<Object>();
        list.add(value1);
        list.add(value2);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", list);
        criteriaWithBetweenValue.add(map);
    }


    
    public Criteria andIdIsNull() {
        addCriterion("ID is null");
        return this;
    }

    public Criteria andIdIsNotNull() {
        addCriterion("ID is not null");
        return this;
    }

    public Criteria andIdEqualTo(String value) {
        addCriterion("ID =", value, "id");
        return this;
    }

    public Criteria andIdNotEqualTo(String value) {
        addCriterion("ID <>", value, "id");
        return this;
    }

    public Criteria andIdGreaterThan(String value) {
        addCriterion("ID >", value, "id");
        return this;
    }

    public Criteria andIdGreaterThanOrEqualTo(String value) {
        addCriterion("ID >=", value, "id");
        return this;
    }

    public Criteria andIdLessThan(String value) {
        addCriterion("ID <", value, "id");
        return this;
    }

    public Criteria andIdLessThanOrEqualTo(String value) {
        addCriterion("ID <=", value, "id");
        return this;
    }

    public Criteria andIdLike(String value) {
        addCriterion("ID like", value, "id");
        return this;
    }

    public Criteria andIdNotLike(String value) {
        addCriterion("ID not like", value, "id");
        return this;
    }

    public Criteria andIdIn(List<String> values) {
        addCriterion("ID in", values, "id");
        return this;
    }

    public Criteria andIdNotIn(List<String> values) {
        addCriterion("ID not in", values, "id");
        return this;
    }

    public Criteria andIdBetween(String value1, String value2) {
        addCriterion("ID between", value1, value2, "id");
        return this;
    }

    public Criteria andIdNotBetween(String value1, String value2) {
        addCriterion("ID not between", value1, value2, "id");
        return this;
    }

    
    public Criteria andTitleIsNull() {
        addCriterion("TITLE is null");
        return this;
    }

    public Criteria andTitleIsNotNull() {
        addCriterion("TITLE is not null");
        return this;
    }

    public Criteria andTitleEqualTo(String value) {
        addCriterion("TITLE =", value, "title");
        return this;
    }

    public Criteria andTitleNotEqualTo(String value) {
        addCriterion("TITLE <>", value, "title");
        return this;
    }

    public Criteria andTitleGreaterThan(String value) {
        addCriterion("TITLE >", value, "title");
        return this;
    }

    public Criteria andTitleGreaterThanOrEqualTo(String value) {
        addCriterion("TITLE >=", value, "title");
        return this;
    }

    public Criteria andTitleLessThan(String value) {
        addCriterion("TITLE <", value, "title");
        return this;
    }

    public Criteria andTitleLessThanOrEqualTo(String value) {
        addCriterion("TITLE <=", value, "title");
        return this;
    }

    public Criteria andTitleLike(String value) {
        addCriterion("TITLE like", value, "title");
        return this;
    }

    public Criteria andTitleNotLike(String value) {
        addCriterion("TITLE not like", value, "title");
        return this;
    }

    public Criteria andTitleIn(List<String> values) {
        addCriterion("TITLE in", values, "title");
        return this;
    }

    public Criteria andTitleNotIn(List<String> values) {
        addCriterion("TITLE not in", values, "title");
        return this;
    }

    public Criteria andTitleBetween(String value1, String value2) {
        addCriterion("TITLE between", value1, value2, "title");
        return this;
    }

    public Criteria andTitleNotBetween(String value1, String value2) {
        addCriterion("TITLE not between", value1, value2, "title");
        return this;
    }

    
    public Criteria andTitleImgIsNull() {
        addCriterion("TITLE_IMG is null");
        return this;
    }

    public Criteria andTitleImgIsNotNull() {
        addCriterion("TITLE_IMG is not null");
        return this;
    }

    public Criteria andTitleImgEqualTo(String value) {
        addCriterion("TITLE_IMG =", value, "titleImg");
        return this;
    }

    public Criteria andTitleImgNotEqualTo(String value) {
        addCriterion("TITLE_IMG <>", value, "titleImg");
        return this;
    }

    public Criteria andTitleImgGreaterThan(String value) {
        addCriterion("TITLE_IMG >", value, "titleImg");
        return this;
    }

    public Criteria andTitleImgGreaterThanOrEqualTo(String value) {
        addCriterion("TITLE_IMG >=", value, "titleImg");
        return this;
    }

    public Criteria andTitleImgLessThan(String value) {
        addCriterion("TITLE_IMG <", value, "titleImg");
        return this;
    }

    public Criteria andTitleImgLessThanOrEqualTo(String value) {
        addCriterion("TITLE_IMG <=", value, "titleImg");
        return this;
    }

    public Criteria andTitleImgLike(String value) {
        addCriterion("TITLE_IMG like", value, "titleImg");
        return this;
    }

    public Criteria andTitleImgNotLike(String value) {
        addCriterion("TITLE_IMG not like", value, "titleImg");
        return this;
    }

    public Criteria andTitleImgIn(List<String> values) {
        addCriterion("TITLE_IMG in", values, "titleImg");
        return this;
    }

    public Criteria andTitleImgNotIn(List<String> values) {
        addCriterion("TITLE_IMG not in", values, "titleImg");
        return this;
    }

    public Criteria andTitleImgBetween(String value1, String value2) {
        addCriterion("TITLE_IMG between", value1, value2, "titleImg");
        return this;
    }

    public Criteria andTitleImgNotBetween(String value1, String value2) {
        addCriterion("TITLE_IMG not between", value1, value2, "titleImg");
        return this;
    }

    
    public Criteria andNewContentIsNull() {
        addCriterion("NEW_CONTENT is null");
        return this;
    }

    public Criteria andNewContentIsNotNull() {
        addCriterion("NEW_CONTENT is not null");
        return this;
    }

    public Criteria andNewContentEqualTo(byte[] value) {
        addCriterion("NEW_CONTENT =", value, "newContent");
        return this;
    }

    public Criteria andNewContentNotEqualTo(byte[] value) {
        addCriterion("NEW_CONTENT <>", value, "newContent");
        return this;
    }

    public Criteria andNewContentGreaterThan(byte[] value) {
        addCriterion("NEW_CONTENT >", value, "newContent");
        return this;
    }

    public Criteria andNewContentGreaterThanOrEqualTo(byte[] value) {
        addCriterion("NEW_CONTENT >=", value, "newContent");
        return this;
    }

    public Criteria andNewContentLessThan(byte[] value) {
        addCriterion("NEW_CONTENT <", value, "newContent");
        return this;
    }

    public Criteria andNewContentLessThanOrEqualTo(byte[] value) {
        addCriterion("NEW_CONTENT <=", value, "newContent");
        return this;
    }

    public Criteria andNewContentLike(byte[] value) {
        addCriterion("NEW_CONTENT like", value, "newContent");
        return this;
    }

    public Criteria andNewContentNotLike(byte[] value) {
        addCriterion("NEW_CONTENT not like", value, "newContent");
        return this;
    }

    public Criteria andNewContentIn(List<byte[]> values) {
        addCriterion("NEW_CONTENT in", values, "newContent");
        return this;
    }

    public Criteria andNewContentNotIn(List<byte[]> values) {
        addCriterion("NEW_CONTENT not in", values, "newContent");
        return this;
    }

    public Criteria andNewContentBetween(byte[] value1, byte[] value2) {
        addCriterion("NEW_CONTENT between", value1, value2, "newContent");
        return this;
    }

    public Criteria andNewContentNotBetween(byte[] value1, String value2) {
        addCriterion("NEW_CONTENT not between", value1, value2, "newContent");
        return this;
    }

    
    public Criteria andCreateDateIsNull() {
        addCriterion("CREATE_DATE is null");
        return this;
    }

    public Criteria andCreateDateIsNotNull() {
        addCriterion("CREATE_DATE is not null");
        return this;
    }

    public Criteria andCreateDateEqualTo(String value) {
        addCriterion("CREATE_DATE =", value, "createDate");
        return this;
    }

    public Criteria andCreateDateNotEqualTo(String value) {
        addCriterion("CREATE_DATE <>", value, "createDate");
        return this;
    }

    public Criteria andCreateDateGreaterThan(String value) {
        addCriterion("CREATE_DATE >", value, "createDate");
        return this;
    }

    public Criteria andCreateDateGreaterThanOrEqualTo(String value) {
        addCriterion("CREATE_DATE >=", value, "createDate");
        return this;
    }

    public Criteria andCreateDateLessThan(String value) {
        addCriterion("CREATE_DATE <", value, "createDate");
        return this;
    }

    public Criteria andCreateDateLessThanOrEqualTo(String value) {
        addCriterion("CREATE_DATE <=", value, "createDate");
        return this;
    }

    public Criteria andCreateDateLike(String value) {
        addCriterion("CREATE_DATE like", value, "createDate");
        return this;
    }

    public Criteria andCreateDateNotLike(String value) {
        addCriterion("CREATE_DATE not like", value, "createDate");
        return this;
    }

    public Criteria andCreateDateIn(List<String> values) {
        addCriterion("CREATE_DATE in", values, "createDate");
        return this;
    }

    public Criteria andCreateDateNotIn(List<String> values) {
        addCriterion("CREATE_DATE not in", values, "createDate");
        return this;
    }

    public Criteria andCreateDateBetween(String value1, String value2) {
        addCriterion("CREATE_DATE between", value1, value2, "createDate");
        return this;
    }

    public Criteria andCreateDateNotBetween(String value1, String value2) {
        addCriterion("CREATE_DATE not between", value1, value2, "createDate");
        return this;
    }

    
    public Criteria andUpdateDateIsNull() {
        addCriterion("UPDATE_DATE is null");
        return this;
    }

    public Criteria andUpdateDateIsNotNull() {
        addCriterion("UPDATE_DATE is not null");
        return this;
    }

    public Criteria andUpdateDateEqualTo(String value) {
        addCriterion("UPDATE_DATE =", value, "updateDate");
        return this;
    }

    public Criteria andUpdateDateNotEqualTo(String value) {
        addCriterion("UPDATE_DATE <>", value, "updateDate");
        return this;
    }

    public Criteria andUpdateDateGreaterThan(String value) {
        addCriterion("UPDATE_DATE >", value, "updateDate");
        return this;
    }

    public Criteria andUpdateDateGreaterThanOrEqualTo(String value) {
        addCriterion("UPDATE_DATE >=", value, "updateDate");
        return this;
    }

    public Criteria andUpdateDateLessThan(String value) {
        addCriterion("UPDATE_DATE <", value, "updateDate");
        return this;
    }

    public Criteria andUpdateDateLessThanOrEqualTo(String value) {
        addCriterion("UPDATE_DATE <=", value, "updateDate");
        return this;
    }

    public Criteria andUpdateDateLike(String value) {
        addCriterion("UPDATE_DATE like", value, "updateDate");
        return this;
    }

    public Criteria andUpdateDateNotLike(String value) {
        addCriterion("UPDATE_DATE not like", value, "updateDate");
        return this;
    }

    public Criteria andUpdateDateIn(List<String> values) {
        addCriterion("UPDATE_DATE in", values, "updateDate");
        return this;
    }

    public Criteria andUpdateDateNotIn(List<String> values) {
        addCriterion("UPDATE_DATE not in", values, "updateDate");
        return this;
    }

    public Criteria andUpdateDateBetween(String value1, String value2) {
        addCriterion("UPDATE_DATE between", value1, value2, "updateDate");
        return this;
    }

    public Criteria andUpdateDateNotBetween(String value1, String value2) {
        addCriterion("UPDATE_DATE not between", value1, value2, "updateDate");
        return this;
    }

    
    public Criteria andFlagIsNull() {
        addCriterion("FLAG is null");
        return this;
    }

    public Criteria andFlagIsNotNull() {
        addCriterion("FLAG is not null");
        return this;
    }

    public Criteria andFlagEqualTo(Integer value) {
        addCriterion("FLAG =", value, "flag");
        return this;
    }

    public Criteria andFlagNotEqualTo(Integer value) {
        addCriterion("FLAG <>", value, "flag");
        return this;
    }

    public Criteria andFlagGreaterThan(Integer value) {
        addCriterion("FLAG >", value, "flag");
        return this;
    }

    public Criteria andFlagGreaterThanOrEqualTo(Integer value) {
        addCriterion("FLAG >=", value, "flag");
        return this;
    }

    public Criteria andFlagLessThan(Integer value) {
        addCriterion("FLAG <", value, "flag");
        return this;
    }

    public Criteria andFlagLessThanOrEqualTo(Integer value) {
        addCriterion("FLAG <=", value, "flag");
        return this;
    }

    public Criteria andFlagLike(Integer value) {
        addCriterion("FLAG like", value, "flag");
        return this;
    }

    public Criteria andFlagNotLike(Integer value) {
        addCriterion("FLAG not like", value, "flag");
        return this;
    }

    public Criteria andFlagIn(List<Integer> values) {
        addCriterion("FLAG in", values, "flag");
        return this;
    }

    public Criteria andFlagNotIn(List<Integer> values) {
        addCriterion("FLAG not in", values, "flag");
        return this;
    }

    public Criteria andFlagBetween(Integer value1, Integer value2) {
        addCriterion("FLAG between", value1, value2, "flag");
        return this;
    }

    public Criteria andFlagNotBetween(Integer value1, String value2) {
        addCriterion("FLAG not between", value1, value2, "flag");
        return this;
    }

    
    public Criteria andDeleteFlagIsNull() {
        addCriterion("DELETE_FLAG is null");
        return this;
    }

    public Criteria andDeleteFlagIsNotNull() {
        addCriterion("DELETE_FLAG is not null");
        return this;
    }

    public Criteria andDeleteFlagEqualTo(Integer value) {
        addCriterion("DELETE_FLAG =", value, "deleteFlag");
        return this;
    }

    public Criteria andDeleteFlagNotEqualTo(Integer value) {
        addCriterion("DELETE_FLAG <>", value, "deleteFlag");
        return this;
    }

    public Criteria andDeleteFlagGreaterThan(Integer value) {
        addCriterion("DELETE_FLAG >", value, "deleteFlag");
        return this;
    }

    public Criteria andDeleteFlagGreaterThanOrEqualTo(Integer value) {
        addCriterion("DELETE_FLAG >=", value, "deleteFlag");
        return this;
    }

    public Criteria andDeleteFlagLessThan(Integer value) {
        addCriterion("DELETE_FLAG <", value, "deleteFlag");
        return this;
    }

    public Criteria andDeleteFlagLessThanOrEqualTo(Integer value) {
        addCriterion("DELETE_FLAG <=", value, "deleteFlag");
        return this;
    }

    public Criteria andDeleteFlagLike(Integer value) {
        addCriterion("DELETE_FLAG like", value, "deleteFlag");
        return this;
    }

    public Criteria andDeleteFlagNotLike(Integer value) {
        addCriterion("DELETE_FLAG not like", value, "deleteFlag");
        return this;
    }

    public Criteria andDeleteFlagIn(List<Integer> values) {
        addCriterion("DELETE_FLAG in", values, "deleteFlag");
        return this;
    }

    public Criteria andDeleteFlagNotIn(List<Integer> values) {
        addCriterion("DELETE_FLAG not in", values, "deleteFlag");
        return this;
    }

    public Criteria andDeleteFlagBetween(Integer value1, Integer value2) {
        addCriterion("DELETE_FLAG between", value1, value2, "deleteFlag");
        return this;
    }

    public Criteria andDeleteFlagNotBetween(Integer value1, String value2) {
        addCriterion("DELETE_FLAG not between", value1, value2, "deleteFlag");
        return this;
    }

    
    public Criteria andEnableStatusIsNull() {
        addCriterion("ENABLE_STATUS is null");
        return this;
    }

    public Criteria andEnableStatusIsNotNull() {
        addCriterion("ENABLE_STATUS is not null");
        return this;
    }

    public Criteria andEnableStatusEqualTo(Integer value) {
        addCriterion("ENABLE_STATUS =", value, "enableStatus");
        return this;
    }

    public Criteria andEnableStatusNotEqualTo(Integer value) {
        addCriterion("ENABLE_STATUS <>", value, "enableStatus");
        return this;
    }

    public Criteria andEnableStatusGreaterThan(Integer value) {
        addCriterion("ENABLE_STATUS >", value, "enableStatus");
        return this;
    }

    public Criteria andEnableStatusGreaterThanOrEqualTo(Integer value) {
        addCriterion("ENABLE_STATUS >=", value, "enableStatus");
        return this;
    }

    public Criteria andEnableStatusLessThan(Integer value) {
        addCriterion("ENABLE_STATUS <", value, "enableStatus");
        return this;
    }

    public Criteria andEnableStatusLessThanOrEqualTo(Integer value) {
        addCriterion("ENABLE_STATUS <=", value, "enableStatus");
        return this;
    }

    public Criteria andEnableStatusLike(Integer value) {
        addCriterion("ENABLE_STATUS like", value, "enableStatus");
        return this;
    }

    public Criteria andEnableStatusNotLike(Integer value) {
        addCriterion("ENABLE_STATUS not like", value, "enableStatus");
        return this;
    }

    public Criteria andEnableStatusIn(List<Integer> values) {
        addCriterion("ENABLE_STATUS in", values, "enableStatus");
        return this;
    }

    public Criteria andEnableStatusNotIn(List<Integer> values) {
        addCriterion("ENABLE_STATUS not in", values, "enableStatus");
        return this;
    }

    public Criteria andEnableStatusBetween(Integer value1, Integer value2) {
        addCriterion("ENABLE_STATUS between", value1, value2, "enableStatus");
        return this;
    }

    public Criteria andEnableStatusNotBetween(Integer value1, String value2) {
        addCriterion("ENABLE_STATUS not between", value1, value2, "enableStatus");
        return this;
    }

    
    public Criteria andNewsCodeIsNull() {
        addCriterion("NEWS_CODE is null");
        return this;
    }

    public Criteria andNewsCodeIsNotNull() {
        addCriterion("NEWS_CODE is not null");
        return this;
    }

    public Criteria andNewsCodeEqualTo(String value) {
        addCriterion("NEWS_CODE =", value, "newsCode");
        return this;
    }

    public Criteria andNewsCodeNotEqualTo(String value) {
        addCriterion("NEWS_CODE <>", value, "newsCode");
        return this;
    }

    public Criteria andNewsCodeGreaterThan(String value) {
        addCriterion("NEWS_CODE >", value, "newsCode");
        return this;
    }

    public Criteria andNewsCodeGreaterThanOrEqualTo(String value) {
        addCriterion("NEWS_CODE >=", value, "newsCode");
        return this;
    }

    public Criteria andNewsCodeLessThan(String value) {
        addCriterion("NEWS_CODE <", value, "newsCode");
        return this;
    }

    public Criteria andNewsCodeLessThanOrEqualTo(String value) {
        addCriterion("NEWS_CODE <=", value, "newsCode");
        return this;
    }

    public Criteria andNewsCodeLike(String value) {
        addCriterion("NEWS_CODE like", value, "newsCode");
        return this;
    }

    public Criteria andNewsCodeNotLike(String value) {
        addCriterion("NEWS_CODE not like", value, "newsCode");
        return this;
    }

    public Criteria andNewsCodeIn(List<String> values) {
        addCriterion("NEWS_CODE in", values, "newsCode");
        return this;
    }

    public Criteria andNewsCodeNotIn(List<String> values) {
        addCriterion("NEWS_CODE not in", values, "newsCode");
        return this;
    }

    public Criteria andNewsCodeBetween(String value1, String value2) {
        addCriterion("NEWS_CODE between", value1, value2, "newsCode");
        return this;
    }

    public Criteria andNewsCodeNotBetween(String value1, String value2) {
        addCriterion("NEWS_CODE not between", value1, value2, "newsCode");
        return this;
    }

    
    public Criteria andMarkIsNull() {
        addCriterion("MARK is null");
        return this;
    }

    public Criteria andMarkIsNotNull() {
        addCriterion("MARK is not null");
        return this;
    }

    public Criteria andMarkEqualTo(String value) {
        addCriterion("MARK =", value, "mark");
        return this;
    }

    public Criteria andMarkNotEqualTo(String value) {
        addCriterion("MARK <>", value, "mark");
        return this;
    }

    public Criteria andMarkGreaterThan(String value) {
        addCriterion("MARK >", value, "mark");
        return this;
    }

    public Criteria andMarkGreaterThanOrEqualTo(String value) {
        addCriterion("MARK >=", value, "mark");
        return this;
    }

    public Criteria andMarkLessThan(String value) {
        addCriterion("MARK <", value, "mark");
        return this;
    }

    public Criteria andMarkLessThanOrEqualTo(String value) {
        addCriterion("MARK <=", value, "mark");
        return this;
    }

    public Criteria andMarkLike(String value) {
        addCriterion("MARK like", value, "mark");
        return this;
    }

    public Criteria andMarkNotLike(String value) {
        addCriterion("MARK not like", value, "mark");
        return this;
    }

    public Criteria andMarkIn(List<String> values) {
        addCriterion("MARK in", values, "mark");
        return this;
    }

    public Criteria andMarkNotIn(List<String> values) {
        addCriterion("MARK not in", values, "mark");
        return this;
    }

    public Criteria andMarkBetween(String value1, String value2) {
        addCriterion("MARK between", value1, value2, "mark");
        return this;
    }

    public Criteria andMarkNotBetween(String value1, String value2) {
        addCriterion("MARK not between", value1, value2, "mark");
        return this;
    }

        }
}