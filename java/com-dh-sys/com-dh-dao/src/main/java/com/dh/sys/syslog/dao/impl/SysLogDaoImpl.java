package com.dh.sys.syslog.dao.impl;

import java.util.List;
import com.dh.sys.common.BaseDao;
import org.springframework.stereotype.Repository;

import com.dh.sys.syslog.dao.SysLogDao;
import com.dh.sys.syslog.model.SysLog;
import com.dh.sys.syslog.model.SysLogCriteria;

@Repository("SysLogDao")
public class SysLogDaoImpl extends BaseDao implements SysLogDao {

    public SysLogDaoImpl() {
        super();
    }

    public int countByExample(SysLogCriteria example) {
        Integer count = (Integer)  getSqlMapClientTemplate().queryForObject("sys_log.ibatorgenerated_countByExample", example);
        return count;
    }

    public int deleteByExample(SysLogCriteria example) {
        int rows = getSqlMapClientTemplate().delete("sys_log.ibatorgenerated_deleteByExample", example);
        return rows;
    }

    public int deleteByPrimaryKey(String id) {
    SysLog key = new SysLog();
        key.setId(id);
        int rows = getSqlMapClientTemplate().delete("sys_log.ibatorgenerated_deleteByPrimaryKey", key);
        return rows;
    }

    public void insert(SysLog record) {
        getSqlMapClientTemplate().insert("sys_log.ibatorgenerated_insert", record);
    }

    public void insertSelective(SysLog record) {
        getSqlMapClientTemplate().insert("sys_log.ibatorgenerated_insertSelective", record);
    }

    @SuppressWarnings("unchecked")
    public List<SysLog> selectByExample(SysLogCriteria example,Integer page, Integer rows) {
        List<SysLog> list = getSqlMapClientTemplate().queryForList("sys_log.ibatorgenerated_selectByExample", example,page, rows);
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<SysLog> selectByExample(SysLogCriteria example) {
        List<SysLog> list = getSqlMapClientTemplate().queryForList("sys_log.ibatorgenerated_selectByExample", example);
        return list;
    }

    public SysLog selectByPrimaryKey(String id) {
        SysLog key = new SysLog();
        key.setId(id);
        SysLog record = (SysLog) getSqlMapClientTemplate().queryForObject("sys_log.ibatorgenerated_selectByPrimaryKey", key);
        return record;
    }

    public int updateByExampleSelective(SysLog record, SysLogCriteria example) {
        UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
        int rows = getSqlMapClientTemplate().update("sys_log.ibatorgenerated_updateByExampleSelective", parms);
        return rows;
    }

    public int updateByExample(SysLog record, SysLogCriteria example) {
        UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
        int rows = getSqlMapClientTemplate().update("sys_log.ibatorgenerated_updateByExample", parms);
        return rows;
    }

    public int updateByPrimaryKeySelective(SysLog record) {
        int rows = getSqlMapClientTemplate().update("sys_log.ibatorgenerated_updateByPrimaryKeySelective", record);
        return rows;
    }

    public int updateByPrimaryKey(SysLog record) {
        int rows = getSqlMapClientTemplate().update("sys_log.ibatorgenerated_updateByPrimaryKey", record);
        return rows;
    }

     private static class UpdateByExampleParms extends SysLogCriteria {
        private Object record;

        public UpdateByExampleParms(Object record, SysLogCriteria example) {
            super(example);
            this.record = record;
        }

        public Object getRecord() {
            return record;
        }
    }
}