package com.dh.sys.syscode.dao;

import com.dh.sys.syscode.model.SysCode;
import com.dh.sys.syscode.model.SysCodeCriteria;
import java.util.List;

public interface SysCodeDao {

    int countByExample(SysCodeCriteria example);


    int deleteByExample(SysCodeCriteria example);


    int deleteByPrimaryKey(String id);


    void insert(SysCode record);


    void insertSelective(SysCode record);


    List<SysCode> selectByExample(SysCodeCriteria example,Integer page, Integer rows);


    SysCode selectByPrimaryKey(String id);


    int updateByExampleSelective(SysCode record, SysCodeCriteria example);


    int updateByExample(SysCode record, SysCodeCriteria example);


    int updateByPrimaryKeySelective(SysCode record);


    int updateByPrimaryKey(SysCode record);

    List<SysCode> selectByExample(SysCodeCriteria example);

}