package com.dh.sys.statistics.dao;

import com.dh.sys.statistics.model.BlogStatistics;
import com.dh.sys.statistics.model.BlogStatisticsCriteria;
import java.util.List;

public interface BlogStatisticsDao {

    int countByExample(BlogStatisticsCriteria example);


    int deleteByExample(BlogStatisticsCriteria example);


    int deleteByPrimaryKey(String id);


    void insert(BlogStatistics record);


    void insertSelective(BlogStatistics record);


    List<BlogStatistics> selectByExample(BlogStatisticsCriteria example,Integer page, Integer rows);


    BlogStatistics selectByPrimaryKey(String id);


    int updateByExampleSelective(BlogStatistics record, BlogStatisticsCriteria example);


    int updateByExample(BlogStatistics record, BlogStatisticsCriteria example);


    int updateByPrimaryKeySelective(BlogStatistics record);


    int updateByPrimaryKey(BlogStatistics record);

    List<BlogStatistics> selectByExample(BlogStatisticsCriteria example);

    
}