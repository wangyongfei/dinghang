package com.dh.sys.sysconfigcenter.dao;

import com.dh.sys.sysconfigcenter.model.SysConfigCenter;
import com.dh.sys.sysconfigcenter.model.SysConfigCenterCriteria;
import java.util.List;

public interface SysConfigCenterDao {

    int countByExample(SysConfigCenterCriteria example);


    int deleteByExample(SysConfigCenterCriteria example);


    int deleteByPrimaryKey(String id);


    void insert(SysConfigCenter record);


    void insertSelective(SysConfigCenter record);


    List<SysConfigCenter> selectByExample(SysConfigCenterCriteria example,Integer page, Integer rows);


    SysConfigCenter selectByPrimaryKey(String id);


    int updateByExampleSelective(SysConfigCenter record, SysConfigCenterCriteria example);


    int updateByExample(SysConfigCenter record, SysConfigCenterCriteria example);


    int updateByPrimaryKeySelective(SysConfigCenter record);


    int updateByPrimaryKey(SysConfigCenter record);

    List<SysConfigCenter> selectByExample(SysConfigCenterCriteria example);

    
}