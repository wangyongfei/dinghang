package com.dh.sys.sysconfigcenter.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 数据库条件操作类
*  create by  Admin at $date.format('yyyy-MM-dd HH:mm:ss',$createDate)
*/
public class SysConfigCenterCriteria {

    /**
    * 排序字段
    */
    protected String orderByClause;

    /**
    * 数据库字段拼接集合
    */
    protected List<Criteria> oredCriteria;

    /**
    * 实例化对象的同时，为oredCriteria实例化
    */
    public SysConfigCenterCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
    * 1.实例化对象的同时，为oredCriteria实例化
    * 2.实例化对象的同时，为排序字段orderByClause赋值
    * @param example
    */
    protected SysConfigCenterCriteria(SysConfigCenterCriteria example) {
        this.orderByClause = example.orderByClause;
        this.oredCriteria = example.oredCriteria;
    }


    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }


    public String getOrderByClause() {
        return orderByClause;
    }


    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }


    public void or(Criteria criteria) {
     oredCriteria.add(criteria);
    }


    public Criteria createCriteria() {
        /**
        * 获取criteria实例化对象
        */
        Criteria criteria = createCriteriaInternal();

        /**
        * 首次调用时如果数据库字段拼接集合为空，
        * 则获取一个新的实例对象加到该结合中
        */
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
    * 获取一个Criteria实例对象
    * @return
    */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }


    /**
    * 清空数据库字段拼接集合
    */
    public void clear() {
        oredCriteria.clear();
    }


    /**
    * 数据库字段拼接实体类
    */
    public static class Criteria {
    protected List<String> criteriaWithoutValue;

    protected List<Map<String, Object>> criteriaWithSingleValue;

    protected List<Map<String, Object>> criteriaWithListValue;

    protected List<Map<String, Object>> criteriaWithBetweenValue;

    protected Criteria() {
        super();
        criteriaWithoutValue = new ArrayList<String>();
        criteriaWithSingleValue = new ArrayList<Map<String, Object>>();
        criteriaWithListValue = new ArrayList<Map<String, Object>>();
        criteriaWithBetweenValue = new ArrayList<Map<String, Object>>();
    }

    public boolean isValid() {
        return criteriaWithoutValue.size() > 0
        || criteriaWithSingleValue.size() > 0
        || criteriaWithListValue.size() > 0
        || criteriaWithBetweenValue.size() > 0;
    }

    public List<String> getCriteriaWithoutValue() {
        return criteriaWithoutValue;
    }

    public List<Map<String, Object>> getCriteriaWithSingleValue() {
        return criteriaWithSingleValue;
    }

    public List<Map<String, Object>> getCriteriaWithListValue() {
        return criteriaWithListValue;
    }

    public List<Map<String, Object>> getCriteriaWithBetweenValue() {
        return criteriaWithBetweenValue;
    }

    protected void addCriterion(String condition) {
        if (condition == null) {
            throw new RuntimeException("Value for condition cannot be null");
        }
        criteriaWithoutValue.add(condition);
    }

    protected void addCriterion(String condition, Object value, String property) {
        if (value == null) {
            throw new RuntimeException("Value for " + property + " cannot be null");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("value", value);
        criteriaWithSingleValue.add(map);
    }

    protected void addCriterion(String condition, List<? extends Object> values, String property) {
        if (values == null || values.size() == 0) {
            throw new RuntimeException("Value list for " + property + " cannot be null or empty");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", values);
        criteriaWithListValue.add(map);
    }

    protected void addCriterion(String condition, Object value1, Object value2, String property) {
        if (value1 == null || value2 == null) {
            throw new RuntimeException("Between values for " + property + " cannot be null");
        }
        List<Object> list = new ArrayList<Object>();
        list.add(value1);
        list.add(value2);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", list);
        criteriaWithBetweenValue.add(map);
    }


    
    public Criteria andIdIsNull() {
        addCriterion("ID is null");
        return this;
    }

    public Criteria andIdIsNotNull() {
        addCriterion("ID is not null");
        return this;
    }

    public Criteria andIdEqualTo(String value) {
        addCriterion("ID =", value, "id");
        return this;
    }

    public Criteria andIdNotEqualTo(String value) {
        addCriterion("ID <>", value, "id");
        return this;
    }

    public Criteria andIdGreaterThan(String value) {
        addCriterion("ID >", value, "id");
        return this;
    }

    public Criteria andIdGreaterThanOrEqualTo(String value) {
        addCriterion("ID >=", value, "id");
        return this;
    }

    public Criteria andIdLessThan(String value) {
        addCriterion("ID <", value, "id");
        return this;
    }

    public Criteria andIdLessThanOrEqualTo(String value) {
        addCriterion("ID <=", value, "id");
        return this;
    }

    public Criteria andIdLike(String value) {
        addCriterion("ID like", value, "id");
        return this;
    }

    public Criteria andIdNotLike(String value) {
        addCriterion("ID not like", value, "id");
        return this;
    }

    public Criteria andIdIn(List<String> values) {
        addCriterion("ID in", values, "id");
        return this;
    }

    public Criteria andIdNotIn(List<String> values) {
        addCriterion("ID not in", values, "id");
        return this;
    }

    public Criteria andIdBetween(String value1, String value2) {
        addCriterion("ID between", value1, value2, "id");
        return this;
    }

    public Criteria andIdNotBetween(String value1, String value2) {
        addCriterion("ID not between", value1, value2, "id");
        return this;
    }

    
    public Criteria andSysTypeIsNull() {
        addCriterion("SYS_TYPE is null");
        return this;
    }

    public Criteria andSysTypeIsNotNull() {
        addCriterion("SYS_TYPE is not null");
        return this;
    }

    public Criteria andSysTypeEqualTo(Integer value) {
        addCriterion("SYS_TYPE =", value, "sysType");
        return this;
    }

    public Criteria andSysTypeNotEqualTo(Integer value) {
        addCriterion("SYS_TYPE <>", value, "sysType");
        return this;
    }

    public Criteria andSysTypeGreaterThan(Integer value) {
        addCriterion("SYS_TYPE >", value, "sysType");
        return this;
    }

    public Criteria andSysTypeGreaterThanOrEqualTo(Integer value) {
        addCriterion("SYS_TYPE >=", value, "sysType");
        return this;
    }

    public Criteria andSysTypeLessThan(Integer value) {
        addCriterion("SYS_TYPE <", value, "sysType");
        return this;
    }

    public Criteria andSysTypeLessThanOrEqualTo(Integer value) {
        addCriterion("SYS_TYPE <=", value, "sysType");
        return this;
    }

    public Criteria andSysTypeLike(Integer value) {
        addCriterion("SYS_TYPE like", value, "sysType");
        return this;
    }

    public Criteria andSysTypeNotLike(Integer value) {
        addCriterion("SYS_TYPE not like", value, "sysType");
        return this;
    }

    public Criteria andSysTypeIn(List<Integer> values) {
        addCriterion("SYS_TYPE in", values, "sysType");
        return this;
    }

    public Criteria andSysTypeNotIn(List<Integer> values) {
        addCriterion("SYS_TYPE not in", values, "sysType");
        return this;
    }

    public Criteria andSysTypeBetween(Integer value1, Integer value2) {
        addCriterion("SYS_TYPE between", value1, value2, "sysType");
        return this;
    }

    public Criteria andSysTypeNotBetween(Integer value1, String value2) {
        addCriterion("SYS_TYPE not between", value1, value2, "sysType");
        return this;
    }

    
    public Criteria andDescribeContentIsNull() {
        addCriterion("DESCRIBE_CONTENT is null");
        return this;
    }

    public Criteria andDescribeContentIsNotNull() {
        addCriterion("DESCRIBE_CONTENT is not null");
        return this;
    }

    public Criteria andDescribeContentEqualTo(String value) {
        addCriterion("DESCRIBE_CONTENT =", value, "describeContent");
        return this;
    }

    public Criteria andDescribeContentNotEqualTo(String value) {
        addCriterion("DESCRIBE_CONTENT <>", value, "describeContent");
        return this;
    }

    public Criteria andDescribeContentGreaterThan(String value) {
        addCriterion("DESCRIBE_CONTENT >", value, "describeContent");
        return this;
    }

    public Criteria andDescribeContentGreaterThanOrEqualTo(String value) {
        addCriterion("DESCRIBE_CONTENT >=", value, "describeContent");
        return this;
    }

    public Criteria andDescribeContentLessThan(String value) {
        addCriterion("DESCRIBE_CONTENT <", value, "describeContent");
        return this;
    }

    public Criteria andDescribeContentLessThanOrEqualTo(String value) {
        addCriterion("DESCRIBE_CONTENT <=", value, "describeContent");
        return this;
    }

    public Criteria andDescribeContentLike(String value) {
        addCriterion("DESCRIBE_CONTENT like", value, "describeContent");
        return this;
    }

    public Criteria andDescribeContentNotLike(String value) {
        addCriterion("DESCRIBE_CONTENT not like", value, "describeContent");
        return this;
    }

    public Criteria andDescribeContentIn(List<String> values) {
        addCriterion("DESCRIBE_CONTENT in", values, "describeContent");
        return this;
    }

    public Criteria andDescribeContentNotIn(List<String> values) {
        addCriterion("DESCRIBE_CONTENT not in", values, "describeContent");
        return this;
    }

    public Criteria andDescribeContentBetween(String value1, String value2) {
        addCriterion("DESCRIBE_CONTENT between", value1, value2, "describeContent");
        return this;
    }

    public Criteria andDescribeContentNotBetween(String value1, String value2) {
        addCriterion("DESCRIBE_CONTENT not between", value1, value2, "describeContent");
        return this;
    }

    
    public Criteria andJsonStrIsNull() {
        addCriterion("JSON_STR is null");
        return this;
    }

    public Criteria andJsonStrIsNotNull() {
        addCriterion("JSON_STR is not null");
        return this;
    }

    public Criteria andJsonStrEqualTo(String value) {
        addCriterion("JSON_STR =", value, "jsonStr");
        return this;
    }

    public Criteria andJsonStrNotEqualTo(String value) {
        addCriterion("JSON_STR <>", value, "jsonStr");
        return this;
    }

    public Criteria andJsonStrGreaterThan(String value) {
        addCriterion("JSON_STR >", value, "jsonStr");
        return this;
    }

    public Criteria andJsonStrGreaterThanOrEqualTo(String value) {
        addCriterion("JSON_STR >=", value, "jsonStr");
        return this;
    }

    public Criteria andJsonStrLessThan(String value) {
        addCriterion("JSON_STR <", value, "jsonStr");
        return this;
    }

    public Criteria andJsonStrLessThanOrEqualTo(String value) {
        addCriterion("JSON_STR <=", value, "jsonStr");
        return this;
    }

    public Criteria andJsonStrLike(String value) {
        addCriterion("JSON_STR like", value, "jsonStr");
        return this;
    }

    public Criteria andJsonStrNotLike(String value) {
        addCriterion("JSON_STR not like", value, "jsonStr");
        return this;
    }

    public Criteria andJsonStrIn(List<String> values) {
        addCriterion("JSON_STR in", values, "jsonStr");
        return this;
    }

    public Criteria andJsonStrNotIn(List<String> values) {
        addCriterion("JSON_STR not in", values, "jsonStr");
        return this;
    }

    public Criteria andJsonStrBetween(String value1, String value2) {
        addCriterion("JSON_STR between", value1, value2, "jsonStr");
        return this;
    }

    public Criteria andJsonStrNotBetween(String value1, String value2) {
        addCriterion("JSON_STR not between", value1, value2, "jsonStr");
        return this;
    }

    
    public Criteria andCreateDateIsNull() {
        addCriterion("CREATE_DATE is null");
        return this;
    }

    public Criteria andCreateDateIsNotNull() {
        addCriterion("CREATE_DATE is not null");
        return this;
    }

    public Criteria andCreateDateEqualTo(String value) {
        addCriterion("CREATE_DATE =", value, "createDate");
        return this;
    }

    public Criteria andCreateDateNotEqualTo(String value) {
        addCriterion("CREATE_DATE <>", value, "createDate");
        return this;
    }

    public Criteria andCreateDateGreaterThan(String value) {
        addCriterion("CREATE_DATE >", value, "createDate");
        return this;
    }

    public Criteria andCreateDateGreaterThanOrEqualTo(String value) {
        addCriterion("CREATE_DATE >=", value, "createDate");
        return this;
    }

    public Criteria andCreateDateLessThan(String value) {
        addCriterion("CREATE_DATE <", value, "createDate");
        return this;
    }

    public Criteria andCreateDateLessThanOrEqualTo(String value) {
        addCriterion("CREATE_DATE <=", value, "createDate");
        return this;
    }

    public Criteria andCreateDateLike(String value) {
        addCriterion("CREATE_DATE like", value, "createDate");
        return this;
    }

    public Criteria andCreateDateNotLike(String value) {
        addCriterion("CREATE_DATE not like", value, "createDate");
        return this;
    }

    public Criteria andCreateDateIn(List<String> values) {
        addCriterion("CREATE_DATE in", values, "createDate");
        return this;
    }

    public Criteria andCreateDateNotIn(List<String> values) {
        addCriterion("CREATE_DATE not in", values, "createDate");
        return this;
    }

    public Criteria andCreateDateBetween(String value1, String value2) {
        addCriterion("CREATE_DATE between", value1, value2, "createDate");
        return this;
    }

    public Criteria andCreateDateNotBetween(String value1, String value2) {
        addCriterion("CREATE_DATE not between", value1, value2, "createDate");
        return this;
    }

    
    public Criteria andUpdateDateIsNull() {
        addCriterion("UPDATE_DATE is null");
        return this;
    }

    public Criteria andUpdateDateIsNotNull() {
        addCriterion("UPDATE_DATE is not null");
        return this;
    }

    public Criteria andUpdateDateEqualTo(String value) {
        addCriterion("UPDATE_DATE =", value, "updateDate");
        return this;
    }

    public Criteria andUpdateDateNotEqualTo(String value) {
        addCriterion("UPDATE_DATE <>", value, "updateDate");
        return this;
    }

    public Criteria andUpdateDateGreaterThan(String value) {
        addCriterion("UPDATE_DATE >", value, "updateDate");
        return this;
    }

    public Criteria andUpdateDateGreaterThanOrEqualTo(String value) {
        addCriterion("UPDATE_DATE >=", value, "updateDate");
        return this;
    }

    public Criteria andUpdateDateLessThan(String value) {
        addCriterion("UPDATE_DATE <", value, "updateDate");
        return this;
    }

    public Criteria andUpdateDateLessThanOrEqualTo(String value) {
        addCriterion("UPDATE_DATE <=", value, "updateDate");
        return this;
    }

    public Criteria andUpdateDateLike(String value) {
        addCriterion("UPDATE_DATE like", value, "updateDate");
        return this;
    }

    public Criteria andUpdateDateNotLike(String value) {
        addCriterion("UPDATE_DATE not like", value, "updateDate");
        return this;
    }

    public Criteria andUpdateDateIn(List<String> values) {
        addCriterion("UPDATE_DATE in", values, "updateDate");
        return this;
    }

    public Criteria andUpdateDateNotIn(List<String> values) {
        addCriterion("UPDATE_DATE not in", values, "updateDate");
        return this;
    }

    public Criteria andUpdateDateBetween(String value1, String value2) {
        addCriterion("UPDATE_DATE between", value1, value2, "updateDate");
        return this;
    }

    public Criteria andUpdateDateNotBetween(String value1, String value2) {
        addCriterion("UPDATE_DATE not between", value1, value2, "updateDate");
        return this;
    }

    
    public Criteria andParamSetIsNull() {
        addCriterion("PARAM_SET is null");
        return this;
    }

    public Criteria andParamSetIsNotNull() {
        addCriterion("PARAM_SET is not null");
        return this;
    }

    public Criteria andParamSetEqualTo(String value) {
        addCriterion("PARAM_SET =", value, "paramSet");
        return this;
    }

    public Criteria andParamSetNotEqualTo(String value) {
        addCriterion("PARAM_SET <>", value, "paramSet");
        return this;
    }

    public Criteria andParamSetGreaterThan(String value) {
        addCriterion("PARAM_SET >", value, "paramSet");
        return this;
    }

    public Criteria andParamSetGreaterThanOrEqualTo(String value) {
        addCriterion("PARAM_SET >=", value, "paramSet");
        return this;
    }

    public Criteria andParamSetLessThan(String value) {
        addCriterion("PARAM_SET <", value, "paramSet");
        return this;
    }

    public Criteria andParamSetLessThanOrEqualTo(String value) {
        addCriterion("PARAM_SET <=", value, "paramSet");
        return this;
    }

    public Criteria andParamSetLike(String value) {
        addCriterion("PARAM_SET like", value, "paramSet");
        return this;
    }

    public Criteria andParamSetNotLike(String value) {
        addCriterion("PARAM_SET not like", value, "paramSet");
        return this;
    }

    public Criteria andParamSetIn(List<String> values) {
        addCriterion("PARAM_SET in", values, "paramSet");
        return this;
    }

    public Criteria andParamSetNotIn(List<String> values) {
        addCriterion("PARAM_SET not in", values, "paramSet");
        return this;
    }

    public Criteria andParamSetBetween(String value1, String value2) {
        addCriterion("PARAM_SET between", value1, value2, "paramSet");
        return this;
    }

    public Criteria andParamSetNotBetween(String value1, String value2) {
        addCriterion("PARAM_SET not between", value1, value2, "paramSet");
        return this;
    }

    
    public Criteria andCreateUserIsNull() {
        addCriterion("CREATE_USER is null");
        return this;
    }

    public Criteria andCreateUserIsNotNull() {
        addCriterion("CREATE_USER is not null");
        return this;
    }

    public Criteria andCreateUserEqualTo(String value) {
        addCriterion("CREATE_USER =", value, "createUser");
        return this;
    }

    public Criteria andCreateUserNotEqualTo(String value) {
        addCriterion("CREATE_USER <>", value, "createUser");
        return this;
    }

    public Criteria andCreateUserGreaterThan(String value) {
        addCriterion("CREATE_USER >", value, "createUser");
        return this;
    }

    public Criteria andCreateUserGreaterThanOrEqualTo(String value) {
        addCriterion("CREATE_USER >=", value, "createUser");
        return this;
    }

    public Criteria andCreateUserLessThan(String value) {
        addCriterion("CREATE_USER <", value, "createUser");
        return this;
    }

    public Criteria andCreateUserLessThanOrEqualTo(String value) {
        addCriterion("CREATE_USER <=", value, "createUser");
        return this;
    }

    public Criteria andCreateUserLike(String value) {
        addCriterion("CREATE_USER like", value, "createUser");
        return this;
    }

    public Criteria andCreateUserNotLike(String value) {
        addCriterion("CREATE_USER not like", value, "createUser");
        return this;
    }

    public Criteria andCreateUserIn(List<String> values) {
        addCriterion("CREATE_USER in", values, "createUser");
        return this;
    }

    public Criteria andCreateUserNotIn(List<String> values) {
        addCriterion("CREATE_USER not in", values, "createUser");
        return this;
    }

    public Criteria andCreateUserBetween(String value1, String value2) {
        addCriterion("CREATE_USER between", value1, value2, "createUser");
        return this;
    }

    public Criteria andCreateUserNotBetween(String value1, String value2) {
        addCriterion("CREATE_USER not between", value1, value2, "createUser");
        return this;
    }

    
    public Criteria andUpdateUserIsNull() {
        addCriterion("UPDATE_USER is null");
        return this;
    }

    public Criteria andUpdateUserIsNotNull() {
        addCriterion("UPDATE_USER is not null");
        return this;
    }

    public Criteria andUpdateUserEqualTo(String value) {
        addCriterion("UPDATE_USER =", value, "updateUser");
        return this;
    }

    public Criteria andUpdateUserNotEqualTo(String value) {
        addCriterion("UPDATE_USER <>", value, "updateUser");
        return this;
    }

    public Criteria andUpdateUserGreaterThan(String value) {
        addCriterion("UPDATE_USER >", value, "updateUser");
        return this;
    }

    public Criteria andUpdateUserGreaterThanOrEqualTo(String value) {
        addCriterion("UPDATE_USER >=", value, "updateUser");
        return this;
    }

    public Criteria andUpdateUserLessThan(String value) {
        addCriterion("UPDATE_USER <", value, "updateUser");
        return this;
    }

    public Criteria andUpdateUserLessThanOrEqualTo(String value) {
        addCriterion("UPDATE_USER <=", value, "updateUser");
        return this;
    }

    public Criteria andUpdateUserLike(String value) {
        addCriterion("UPDATE_USER like", value, "updateUser");
        return this;
    }

    public Criteria andUpdateUserNotLike(String value) {
        addCriterion("UPDATE_USER not like", value, "updateUser");
        return this;
    }

    public Criteria andUpdateUserIn(List<String> values) {
        addCriterion("UPDATE_USER in", values, "updateUser");
        return this;
    }

    public Criteria andUpdateUserNotIn(List<String> values) {
        addCriterion("UPDATE_USER not in", values, "updateUser");
        return this;
    }

    public Criteria andUpdateUserBetween(String value1, String value2) {
        addCriterion("UPDATE_USER between", value1, value2, "updateUser");
        return this;
    }

    public Criteria andUpdateUserNotBetween(String value1, String value2) {
        addCriterion("UPDATE_USER not between", value1, value2, "updateUser");
        return this;
    }

        }
}