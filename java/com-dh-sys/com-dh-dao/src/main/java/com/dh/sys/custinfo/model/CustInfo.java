package com.dh.sys.custinfo.model;

import java.io.Serializable;

public class CustInfo implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
    * 
    */
    private String id;


    /**
    * 用户编号
    */
    private String userCode;


    /**
    * 用户昵称
    */
    private String userName;


    /**
    * 创建时间
    */
    private String createTime;


    /**
    * 密码
    */
    private String password;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    

}