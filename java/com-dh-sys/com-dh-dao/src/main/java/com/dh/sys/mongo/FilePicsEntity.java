package com.dh.sys.mongo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by Administrator on 2016/8/20.
 */

@Document(collection = "news")
public class FilePicsEntity {

    @Id
    private String id;
    private String name;
    private String titleImgId;
    private byte[] content;
    private Date createDate;

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitleImgId() {
        return titleImgId;
    }

    public void setTitleImgId(String titleImgId) {
        this.titleImgId = titleImgId;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
}
