package com.dh.sys.sysuser.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 数据库条件操作类
*  create by  Admin at $date.format('yyyy-MM-dd HH:mm:ss',$createDate)
*/
public class SysUserCriteria {

    /**
    * 排序字段
    */
    protected String orderByClause;

    /**
    * 数据库字段拼接集合
    */
    protected List<Criteria> oredCriteria;

    /**
    * 实例化对象的同时，为oredCriteria实例化
    */
    public SysUserCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
    * 1.实例化对象的同时，为oredCriteria实例化
    * 2.实例化对象的同时，为排序字段orderByClause赋值
    * @param example
    */
    protected SysUserCriteria(SysUserCriteria example) {
        this.orderByClause = example.orderByClause;
        this.oredCriteria = example.oredCriteria;
    }


    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }


    public String getOrderByClause() {
        return orderByClause;
    }


    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }


    public void or(Criteria criteria) {
     oredCriteria.add(criteria);
    }


    public Criteria createCriteria() {
        /**
        * 获取criteria实例化对象
        */
        Criteria criteria = createCriteriaInternal();

        /**
        * 首次调用时如果数据库字段拼接集合为空，
        * 则获取一个新的实例对象加到该结合中
        */
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
    * 获取一个Criteria实例对象
    * @return
    */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }


    /**
    * 清空数据库字段拼接集合
    */
    public void clear() {
        oredCriteria.clear();
    }


    /**
    * 数据库字段拼接实体类
    */
    public static class Criteria {
    protected List<String> criteriaWithoutValue;

    protected List<Map<String, Object>> criteriaWithSingleValue;

    protected List<Map<String, Object>> criteriaWithListValue;

    protected List<Map<String, Object>> criteriaWithBetweenValue;

    protected Criteria() {
        super();
        criteriaWithoutValue = new ArrayList<String>();
        criteriaWithSingleValue = new ArrayList<Map<String, Object>>();
        criteriaWithListValue = new ArrayList<Map<String, Object>>();
        criteriaWithBetweenValue = new ArrayList<Map<String, Object>>();
    }

    public boolean isValid() {
        return criteriaWithoutValue.size() > 0
        || criteriaWithSingleValue.size() > 0
        || criteriaWithListValue.size() > 0
        || criteriaWithBetweenValue.size() > 0;
    }

    public List<String> getCriteriaWithoutValue() {
        return criteriaWithoutValue;
    }

    public List<Map<String, Object>> getCriteriaWithSingleValue() {
        return criteriaWithSingleValue;
    }

    public List<Map<String, Object>> getCriteriaWithListValue() {
        return criteriaWithListValue;
    }

    public List<Map<String, Object>> getCriteriaWithBetweenValue() {
        return criteriaWithBetweenValue;
    }

    protected void addCriterion(String condition) {
        if (condition == null) {
            throw new RuntimeException("Value for condition cannot be null");
        }
        criteriaWithoutValue.add(condition);
    }

    protected void addCriterion(String condition, Object value, String property) {
        if (value == null) {
            throw new RuntimeException("Value for " + property + " cannot be null");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("value", value);
        criteriaWithSingleValue.add(map);
    }

    protected void addCriterion(String condition, List<? extends Object> values, String property) {
        if (values == null || values.size() == 0) {
            throw new RuntimeException("Value list for " + property + " cannot be null or empty");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", values);
        criteriaWithListValue.add(map);
    }

    protected void addCriterion(String condition, Object value1, Object value2, String property) {
        if (value1 == null || value2 == null) {
            throw new RuntimeException("Between values for " + property + " cannot be null");
        }
        List<Object> list = new ArrayList<Object>();
        list.add(value1);
        list.add(value2);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", list);
        criteriaWithBetweenValue.add(map);
    }


    
    public Criteria andIdIsNull() {
        addCriterion("ID is null");
        return this;
    }

    public Criteria andIdIsNotNull() {
        addCriterion("ID is not null");
        return this;
    }

    public Criteria andIdEqualTo(String value) {
        addCriterion("ID =", value, "id");
        return this;
    }

    public Criteria andIdNotEqualTo(String value) {
        addCriterion("ID <>", value, "id");
        return this;
    }

    public Criteria andIdGreaterThan(String value) {
        addCriterion("ID >", value, "id");
        return this;
    }

    public Criteria andIdGreaterThanOrEqualTo(String value) {
        addCriterion("ID >=", value, "id");
        return this;
    }

    public Criteria andIdLessThan(String value) {
        addCriterion("ID <", value, "id");
        return this;
    }

    public Criteria andIdLessThanOrEqualTo(String value) {
        addCriterion("ID <=", value, "id");
        return this;
    }

    public Criteria andIdLike(String value) {
        addCriterion("ID like", value, "id");
        return this;
    }

    public Criteria andIdNotLike(String value) {
        addCriterion("ID not like", value, "id");
        return this;
    }

    public Criteria andIdIn(List<String> values) {
        addCriterion("ID in", values, "id");
        return this;
    }

    public Criteria andIdNotIn(List<String> values) {
        addCriterion("ID not in", values, "id");
        return this;
    }

    public Criteria andIdBetween(String value1, String value2) {
        addCriterion("ID between", value1, value2, "id");
        return this;
    }

    public Criteria andIdNotBetween(String value1, String value2) {
        addCriterion("ID not between", value1, value2, "id");
        return this;
    }

    
    public Criteria andUserNameIsNull() {
        addCriterion("USER_NAME is null");
        return this;
    }

    public Criteria andUserNameIsNotNull() {
        addCriterion("USER_NAME is not null");
        return this;
    }

    public Criteria andUserNameEqualTo(String value) {
        addCriterion("USER_NAME =", value, "userName");
        return this;
    }

    public Criteria andUserNameNotEqualTo(String value) {
        addCriterion("USER_NAME <>", value, "userName");
        return this;
    }

    public Criteria andUserNameGreaterThan(String value) {
        addCriterion("USER_NAME >", value, "userName");
        return this;
    }

    public Criteria andUserNameGreaterThanOrEqualTo(String value) {
        addCriterion("USER_NAME >=", value, "userName");
        return this;
    }

    public Criteria andUserNameLessThan(String value) {
        addCriterion("USER_NAME <", value, "userName");
        return this;
    }

    public Criteria andUserNameLessThanOrEqualTo(String value) {
        addCriterion("USER_NAME <=", value, "userName");
        return this;
    }

    public Criteria andUserNameLike(String value) {
        addCriterion("USER_NAME like", value, "userName");
        return this;
    }

    public Criteria andUserNameNotLike(String value) {
        addCriterion("USER_NAME not like", value, "userName");
        return this;
    }

    public Criteria andUserNameIn(List<String> values) {
        addCriterion("USER_NAME in", values, "userName");
        return this;
    }

    public Criteria andUserNameNotIn(List<String> values) {
        addCriterion("USER_NAME not in", values, "userName");
        return this;
    }

    public Criteria andUserNameBetween(String value1, String value2) {
        addCriterion("USER_NAME between", value1, value2, "userName");
        return this;
    }

    public Criteria andUserNameNotBetween(String value1, String value2) {
        addCriterion("USER_NAME not between", value1, value2, "userName");
        return this;
    }

    
    public Criteria andPasswordIsNull() {
        addCriterion("PASSWORD is null");
        return this;
    }

    public Criteria andPasswordIsNotNull() {
        addCriterion("PASSWORD is not null");
        return this;
    }

    public Criteria andPasswordEqualTo(String value) {
        addCriterion("PASSWORD =", value, "password");
        return this;
    }

    public Criteria andPasswordNotEqualTo(String value) {
        addCriterion("PASSWORD <>", value, "password");
        return this;
    }

    public Criteria andPasswordGreaterThan(String value) {
        addCriterion("PASSWORD >", value, "password");
        return this;
    }

    public Criteria andPasswordGreaterThanOrEqualTo(String value) {
        addCriterion("PASSWORD >=", value, "password");
        return this;
    }

    public Criteria andPasswordLessThan(String value) {
        addCriterion("PASSWORD <", value, "password");
        return this;
    }

    public Criteria andPasswordLessThanOrEqualTo(String value) {
        addCriterion("PASSWORD <=", value, "password");
        return this;
    }

    public Criteria andPasswordLike(String value) {
        addCriterion("PASSWORD like", value, "password");
        return this;
    }

    public Criteria andPasswordNotLike(String value) {
        addCriterion("PASSWORD not like", value, "password");
        return this;
    }

    public Criteria andPasswordIn(List<String> values) {
        addCriterion("PASSWORD in", values, "password");
        return this;
    }

    public Criteria andPasswordNotIn(List<String> values) {
        addCriterion("PASSWORD not in", values, "password");
        return this;
    }

    public Criteria andPasswordBetween(String value1, String value2) {
        addCriterion("PASSWORD between", value1, value2, "password");
        return this;
    }

    public Criteria andPasswordNotBetween(String value1, String value2) {
        addCriterion("PASSWORD not between", value1, value2, "password");
        return this;
    }

    
    public Criteria andCreateDateIsNull() {
        addCriterion("CREATE_DATE is null");
        return this;
    }

    public Criteria andCreateDateIsNotNull() {
        addCriterion("CREATE_DATE is not null");
        return this;
    }

    public Criteria andCreateDateEqualTo(String value) {
        addCriterion("CREATE_DATE =", value, "createDate");
        return this;
    }

    public Criteria andCreateDateNotEqualTo(String value) {
        addCriterion("CREATE_DATE <>", value, "createDate");
        return this;
    }

    public Criteria andCreateDateGreaterThan(String value) {
        addCriterion("CREATE_DATE >", value, "createDate");
        return this;
    }

    public Criteria andCreateDateGreaterThanOrEqualTo(String value) {
        addCriterion("CREATE_DATE >=", value, "createDate");
        return this;
    }

    public Criteria andCreateDateLessThan(String value) {
        addCriterion("CREATE_DATE <", value, "createDate");
        return this;
    }

    public Criteria andCreateDateLessThanOrEqualTo(String value) {
        addCriterion("CREATE_DATE <=", value, "createDate");
        return this;
    }

    public Criteria andCreateDateLike(String value) {
        addCriterion("CREATE_DATE like", value, "createDate");
        return this;
    }

    public Criteria andCreateDateNotLike(String value) {
        addCriterion("CREATE_DATE not like", value, "createDate");
        return this;
    }

    public Criteria andCreateDateIn(List<String> values) {
        addCriterion("CREATE_DATE in", values, "createDate");
        return this;
    }

    public Criteria andCreateDateNotIn(List<String> values) {
        addCriterion("CREATE_DATE not in", values, "createDate");
        return this;
    }

    public Criteria andCreateDateBetween(String value1, String value2) {
        addCriterion("CREATE_DATE between", value1, value2, "createDate");
        return this;
    }

    public Criteria andCreateDateNotBetween(String value1, String value2) {
        addCriterion("CREATE_DATE not between", value1, value2, "createDate");
        return this;
    }

    
    public Criteria andFlagIsNull() {
        addCriterion("FLAG is null");
        return this;
    }

    public Criteria andFlagIsNotNull() {
        addCriterion("FLAG is not null");
        return this;
    }

    public Criteria andFlagEqualTo(Integer value) {
        addCriterion("FLAG =", value, "flag");
        return this;
    }

    public Criteria andFlagNotEqualTo(Integer value) {
        addCriterion("FLAG <>", value, "flag");
        return this;
    }

    public Criteria andFlagGreaterThan(Integer value) {
        addCriterion("FLAG >", value, "flag");
        return this;
    }

    public Criteria andFlagGreaterThanOrEqualTo(Integer value) {
        addCriterion("FLAG >=", value, "flag");
        return this;
    }

    public Criteria andFlagLessThan(Integer value) {
        addCriterion("FLAG <", value, "flag");
        return this;
    }

    public Criteria andFlagLessThanOrEqualTo(Integer value) {
        addCriterion("FLAG <=", value, "flag");
        return this;
    }

    public Criteria andFlagLike(Integer value) {
        addCriterion("FLAG like", value, "flag");
        return this;
    }

    public Criteria andFlagNotLike(Integer value) {
        addCriterion("FLAG not like", value, "flag");
        return this;
    }

    public Criteria andFlagIn(List<Integer> values) {
        addCriterion("FLAG in", values, "flag");
        return this;
    }

    public Criteria andFlagNotIn(List<Integer> values) {
        addCriterion("FLAG not in", values, "flag");
        return this;
    }

    public Criteria andFlagBetween(Integer value1, Integer value2) {
        addCriterion("FLAG between", value1, value2, "flag");
        return this;
    }

    public Criteria andFlagNotBetween(Integer value1, String value2) {
        addCriterion("FLAG not between", value1, value2, "flag");
        return this;
    }

        }
}