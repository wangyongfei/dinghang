package com.dh.sys.common;

import com.ibatis.sqlmap.client.SqlMapClient;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

public class BaseDao extends SqlMapClientDaoSupport{


	@Resource(name="sqlMapClient") //通过bean注入
	private SqlMapClient sqlMapClient;
	
	@PostConstruct //完成sqlMapClient初始化工作
	public void initSqlMapClient(){
		super.setSqlMapClient(sqlMapClient);
	}
}
