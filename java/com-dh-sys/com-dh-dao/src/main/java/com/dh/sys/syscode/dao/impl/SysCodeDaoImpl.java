package com.dh.sys.syscode.dao.impl;

import java.util.List;
import com.dh.sys.BaseDao;
import org.springframework.stereotype.Repository;

import com.dh.sys.syscode.dao.SysCodeDao;
import com.dh.sys.syscode.model.SysCode;
import com.dh.sys.syscode.model.SysCodeCriteria;

@Repository("SysCodeDao")
public class SysCodeDaoImpl extends BaseDao implements SysCodeDao {


        public SysCodeDaoImpl() {
            super();
        }


        public int countByExample(SysCodeCriteria example) {
            Integer count = (Integer)  getSqlMapClientTemplate().queryForObject("sys_code.ibatorgenerated_countByExample", example);
            return count;
        }


        public int deleteByExample(SysCodeCriteria example) {
            int rows = getSqlMapClientTemplate().delete("sys_code.ibatorgenerated_deleteByExample", example);
            return rows;
        }


        public int deleteByPrimaryKey(String id) {
        SysCode key = new SysCode();
            key.setId(id);
            int rows = getSqlMapClientTemplate().delete("sys_code.ibatorgenerated_deleteByPrimaryKey", key);
            return rows;
        }


        public void insert(SysCode record) {
            getSqlMapClientTemplate().insert("sys_code.ibatorgenerated_insert", record);
        }


        public void insertSelective(SysCode record) {
            getSqlMapClientTemplate().insert("sys_code.ibatorgenerated_insertSelective", record);
        }


        @SuppressWarnings("unchecked")
        public List<SysCode> selectByExample(SysCodeCriteria example,Integer page, Integer rows) {
            List<SysCode> list = getSqlMapClientTemplate().queryForList("sys_code.ibatorgenerated_selectByExample", example,page, rows);
            return list;
        }

        @SuppressWarnings("unchecked")
        public List<SysCode> selectByExample(SysCodeCriteria example) {
            List<SysCode> list = getSqlMapClientTemplate().queryForList("sys_code.ibatorgenerated_selectByExample", example);
            return list;
        }


        public SysCode selectByPrimaryKey(String id) {
            SysCode key = new SysCode();
            key.setId(id);
            SysCode record = (SysCode) getSqlMapClientTemplate().queryForObject("sys_code.ibatorgenerated_selectByPrimaryKey", key);
            return record;
        }


        public int updateByExampleSelective(SysCode record, SysCodeCriteria example) {
            UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
            int rows = getSqlMapClientTemplate().update("sys_code.ibatorgenerated_updateByExampleSelective", parms);
            return rows;
        }


        public int updateByExample(SysCode record, SysCodeCriteria example) {
            UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
            int rows = getSqlMapClientTemplate().update("sys_code.ibatorgenerated_updateByExample", parms);
            return rows;
        }


        public int updateByPrimaryKeySelective(SysCode record) {
            int rows = getSqlMapClientTemplate().update("sys_code.ibatorgenerated_updateByPrimaryKeySelective", record);
            return rows;
        }


        public int updateByPrimaryKey(SysCode record) {
            int rows = getSqlMapClientTemplate().update("sys_code.ibatorgenerated_updateByPrimaryKey", record);
            return rows;
        }


        private static class UpdateByExampleParms extends SysCodeCriteria {
        private Object record;

        public UpdateByExampleParms(Object record, SysCodeCriteria example) {
            super(example);
            this.record = record;
        }

        public Object getRecord() {
            return record;
        }
  }
}