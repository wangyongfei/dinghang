package com.dh.sys.news.service.impl;


import com.dh.sys.news.dao.DhNewsDao;
import com.dh.sys.news.model.DhNews;
import com.dh.sys.news.model.DhNewsCriteria;
import com.dh.sys.news.service.DhNewsService;
import com.dh.sys.resin.RedisGeneratorDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Repository;

import java.util.List;


@Slf4j
@Repository(value="DhNewsService")
//@Service("DhNewsService")
public class DhNewsServiceImpl extends RedisGeneratorDao<String,DhNews> implements DhNewsService {

    @Autowired
    private DhNewsDao dao;


    public List<DhNews> select(DhNews record){

        DhNewsCriteria example = new DhNewsCriteria();

        List<DhNews> selectByExample = dao.selectByExampleWithBLOBs(example);

        return selectByExample;
    }

    public boolean delete(DhNews record){
        try {
            int i = dao.deleteByPrimaryKey(record.getId());
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(DhNews record){
//        try {
//            int i = dao.updateByPrimaryKey(record);
//            return i>0?true:false;
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        return false;
    }

    public List<DhNews> findAllListPage(DhNews record, Integer page, Integer rows) {

        DhNewsCriteria example = new DhNewsCriteria();

        List<DhNews> selectByExample = dao.selectByExampleWithBLOBs(example);

        return selectByExample;
    }

    public int count(DhNews record) {
        DhNewsCriteria example = new DhNewsCriteria();
        // todo
        return dao.countByExample(example);
    }

    public DhNews selectById(String id) {
        return dao.selectByPrimaryKey(id);
    }

    public void save(DhNews record) {
        dao.insert(record);
    }

    /**---------------------------redis测试 start-----------------------------**/

    public void saveRedis(final DhNews record) {

        Boolean result = redisTemplate.execute(new org.springframework.data.redis.core.RedisCallback<Boolean>() {
            public Boolean doInRedis(RedisConnection connection)
                    throws DataAccessException {
                RedisSerializer<String> serializer = getRedisSerializer();
                byte[] key = serializer.serialize(record.getId());
                byte[] name = serializer.serialize(record.getTitle());
                connection.setNX(key, name);
                return true;
            }

        });

        log.info("添加redis数据成功：{}", record.getId() + result);
    }

    public DhNews getByKey(final String keyId) {

        DhNews result = redisTemplate.execute(new RedisCallback<DhNews>() {
            public DhNews doInRedis(RedisConnection connection)
                    throws DataAccessException {
                RedisSerializer<String> serializer = getRedisSerializer();
                byte[] key = serializer.serialize(keyId);
                byte[] value = connection.get(key);
                if (value == null) {
                    return null;
                }
                String nickname = serializer.deserialize(value);
                return new DhNews(keyId, nickname);
            }
        });
        return result;
    }

    /**---------------------------redis测试 end-----------------------------**/
}