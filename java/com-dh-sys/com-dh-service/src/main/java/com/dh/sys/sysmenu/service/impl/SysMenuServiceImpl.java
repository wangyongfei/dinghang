package com.dh.sys.sysmenu.service.impl;

import com.dh.sys.sysmenu.service.SysMenuService;
import com.dh.sys.sysmenu.dao.SysMenuDao;
import com.dh.sys.sysmenu.model.SysMenu;
import com.dh.sys.sysmenu.model.SysMenuCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;



@Service("sysMenuService")
public class SysMenuServiceImpl implements SysMenuService {

//	public boolean insert(SysMenu record) {
//		// TODO Auto-generated method stub
//		return false;
//	}

	@Autowired
	private SysMenuDao dao;
//	
	public List<SysMenu> selectSysMenu(SysMenu record){
		
		SysMenuCriteria example = new SysMenuCriteria();
		if(record != null){
			SysMenuCriteria.Criteria criteria = example.createCriteria();
			if(record.getFlag() != null && !"".equals(record.getFlag())){
				criteria.andFlagEqualTo(record.getFlag());
			}
			if(record.getParentNode() != null){
				List<String>list = new ArrayList<String>();
				list.add(record.getParentNode());
				list.add("root");
				criteria.andParentNodeIn(list);

			}

		}else{
			SysMenuCriteria.Criteria criteria = example.createCriteria();
			criteria.andParentNodeNotEqualTo("root");
		}
		List<SysMenu> selectByExample = dao.selectByExample(example);
		return selectByExample;
	}
	
	public boolean insert(SysMenu record){
//		try {
//			record.setCreateDate(new Date());
//			sysMenuDao.insert(record);
//			return true;
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return false;
	}
	
	public boolean delete(SysMenu record){
		try {
			int i = dao.deleteByPrimaryKey(record.getId());
			return i>0?true:false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean update(SysMenu record){
		try {
			int i = dao.updateByPrimaryKey(record);
			return i>0?true:false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public List<SysMenu> findAllListPage(SysMenu record, Integer page, Integer rows) {
		SysMenuCriteria example = new SysMenuCriteria();
		if(record != null){
			if(record.getFlag() != null && !"".equals(record.getFlag())){
				example.createCriteria().andFlagEqualTo(record.getFlag());
			}
		}
		example.createCriteria().andParentNodeNotEqualTo("root");
		List<SysMenu> selectByExample = dao.selectByExample(example,page,rows);
		return selectByExample;

	}

	public int count(SysMenu record) {
		SysMenuCriteria example = new SysMenuCriteria();
		example.createCriteria().andParentNodeNotEqualTo("root");
		return dao.countByExample(example);
	}

	public SysMenu selectById(String id) {
		return dao.selectByPrimaryKey(id);
	}

	public void save(SysMenu sm) {
		dao.insert(sm);
	}

	@Override
	public List<SysMenu> selectSysMenuNoContain(SysMenu record) {
		SysMenuCriteria example = new SysMenuCriteria();
		example.createCriteria().andParentNodeNotEqualTo(record.getParentNode()).andParentNodeNotEqualTo("root");
		return dao.selectByExample(example);
	}
}
