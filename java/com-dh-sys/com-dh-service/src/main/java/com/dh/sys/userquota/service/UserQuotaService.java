package com.dh.sys.userquota.service;

import com.dh.sys.userquota.model.UserQuota;
import java.util.List;

public interface UserQuotaService {

    public List<UserQuota> select(UserQuota record);

    public boolean delete(UserQuota record);

    public boolean update(UserQuota record);

    List<UserQuota> findAllListPage(UserQuota record, Integer page, Integer rows);

    int count(UserQuota record);

    UserQuota selectById(Long id);

    void save(UserQuota sm);

    void insert();

}