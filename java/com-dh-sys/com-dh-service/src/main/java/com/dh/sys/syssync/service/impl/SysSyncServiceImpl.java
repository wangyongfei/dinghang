package com.dh.sys.syssync.service.impl;


import com.dh.sys.syssync.dao.SysSyncDao;
import com.dh.sys.syssync.dao.impl.SysSyncExtDao;
import com.dh.sys.syssync.model.SysSync;
import com.dh.sys.syssync.model.SysSyncCriteria;
import com.dh.sys.syssync.service.SysSyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("SysSyncService")
public class SysSyncServiceImpl implements SysSyncService {

    @Autowired
    private SysSyncDao dao;

    @Autowired
    private SysSyncExtDao sysSyncExtDao;

    public List<SysSync> select(SysSync record){

        SysSyncCriteria example = new SysSyncCriteria();
        example.setOrderByClause("id");
        List<SysSync> selectByExample = dao.selectByExample(example);

        return selectByExample;
    }

    public boolean delete(SysSync record){
        try {
            int i = dao.deleteByPrimaryKey(record.getId());
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(SysSync record){
        try {
            int i = dao.updateByPrimaryKey(record);
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public List<SysSync> findAllListPage(SysSync record, Integer page, Integer rows) {

        SysSyncCriteria example = new SysSyncCriteria();

        List<SysSync> selectByExample = dao.selectByExample(example,page,rows);

        return selectByExample;

    }

    public int count(SysSync record) {
        SysSyncCriteria example = new SysSyncCriteria();
        // todo
        return dao.countByExample(example);
    }

    public SysSync selectById(String id) {
        return dao.selectByPrimaryKey(id);
    }

    public void save(SysSync record) {
        dao.insert(record);
    }

    @Override
    public List<SysSync> findSysSyncData(List<Integer> unStatus, int status) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("unStatus",unStatus);
        map.put("status",status);
        return sysSyncExtDao.findSysSyncData(map);
    }
}