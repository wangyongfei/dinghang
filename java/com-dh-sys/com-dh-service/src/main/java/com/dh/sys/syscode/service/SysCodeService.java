package com.dh.sys.syscode.service;

import com.dh.sys.syscode.model.SysCode;
import java.util.List;

public interface SysCodeService {

    public List<SysCode> select(SysCode record);

    public boolean delete(SysCode record);

    public boolean update(SysCode record);

    List<SysCode> findAllListPage(SysCode record, Integer page, Integer rows);

    int count(SysCode record);

    SysCode selectById(String id);

    void save(SysCode sm);

}