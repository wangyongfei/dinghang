package com.dh.sys.sysmenu.service;




import com.dh.sys.sysmenu.model.SysMenu;

import java.util.List;

public interface SysMenuService {

	public List<SysMenu> selectSysMenu(SysMenu record);
	public boolean insert(SysMenu record);
	public boolean delete(SysMenu record);
	public boolean update(SysMenu record);

	List<SysMenu> findAllListPage(SysMenu record, Integer page, Integer rows);

	int count(SysMenu record);

	SysMenu selectById(String id);

	void save(SysMenu sm);

	List<SysMenu> selectSysMenuNoContain(SysMenu record);
}
