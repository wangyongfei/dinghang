package com.dh.sys.userquota.service.impl;


import com.dh.sys.userquota.dao.UserQuotaDao;
import com.dh.sys.userquota.model.UserQuota;
import com.dh.sys.userquota.model.UserQuotaCriteria;
import com.dh.sys.userquota.service.UserQuotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("UserQuotaService")
public class UserQuotaServiceImpl implements UserQuotaService {

    @Autowired
    private UserQuotaDao dao;
    //
    public List<UserQuota> select(UserQuota record){

        UserQuotaCriteria example = new UserQuotaCriteria();

        List<UserQuota> selectByExample = dao.selectByExample(example);

        return selectByExample;
    }

    public boolean delete(UserQuota record){
        try {
            int i = dao.deleteByPrimaryKey(record.getId());
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }



    @Transactional(value = "transactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void insert(){
        for(int i = 30; i>25; i--) {
            UserQuota record = new UserQuota();
            record.setCreditQuota(10000L);
            record.setUsedQuota(0L);
            record.setUid("test"+i);
            dao.insert(record);
        }
    }

    public boolean update(UserQuota record){
        try {
            int i = dao.updateByPrimaryKey(record);
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public List<UserQuota> findAllListPage(UserQuota record, Integer page, Integer rows) {

        UserQuotaCriteria example = new UserQuotaCriteria();

        List<UserQuota> selectByExample = dao.selectByExample(example,page,rows);

        return selectByExample;

    }

    public int count(UserQuota record) {
        UserQuotaCriteria example = new UserQuotaCriteria();
        // todo
        return dao.countByExample(example);
    }

    public UserQuota selectById(Long id) {
        return dao.selectByPrimaryKey(id);
    }

    public void save(UserQuota record) {
        dao.insert(record);
    }
}