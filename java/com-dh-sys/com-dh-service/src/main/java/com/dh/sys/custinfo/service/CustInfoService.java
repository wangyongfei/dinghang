package com.dh.sys.custinfo.service;

import com.dh.sys.custinfo.model.CustInfo;
import java.util.List;

public interface CustInfoService {

    public List<CustInfo> select(CustInfo record);

    public boolean delete(CustInfo record);

    public boolean update(CustInfo record);

    List<CustInfo> findAllListPage(CustInfo record, Integer page, Integer rows);

    int count(CustInfo record);

    CustInfo selectById(String id);

    void save(CustInfo sm);

}