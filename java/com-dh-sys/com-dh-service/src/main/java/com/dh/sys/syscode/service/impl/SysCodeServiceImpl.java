package com.dh.sys.syscode.service.impl;


import com.dh.sys.syscode.dao.SysCodeDao;
import com.dh.sys.syscode.model.SysCode;
import com.dh.sys.syscode.model.SysCodeCriteria;
import com.dh.sys.syscode.service.SysCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("sysCodeService")
public class SysCodeServiceImpl implements SysCodeService {

    @Autowired
    private SysCodeDao dao;
    //
    public List<SysCode> select(SysCode record){

        SysCodeCriteria example = new SysCodeCriteria();
        example.createCriteria().andCodeTypeEqualTo(record.getCodeType());
        List<SysCode> selectByExample = dao.selectByExample(example);

        return selectByExample;
    }

    public boolean delete(SysCode record){
        try {
            int i = dao.deleteByPrimaryKey(record.getId());
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(SysCode record){
        try {
            int i = dao.updateByPrimaryKey(record);
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public List<SysCode> findAllListPage(SysCode record, Integer page, Integer rows) {

        SysCodeCriteria example = new SysCodeCriteria();

        List<SysCode> selectByExample = dao.selectByExample(example,page,rows);

        return selectByExample;

    }

    public int count(SysCode record) {
        SysCodeCriteria example = new SysCodeCriteria();
        // todo
        return dao.countByExample(example);
    }

    public SysCode selectById(String id) {
        return dao.selectByPrimaryKey(id);
    }

    public void save(SysCode record) {
        dao.insert(record);
    }
}