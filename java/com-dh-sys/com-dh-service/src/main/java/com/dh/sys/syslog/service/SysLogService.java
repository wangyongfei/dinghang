package com.dh.sys.syslog.service;

import com.dh.sys.syslog.model.SysLog;
import java.util.List;

public interface SysLogService {

    public List<SysLog> select(SysLog record);

    public boolean delete(SysLog record);

    public boolean update(SysLog record);

    List<SysLog> findAllListPage(SysLog record, Integer page, Integer rows);

    int count(SysLog record);

    SysLog selectById(String id);

    void save(SysLog sm);

    List<SysLog> select(String startDate, String endDate);
}