package com.dh.sys.sync;

import com.dh.sys.mongo.MongoService;
import com.dh.sys.syslog.model.SysLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * mongo数据同步
 * Created by Administrator on 2016/12/31.
 */

@Service("mongoSyncService")
public class MongoSyncService {

    @Autowired
    private MongoService mongoService;

    public void insert(List<SysLog> list,String collectionName){
        mongoService.batchInsertByCollectionName(list,collectionName);
    }


    /**
     * 添加collection，统一管理
     * @param clooectionName
     */
    public void insertCollection(String clooectionName){
        List<MongosyncEntity>list = (List<MongosyncEntity>) mongoService.findAllList(clooectionName);
        if(null == list || list.isEmpty()){
            MongosyncEntity mongosyncEntity = new MongosyncEntity();
            mongosyncEntity.setClooectionName(clooectionName);
            mongosyncEntity.setCredateDate(new Date());
            mongoService.save(mongosyncEntity);
        }
    }


}
