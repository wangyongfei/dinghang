package com.dh.sys.sysuser.service.impl;


import com.dh.sys.sysuser.dao.SysUserDao;
import com.dh.sys.sysuser.model.SysUser;
import com.dh.sys.sysuser.model.SysUserCriteria;
import com.dh.sys.sysuser.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("SysUserService")
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserDao dao;
    //
    public List<SysUser> select(SysUser record){

        SysUserCriteria example = new SysUserCriteria();
        example.createCriteria().andUserNameEqualTo(record.getUserName()).andPasswordEqualTo(record.getPassword());
        List<SysUser> selectByExample = dao.selectByExample(example);

        return selectByExample;
    }

    public boolean delete(SysUser record){
        try {
            int i = dao.deleteByPrimaryKey(record.getId());
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(SysUser record){
        try {
            int i = dao.updateByPrimaryKey(record);
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public List<SysUser> findAllListPage(SysUser record, Integer page, Integer rows) {

        SysUserCriteria example = new SysUserCriteria();

        List<SysUser> selectByExample = dao.selectByExample(example,page,rows);

        return selectByExample;

    }

    public int count(SysUser record) {
        SysUserCriteria example = new SysUserCriteria();
        // todo
        return dao.countByExample(example);
    }

    public SysUser selectById(String id) {
        return dao.selectByPrimaryKey(id);
    }

    public void save(SysUser record) {
        dao.insert(record);
    }
}