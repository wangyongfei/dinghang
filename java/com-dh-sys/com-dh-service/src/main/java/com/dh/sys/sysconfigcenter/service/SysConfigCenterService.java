package com.dh.sys.sysconfigcenter.service;

import com.dh.sys.sysconfigcenter.model.SysConfigCenter;
import java.util.List;

public interface SysConfigCenterService {

    public List<SysConfigCenter> select(SysConfigCenter record);

    public boolean delete(SysConfigCenter record);

    public boolean update(SysConfigCenter record);

    List<SysConfigCenter> findAllListPage(SysConfigCenter record, Integer page, Integer rows);

    int count(SysConfigCenter record);

    SysConfigCenter selectById(String id);

    void save(SysConfigCenter sm);

}