package com.dh.sys.sysuser.service;

import com.dh.sys.sysuser.model.SysUser;
import java.util.List;

public interface SysUserService {

    public List<SysUser> select(SysUser record);

    public boolean delete(SysUser record);

    public boolean update(SysUser record);

    List<SysUser> findAllListPage(SysUser record, Integer page, Integer rows);

    int count(SysUser record);

    SysUser selectById(String id);

    void save(SysUser sm);

}