package com.dh.sys.custinfo.service.impl;


import com.dh.sys.custinfo.dao.CustInfoDao;
import com.dh.sys.custinfo.model.CustInfo;
import com.dh.sys.custinfo.model.CustInfoCriteria;
import com.dh.sys.custinfo.service.CustInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("CustInfoService")
public class CustInfoServiceImpl implements CustInfoService {

    @Autowired
    private CustInfoDao dao;
    //
    public List<CustInfo> select(CustInfo record){

        CustInfoCriteria example = new CustInfoCriteria();
        example.createCriteria().andPasswordEqualTo(record.getPassword()).andUserNameEqualTo(record.getUserName());
        List<CustInfo> selectByExample = dao.selectByExample(example);

        return selectByExample;
    }

    public boolean delete(CustInfo record){
        try {
            int i = dao.deleteByPrimaryKey(record.getId());
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(CustInfo record){
        try {
            int i = dao.updateByPrimaryKey(record);
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public List<CustInfo> findAllListPage(CustInfo record, Integer page, Integer rows) {

        CustInfoCriteria example = new CustInfoCriteria();

        List<CustInfo> selectByExample = dao.selectByExample(example,page,rows);

        return selectByExample;

    }

    public int count(CustInfo record) {
        CustInfoCriteria example = new CustInfoCriteria();
        // todo
        return dao.countByExample(example);
    }

    public CustInfo selectById(String id) {
        return dao.selectByPrimaryKey(id);
    }

    public void save(CustInfo record) {
        dao.insert(record);
    }
}