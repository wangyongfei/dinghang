package com.dhsys.gen.table;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 数据库表
 * Created by wyf on 2016/5/15 0015.
 */
public class Table implements Serializable {

    /**
     * 表名
     */
    private String tableName;

    /**
     * 类名
     */
    private String className;

    /**
     * 包名
     */
    private  String pakeName;

    /**
     * 数据库 schema
     */
    private String schema;


    /**
     * 数据库
     */
    private String dataBase;

    /**
     * 主键
     */
    private Map<String,String> primaryKey;

    /**
     * 主键名称
     */
    private String primarykeyName;

    /**
     * 主键类型
     */
    private String primarykeyType;


    /**
     * 类域名
     */
    private List<Fields>listField = new ArrayList<Fields>();


    public String getPrimarykeyName() {
        return primarykeyName;
    }

    public void setPrimarykeyName(String primarykeyName) {
        this.primarykeyName = primarykeyName;
    }

    public String getPrimarykeyType() {
        return primarykeyType;
    }

    public void setPrimarykeyType(String primarykeyType) {
        this.primarykeyType = primarykeyType;
    }

    public Map<String, String> getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(Map<String, String> primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getDataBase() {
        return dataBase;
    }

    public void setDataBase(String dataBase) {
        this.dataBase = dataBase;
    }


    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public List<Fields> getListField() {
        return listField;
    }

    public void setListField(List<Fields> listField) {
        this.listField = listField;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getPakeName() {
        return pakeName;
    }

    public void setPakeName(String pakeName) {
        this.pakeName = pakeName;
    }

    @Override
    public String toString() {
        return "Table{" +
                "tableName='" + tableName + '\'' +
                ", className='" + className + '\'' +
                ", pakeName='" + pakeName + '\'' +
                ", schema='" + schema + '\'' +
                ", dataBase='" + dataBase + '\'' +
                ", primaryKey='" + primaryKey + '\'' +
                ", listField=" + listField +
                '}';
    }
}