package com.dhsys.gen.table;


import java.io.Serializable;

/**
 * 表字段实体类
 * Created by wyf on 2016/5/24 0024.
 */
public class Fields implements Serializable {

    /**
     * 字段名
     */
    private  String fieldName;


    /**
     * 字段长度
     */
    private Integer fieldLength;


    /**
     * 字段类型
     */
    private  String fieldType;

    /**
     * 字段描述
     */
    private  String fieldMark;


    /**
     * 实体类字段名
     */
    private  String modelName;


    /**
     * 实体类字段类型
     */
    private  String modelType;

    /**
     * get,set 方法名称
     */
    private  String methodName;


    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getFieldMark() {
        return fieldMark;
    }

    public void setFieldMark(String fieldMark) {
        this.fieldMark = fieldMark;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Integer getFieldLength() {
        return fieldLength;
    }

    public void setFieldLength(Integer fieldLength) {
        this.fieldLength = fieldLength;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    @Override
    public String toString() {
        return "Fields{" +
                "fieldName='" + fieldName + '\'' +
                ", fieldLength=" + fieldLength +
                ", fieldType='" + fieldType + '\'' +
                ", fieldMark='" + fieldMark + '\'' +
                ", modelName='" + modelName + '\'' +
                ", modelType='" + modelType + '\'' +
                ", methodName='" + methodName + '\'' +
                '}';
    }
}
