package com.dhsys.gen.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 数据库连接
 * Created by Administrator on 2016/5/15 0015.
 */
public class DbConnection {

    private static final String dbEnCode = "?useUnicode=true&characterEncoding=UTF-8";
    /**
     * 连接mysql数据库
     *
     * @param dbDriver   数据库驱动
     * @param dbUrl      数据库url
     * @param dbUserName 数据库用户名
     * @param dbUserPwd  数据库密码
     * @return
     */
    public static Connection getMySqlConnection(String dbDriver, String dbUrl, String dbUserName, String dbUserPwd) {
        try {
            Class.forName(dbDriver).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            return DriverManager.getConnection(dbUrl+dbEnCode, dbUserName, dbUserPwd);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
