package com.dhsys.gen.connection;

import com.dhsys.gen.table.Fields;
import com.dhsys.gen.table.Table;
import com.dhsys.gen.util.StringUtil;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 查找mysql数据库表
 * Created by wyf on 2016/5/29 0029.
 */
public class FindMySqlTables{

    /**
     * 表名
     */
    private String tableName;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public FindMySqlTables() {
    }

    public FindMySqlTables(String tableName) {
        this.tableName = tableName;
    }

    /**
     * 获取全部表
     * @param dbDriver
     * @param dbUrl
     * @param dbUserName
     * @param dbUserPwd
     * @return
     */
    private  List<Table> findTables(String dbDriver,
                                  String dbUrl,
                                  String dbUserName,
                                  String dbUserPwd) {

        Connection conn = DbConnection.getMySqlConnection(dbDriver, dbUrl, dbUserName, dbUserPwd);
        String dbName = "";
        List<Table> list = new ArrayList<Table>();
        if (conn != null) {
            try {
                DatabaseMetaData databaseMetaData = conn.getMetaData();
                ResultSet resultSet = databaseMetaData.getTables(dbName, null, this.getTableName(), new String[]{"TABLE"});
                while (resultSet.next()) {
                    Table table = new Table();
                    String name = resultSet.getString("TABLE_NAME");
                    String[] strAry = name.split("_");
                    StringBuffer sb = new StringBuffer();
                    for (String str : strAry) {
                        sb.append(str.substring(0, 1).toUpperCase()).append(str.substring(1, str.length()).toLowerCase());
                    }
                    table.setClassName(sb.toString());
                    table.setTableName(name);
                    list.add(table);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return list;
    }

    /**
     * 获取表对应字段
     * @param dbDriver
     * @param dbUrl
     * @param dbUserName
     * @param dbUserPwd
     * @return
     */
    public  List<Table> getTablesFields(
                                       String dbDriver,
                                       String dbUrl,
                                       String schema,
                                       String dbUserName,
                                       String dbUserPwd) {

        List<Table>list = findTables( dbDriver, dbUrl, dbUserName, dbUserPwd);
        if(list != null) {
            Connection conn = DbConnection.getMySqlConnection(dbDriver, dbUrl, dbUserName, dbUserPwd);
            if (conn != null) {
                try {
                    List<Table> tableList = new ArrayList<Table>();

                    for (Table table : list) {
                        DatabaseMetaData databaseMetaData = conn.getMetaData();
                        ResultSet resultSet = databaseMetaData.getColumns(null, "%", table.getTableName(), "%");
                        ResultSet resultSet2 = databaseMetaData.getPrimaryKeys(null,schema,table.getTableName());//获取主键

                        Map<String,String> priMap = new HashMap<String, String>();
                        List<String>priKey = new ArrayList<String>();

                        while(resultSet2.next()){
//                            System.out.print("目录名："+resultSet2.getString(1));
//                            System.out.print(" 模式名："+resultSet2.getString(2));
//                            System.out.print(" 表名："+resultSet2.getString(3));
//                            System.out.print(" 列名顺序号："+resultSet2.getString(4));
//                            System.out.print(" 列名顺序号："+resultSet2.getString(5));
//                            System.out.println(" 主键名："+resultSet2.getString(6));
                            priKey.add(resultSet2.getString(4));
                        }
//                        table.setPrimaryKey(listPri);

                        table.setDataBase(databaseMetaData.getDatabaseProductName()) ;//数据库名
                        List<Fields> listField = new ArrayList<Fields>();
                        while (resultSet.next()) {
//                            if (resultSet.getString("COLUMN_SIZE") != null) {

                                Fields fields = new Fields();

                                fields.setFieldLength(resultSet.getString("COLUMN_SIZE")==null?0:Integer.parseInt(resultSet.getString("COLUMN_SIZE")));
                                fields.setFieldType(StringUtil.getDBDadaType(resultSet.getString("TYPE_NAME")));
                                fields.setModelType(StringUtil.getModelDadaType(resultSet.getString("TYPE_NAME")));
                                fields.setFieldName(resultSet.getString("COLUMN_NAME"));
                                fields.setMethodName(StringUtil.getMethodName(resultSet.getString("COLUMN_NAME")));
                                fields.setModelName(StringUtil.getModelName(resultSet.getString("COLUMN_NAME")));
                                fields.setFieldMark(resultSet.getString("REMARKS"));
                                listField.add(fields);
//                            }
                        }
                        table.setListField(listField);
                        if (resultSet != null) {
                            resultSet.close();
                        }

                        for(String str: priKey) {
                            for(Fields field: table.getListField()){
                                if(str.equals(field.getFieldName())){
                                    table.setPrimarykeyName(str);
                                    table.setPrimarykeyType(field.getModelType());
                                    priMap.put(str, field.getFieldType());
                                    break;
                                }
                            }
                        }
                        table.setPrimaryKey(priMap);
                        tableList.add(table);
                    }
                    return tableList;
                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    if (conn != null) {
                        try {
                            conn.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return null;
    }




}
