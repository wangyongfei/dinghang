package com.dhsys.gen.util;

/**
 * Created by wyf on 2016/5/29 0029.
 */
public class StringUtil {


    /**
     * 获取实体类方法名
     * @param fieldName
     * @return
     */
    public static String getMethodName(String fieldName){
        StringBuffer stringBuffer = new StringBuffer(1024);
        String[] strAry = fieldName.split("_");
        for (int i = 0; i < strAry.length; i++){
            stringBuffer.append(firstUpper(strAry[i]));
        }
        return stringBuffer.toString();
    }

    /**
     * 获取实体类属性名
     * @param fieldName
     * @return
     */
    public static String getModelName(String fieldName){
        StringBuffer stringBuffer = new StringBuffer(1024);
        String[] strAry = fieldName.split("_");
        for (int i = 0; i < strAry.length; i++){
            if(i>0) {
                stringBuffer.append(firstUpper(strAry[i]));
            }else{
                stringBuffer.append(strAry[i].toLowerCase());
            }
        }
        return stringBuffer.toString();
    }

    public static String firstUpper(String str){
        StringBuffer stringBuffer = new StringBuffer(1024);
        stringBuffer.append(str.substring(0,1).toUpperCase());
        stringBuffer.append(str.substring(1, str.length()).toLowerCase());
        return stringBuffer.toString();

    }

    public static String getModelDadaType(String dbType){
        if("INT".equals(dbType)){
              return "Integer";
        }
        if("VARCHAR".equals(dbType)){
            return "String";
        }
        if("DATETIME".equals(dbType)){
            return "String";
        }
        if("BLOB".equals(dbType)){
            return "byte[]";
        }
        if("BIGINT".equals(dbType)){
            return "Long";
        }
        if("DOUBLE".equals(dbType)){
            return "Double";
        }

      return dbType;
    }

    public static String getDBDadaType(String dbType){
        if("INT".equals(dbType)){
            return "INTEGER";
        }
        if("DATETIME".equals(dbType)){
            return "TIMESTAMP";
        }
//        if("BLOB".equals(dbType)){
//            return "LONGVARBINARY";
//        }

        return dbType;
    }

//    public static void main(String[] args) {
//        System.out.println(getMethodName("FLAG_NAME"));
//        System.out.println(getModelName("FLAG_NAME"));
//    }
}
