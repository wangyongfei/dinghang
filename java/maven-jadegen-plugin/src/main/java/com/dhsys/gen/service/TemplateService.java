package com.dhsys.gen.service;

import com.dhsys.gen.connection.FindMySqlTables;
import com.dhsys.gen.table.Table;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.velocity.tools.generic.DateTool;

import java.io.File;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

/**
 * 生成模板工具-通用
 * Created by wyf on 2016/5/26 0026.
 */
public class TemplateService {


    /**
     * 生成模板velocity
     *
     * @param list
     */
    public static void veloCityTemplet(
                                       Integer tmpFileType,
                                       List<Table> list,
                                       String type,
                                       String pakeName,
                                       String srcFolder,
                                       String baseDir,
                                       String resFolder) throws Exception {


        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.setProperty("input.encoding", "UTF-8");
        ve.setProperty("output.encoding", "UTF-8");
        ve.init();
        String prefixPath = new StringBuilder().append(srcFolder).append(File.separator).toString();
        for (Table table : list) {

            VelocityContext ctx = new VelocityContext();
            table.setPakeName(pakeName);
            System.out.println("filed size :"+table.getListField().size()+","+table.getListField().get(0).getFieldMark());
            ctx.put("table", table);
            //ctx.put("date",  new DateTool());

            if(tmpFileType ==1) { // 生成dao
                // model
                String modelPath = getRealPath(prefixPath, pakeName + ".model");
                String modelFileName = new StringBuilder().append(modelPath).append(table.getClassName()).append(".java").toString();
                doMerge(modelFileName, ve, ctx, "template/" + type + "/model.vm");


                // modelceriteria
                String modelCeriteriaPath = getRealPath(prefixPath, pakeName + ".model");
                String modelCeriteriaFileName = new StringBuilder().append(modelCeriteriaPath).append(table.getClassName() + "Criteria").append(".java").toString();
                doMerge(modelCeriteriaFileName, ve, ctx, "template/" + type + "/modelcriteria.vm");


                // dao
                String daoPath = getRealPath(prefixPath, pakeName + ".dao");
                String daoFileName = new StringBuilder().append(daoPath).append(table.getClassName()).append("Dao.java").toString();
                doMerge(daoFileName, ve, ctx, "template/" + type + "/dao.vm");


                // dao impl
                String daoImplPath = getRealPath(prefixPath, pakeName + ".dao.impl");
                String daoImplFileName = new StringBuilder().append(daoImplPath).append(table.getClassName()).append("DaoImpl.java").toString();
                doMerge(daoImplFileName, ve, ctx, "template/" + type + "/daoimpl.vm");


                // sqlmap
                String sqlmapPath = getRealPath(new StringBuffer().append(baseDir).append(File.separator).append(resFolder).append(File.separator).append("sqlMap").toString());
                String sqlmapPathFileName = new StringBuilder().append(sqlmapPath).append(table.getTableName()).append("_SqlMap.xml").toString();
                doMerge(sqlmapPathFileName, ve, ctx, "template/" + type + "/ibatis.vm");
            }

            if(tmpFileType ==2) { // 生成servie

                String servicePath = getRealPath(prefixPath, pakeName + ".service");
                String serviceFileName = new StringBuilder().append(servicePath).append(table.getClassName()).append("Service.java").toString();
                doMerge(serviceFileName, ve, ctx, "template/" + type + "/service.vm");

                String serviceImplPath = getRealPath(prefixPath, pakeName + ".service.impl");
                String serviceImplFileName = new StringBuilder().append(serviceImplPath).append(table.getClassName()).append("ServiceImpl.java").toString();
                doMerge(serviceImplFileName, ve, ctx, "template/" + type + "/serviceimpl.vm");

            }

//            if(tmpFileType ==3) { // 生成controller，jsp
//
//
//
//            }


        }
    }


    /**
     * @param fileName
     * @param ve
     * @param context
     * @param template
     * @throws Exception
     */
    private static void doMerge(String fileName, VelocityEngine ve, VelocityContext context, String template) throws Exception {
        File modelFile = new File(fileName);
        PrintWriter pw = new PrintWriter(modelFile);
        Template t = ve.getTemplate(template);
        t.merge(context, pw);
        pw.close();
    }

    private static String getRealPath(String srcPrefix, String pkgName) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append(srcPrefix);
        sb.append(StringUtils.replace(pkgName, ".", File.separator));
        sb.append(File.separator);
        String realPath = sb.toString();
        File f = new File(realPath);
        if ((!f.isDirectory()) && (!f.exists())) {
            f.mkdirs();
        }
        return realPath;
    }

    private static String getRealPath(String srcPrefix) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append(srcPrefix);
       // sb.append(StringUtils.replace(pkgName, ".", File.separator));
        sb.append(File.separator);
        String realPath = sb.toString();
        File f = new File(realPath);
        System.out.println(f.isDirectory()+","+(f.exists()));
        if ((!f.isDirectory()) && (!f.exists())) {
            f.mkdirs();
        }
        return realPath;
    }


    public static void veloCityTemplet(String fileName,
                                       Integer tmpFileType,
                                       List<Table> list,
                                       String type,
                                       String pakeName,
                                       String srcFolder,
                                       String baseDir,
                                       String resFolder) throws Exception {

        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.setProperty("input.encoding", "UTF-8");
        ve.setProperty("output.encoding", "UTF-8");
        ve.init();
        String prefixPath = new StringBuilder().append(srcFolder).append(File.separator).toString();
        for (Table table : list) {

            VelocityContext ctx = new VelocityContext();
            table.setPakeName(pakeName);
            System.out.println("filed size :"+table.getListField().size()+","+table.getListField().get(0).getFieldMark());
            ctx.put("table", table);
            ctx.put("fileName",fileName);
            ctx.put("jquery","$");
            //ctx.put("date",  new DateTool());

            if(tmpFileType ==3) { // 生成controller

                // 生成controller
                String controllerPath = getRealPath(prefixPath, new StringBuffer().append(pakeName).append(".controller.").append(fileName).toString());
                String modelFileName = new StringBuilder().append(controllerPath).append(table.getClassName()).append("Controller.java").toString();
                doMerge(modelFileName, ve, ctx, "template/" + type + "/controller.vm");

                prefixPath = prefixPath.substring(0,prefixPath.indexOf("java"));
                System.out.println("prefixPath:"+prefixPath);
                // list
                String listPath = getRealPath(prefixPath, new StringBuffer().append(".webapp.WEB-INF.views.").append(fileName).toString());
                System.out.println("listPath:"+listPath);
                String listFileName = new StringBuilder().append(listPath).append("list.jsp").toString();
                doMerge(listFileName, ve, ctx, "template/" + type + "/list.vm");


                // edit
                String editPath = getRealPath(prefixPath, new StringBuffer().append(".webapp.WEB-INF.views.").append(fileName).toString());
                String editFileName = new StringBuilder().append(editPath).append("edit.jsp").toString();
                doMerge(editFileName, ve, ctx, "template/" + type + "/edit.vm");

            }

        }
    }


//    public static void main(String[] args) throws SQLException {
////
//        String dbUrl = "jdbc:mysql://120.25.96.154:3306/dhdb??useUnicode=true&characterEncoding=UTF-8";
//        String dbUser = "dhsys";
//        String dbPassword = "123456";
//        String tbNamePattern = null;//"sys_menu";
//        String dbDriver = "com.mysql.jdbc.Driver";
//        FindMySqlTables findMySqlTables = new FindMySqlTables("sys_menu");
//
//
//
//        List<Table> list = findMySqlTables.getTablesFields(dbDriver,dbUrl, "dhdb",dbUser,dbPassword);
//
//
//        System.out.println(list);
////        System.out.println(db.getSrcFolder());
////        System.out.println(File.separator);
//
//    }
}
