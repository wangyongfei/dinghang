package com.dhsys.gen;


import com.dhsys.gen.connection.FindMySqlTables;
import com.dhsys.gen.service.TemplateService;
import com.dhsys.gen.table.Table;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.util.List;


@Mojo(name="db2code")
public class Db2code extends AbstractMojo {

    /**
     * db 驱动
     *  mysql 驱动
     */
    @Parameter(defaultValue = "com.mysql.jdbc.Driver")
    private String dbDriver;

    /**
     * db url
     */
    @Parameter(required = true)
    private String dbUrl;

    /**
     * db 用户名
     */
    @Parameter(defaultValue = "root")
    private String dbUserName;

    /**
     * db 密码
     */
    @Parameter(defaultValue = "")
    private String dbUserPwd;

    /**
     * 数据源
     */
    @Parameter(defaultValue = "")
    private String dbDataSource;


    /**
     * 生成类型
     * 1: 全部生成 (dao、model、service、controller、jsp)
     * 2: dao、model
     * 3: dao、model、service
     * 4: dao、model、service、controller
     * 默认生成全部
     */
    @Parameter(defaultValue = "1")
    private  Integer genType;


    /**
     * 生成数据库表名
     *  如果表名为 null,则生成全部表
     * */
    @Parameter(required = true)
    private String tableName;

    /**
     * 主源码路径
     */

    @Parameter(required=true, defaultValue="${project.build.sourceDirectory}")
    private String srcFolder;


    /**
     * 项目路径
     */
    @Parameter(defaultValue="${basedir}")
    private String baseDir;

    /**
     * resources 主路径
     */
    @Parameter(defaultValue="${resFolder}")
    private String resFolder;


    /**
     * 操作数据库类型
     */
    @Parameter(defaultValue = "mysql")
    private  String dbType;

    /**
     * 数据层模板类型
     */
    @Parameter(defaultValue = "1")
    private Integer tmpFileType;

    /**
     * 包名
     */
    @Parameter(defaultValue = "")
    private String pakeName;

    /**
     * schema
     */
    @Parameter(defaultValue = "dhdb")
    private String schema;

    /**
     * 文件夹名称
     */
    @Parameter(defaultValue = "")
    private String fileName;





    public void execute() throws MojoExecutionException, MojoFailureException {


        getLog().info("项目路径："+baseDir);
        getLog().info("java包路径："+srcFolder);
        getLog().info("resource包路径："+resFolder);

        FindMySqlTables findMySqlTables = new FindMySqlTables(this.tableName);
        List<Table> list = null;
        if("mysql".equals(dbType)){
            getLog().info("操作mysql数据库:"+dbDriver+","+dbUrl+","+dbUserName+","+dbUserPwd+","+schema);
            list = findMySqlTables.getTablesFields(dbDriver,dbUrl, schema,dbUserName,dbUserPwd);
        }else if("oralce".equals(dbType)){
            getLog().info("操作oracle数据库");
        }

        if(list != null){
            try {
                if(tmpFileType == 1 || tmpFileType == 2) {
                    TemplateService.veloCityTemplet(
                            tmpFileType,
                            list,
                            getTempFileType(tmpFileType),
                            pakeName,
                            srcFolder,
                            baseDir,
                            resFolder
                    );
                }

                if(tmpFileType == 3) {
                    TemplateService.veloCityTemplet(
                            fileName,
                            tmpFileType,
                            list,
                            getTempFileType(tmpFileType),
                            pakeName,
                            srcFolder,
                            baseDir,
                            resFolder
                    );
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     *  查询数据层模板类型
     * @param type
     * @return
     */
    private static  String getTempFileType(int type){
        String tmpFileName = "ibatis";
        switch (type){
            case 1: tmpFileName = "ibatis";
                break;
            case 2:tmpFileName = "service";
                break;
            case 3:tmpFileName = "controller";
                break;

            default:tmpFileName = "ibatis";
        }
        return tmpFileName;
    }

}
